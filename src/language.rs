use crate::chart::JsonFileError;
use once_cell::sync::Lazy;
use serde::Deserialize;
use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::sync::RwLock;

#[derive(Debug, Clone, Deserialize)]
pub struct Translation {
    pub version: i32,
    pub values: HashMap<String, String>,
}

impl Translation {
    pub fn from_file(path: &Path) -> Result<Self, JsonFileError> {
        Ok(serde_json::from_str(&fs::read_to_string(path)?)?)
    }
    pub fn get(&self, key: &str) -> String {
        self.values
            .get(key)
            .unwrap_or(&(String::from("ERR: ") + key))
            .to_owned()
    }
    pub fn translation(operation: TranslationOperation) -> &'static Lazy<RwLock<Translation>> {
        static TRANSLATION: Lazy<RwLock<Translation>> = Lazy::new(|| {
            println!("Loading initial translation file");
            RwLock::new(Translation::from_file(&Path::new("res/lang/eng.lang.json")).unwrap())
        });
        match operation {
            TranslationOperation::Patch(new_translation) => {
                if let Ok(mut translation) = TRANSLATION.try_write() {
                    println!("Patching translation");
                    for (key, value) in new_translation.values {
                        translation.values.insert(key, value);
                    }
                }
            }
            TranslationOperation::Replace(new_translation) => {
                if let Ok(mut translation) = TRANSLATION.try_write() {
                    println!("Replacing translation");
                    *translation = new_translation;
                }
            }
            _ => {}
        }
        &TRANSLATION
    }
}

#[derive(Debug, Clone)]
pub enum TranslationOperation {
    Get,
    Patch(Translation),
    Replace(Translation),
}

#[macro_export]
macro_rules! translate {
    ( $key: expr ) => {{
        use crate::language::Translation;
        use crate::language::TranslationOperation;
        Translation::translation(TranslationOperation::Get)
            .read()
            .unwrap()
            .get($key)
    }};
}

#[macro_export]
macro_rules! translate_fmt {
    ( $key: expr, $( $arg: expr ),* ) => {{
        use crate::translate;
        let mut format_string = translate!($key);
        let mut arg_num = 0;
        #[allow(unused_assignments)]
        {
            $(
                let placeholder_string = String::new() + "{" + &arg_num.to_string() + "}";
                let replacement_string = ($arg).to_string();
                format_string = format_string.replace(&placeholder_string, &replacement_string);
                arg_num += 1;
            )*
        }
        format_string = format_string.replace("{lb}", "{");
        format_string = format_string.replace("{rb}", "}");
        format_string
    }};
}
