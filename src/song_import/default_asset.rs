use std::{fs, path::Path};

use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Level {
    pub id: String,
    #[serde(rename = "musicId")]
    pub music_id: String,
    #[serde(rename = "illustrationId")]
    pub illustration_id: String,
    #[serde(rename = "chartIds")]
    pub chart_ids: Vec<String>,
    #[serde(rename = "appearType")]
    pub appear_type: u32,
    #[serde(rename = "discName")]
    pub disc_name: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Music {
    pub id: String,
    #[serde(rename = "musicName")]
    pub music_name: String,
    pub artist: String,
    #[serde(rename = "previewStartTime")]
    pub preview_start_time: f64,
    #[serde(rename = "previewOverTime")]
    pub preview_over_time: f64,
    #[serde(rename = "themeBackgroundColor")]
    pub theme_background_color: Color,
    #[serde(rename = "themeUiColor")]
    pub theme_ui_color: Color,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
    pub a: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Illustration {
    pub id: String,
    pub artist: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Chart {
    pub id: String,
    pub level: String,
    pub difficulty: f64,
    pub designer: String,
}

impl Chart {
    pub fn get_standard_difficulty_type(&self) -> String {
        match self.level.as_str() {
            "EZ" => "ez".to_string(),
            "HD" => "hd".to_string(),
            "IN" => "in".to_string(),
            custom => format!("custom_{}", custom),
        }
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct MonoBehaviour {
    pub levels: Vec<Level>,
    pub musics: Vec<Music>,
    pub illustrations: Vec<Illustration>,
    pub charts: Vec<Chart>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct DefaultAsset {
    #[serde(rename = "MonoBehaviour")]
    pub mono_behaviour: MonoBehaviour,
}

impl DefaultAsset {
    pub fn from_file(path: &Path) -> DefaultAsset {
        let yaml = fs::read_to_string(path).unwrap();
        return serde_yaml::from_str(&yaml).unwrap();
    }
    pub fn level(&self, id: &String) -> Option<&Level> {
        self.mono_behaviour
            .levels
            .iter()
            .find(|level| &level.id == id)
    }
    pub fn music(&self, id: &String) -> Option<&Music> {
        self.mono_behaviour
            .musics
            .iter()
            .find(|music| &music.id == id)
    }
    pub fn illustration(&self, id: &String) -> Option<&Illustration> {
        self.mono_behaviour
            .illustrations
            .iter()
            .find(|illustration| &illustration.id == id)
    }
    pub fn chart(&self, id: &String) -> Option<&Chart> {
        self.mono_behaviour
            .charts
            .iter()
            .find(|chart| &chart.id == id)
    }
}
