#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum IllustrationKind {
    Standard,
    StandardBig,
    Alt,
    AltBig,
}

impl IllustrationKind {
    pub fn get_standard_filename(&self) -> String {
        match &self {
            Self::Standard => "image.png",
            Self::StandardBig => "image_big.png",
            Self::Alt => "altimage.png",
            Self::AltBig => "altimage_big.png",
        }
        .to_string()
    }
    pub fn get_name(&self) -> String {
        match &self {
            Self::Standard => "illustration",
            Self::StandardBig => "illustration_big",
            Self::Alt => "altillustration",
            Self::AltBig => "altillustration_big",
        }
        .to_string()
    }
}
