#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum ChartKind {
    EZ,
    HD,
    IN,
}

impl ChartKind {
    pub fn get_bundled_filename(&self) -> String {
        match &self {
            Self::EZ => "Chart_EZ.json",
            Self::HD => "Chart_HD.json",
            Self::IN => "Chart_IN.json",
        }
        .to_string()
    }
    pub fn get_standard_filename(&self) -> String {
        match &self {
            Self::EZ => "chart_ez.json",
            Self::HD => "chart_hd.json",
            Self::IN => "chart_in.json",
        }
        .to_string()
    }
    pub fn get_name(&self) -> String {
        match &self {
            Self::EZ => "chart_ez",
            Self::HD => "chart_hd",
            Self::IN => "chart_in",
        }
        .to_string()
    }
}
