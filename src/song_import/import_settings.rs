use super::{chart_kind::ChartKind, illustration_kind::IllustrationKind};

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub enum AudioExportMode {
    DontConvert,
    ConvertFlac,
    #[default]
    ConvertOgg,
    ConvertOpus,
    ConvertMp3,
}

impl AudioExportMode {
    pub fn get_extension(&self) -> String {
        match self {
            Self::DontConvert => "wav",
            Self::ConvertFlac => "flac",
            Self::ConvertOgg => "ogg",
            Self::ConvertOpus => "opus",
            Self::ConvertMp3 => "mp3",
        }
        .to_string()
    }
}

#[derive(Clone, Debug)]
pub struct ImportSettings {
    pub delete_temp_dir_after_finish: bool,
    pub copy_instead_of_move: bool,
    pub audio_export_mode: AudioExportMode,
    pub charts_to_export: Vec<ChartKind>,
    pub illustrations_to_export: Vec<IllustrationKind>,
}

impl ImportSettings {
    pub fn audio_filename(&self) -> String {
        String::from("song.") + &self.audio_export_mode.get_extension()
    }
}

impl Default for ImportSettings {
    fn default() -> Self {
        Self {
            copy_instead_of_move: false,
            delete_temp_dir_after_finish: true,
            audio_export_mode: AudioExportMode::default(),
            charts_to_export: vec![ChartKind::EZ, ChartKind::HD, ChartKind::IN],
            illustrations_to_export: vec![
                IllustrationKind::Standard,
                IllustrationKind::StandardBig,
                IllustrationKind::Alt,
                IllustrationKind::AltBig,
            ],
        }
    }
}
