use super::{
    external_tools::{
        ffmpeg::FFmpegError,
        vgmstream::{vgmstream_convert, VgmStreamError},
    },
    import::find_acb_file,
    import_settings::{AudioExportMode, ImportSettings},
};
use crate::song_import::{
    external_tools::ffmpeg::ffmpeg_convert,
    import::{copy_and_log, shorten_path},
};
use std::{
    fs,
    io::{self, Write},
    path::Path,
};

pub fn extract_audio(
    unitycache_shared_dir: &Path,
    song_temp_dir: &Path,
    song_output_dir: &Path,
    asset_bundle_name: String,
    named_id: &String,
    import_settings: &ImportSettings,
) {
    let asset_bundle_dir = unitycache_shared_dir.join(&asset_bundle_name);
    let acb_temp_dir = song_temp_dir.join("acb");
    let acb_temp_file = &acb_temp_dir.join(named_id.to_owned() + ".acb");
    let wav_temp_file = &acb_temp_dir.join(named_id.to_owned() + ".wav");
    let result_path = song_output_dir.join(import_settings.audio_filename());

    // TODO: Handle errors
    fs::create_dir_all(&acb_temp_dir).unwrap();

    if let Some(acb_file) = find_acb_file(&asset_bundle_dir) {
        copy_and_log(&acb_file, &acb_temp_file);

        if result_path.is_file() {
            // File is already extracted
            // If extraction is not forced, skip the file
            println!("File {} is already extracted", shorten_path(&result_path));
            return;
        }

        let vgmstream = vgmstream_convert(&acb_temp_file, &wav_temp_file);
        if let Ok(_) = vgmstream {
            // io::stdout().write_all(&assetripper_output.stdout).unwrap();

            if import_settings.audio_export_mode == AudioExportMode::DontConvert {
                copy_and_log(&wav_temp_file, &result_path);
            } else {
                let ffmpeg = ffmpeg_convert(&wav_temp_file, &result_path);
                if let Ok(_) = ffmpeg {
                } else if let Err(FFmpegError::NonZeroStatusCode((_, output))) = ffmpeg {
                    let _ = io::stderr().write_all(&output.stderr);
                } else if let Err(FFmpegError::TerminatedBySignal(output)) = ffmpeg {
                    let _ = io::stderr().write_all(&output.stderr);
                }
            }
        } else if let Err(VgmStreamError::NonZeroStatusCode((_, output))) = vgmstream {
            let _ = io::stderr().write_all(&output.stderr);
        } else if let Err(VgmStreamError::TerminatedBySignal(output)) = vgmstream {
            let _ = io::stderr().write_all(&output.stderr);
        }
    } else {
        println!(
            "Error: ACB file in {} not found",
            shorten_path(&asset_bundle_dir)
        );
    }
}
