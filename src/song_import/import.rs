use super::{
    chart_kind::ChartKind,
    default_asset::DefaultAsset,
    external_tools::{
        assetripper::assetripper_meta::AssetRipperMeta, ffmpeg::ffprobe_get_duration,
    },
    extract_audio::extract_audio,
    illustration_kind::IllustrationKind,
    import_data::ImportData,
    import_settings::ImportSettings,
};
use crate::{
    song_import::{
        default_asset::Chart, external_tools::assetripper::assetripper_extract_multiple,
    },
    song_info::{chart_info::ChartInfo, credit_info::CreditInfo, SongInfo},
};
use std::{
    collections::HashMap,
    env, fs,
    io::{self},
    path::{Path, PathBuf},
};

pub fn shorten_path(path: &Path) -> String {
    shorten_path_to(path, 30)
}

pub fn shorten_path_to(path: &Path, max_length: usize) -> String {
    let path_string = path.to_string_lossy();
    if path_string.len() > max_length {
        let mut chars: Vec<char> = path_string.chars().collect();
        let range = (chars.len() - max_length)..chars.len();
        let chars_slice = &mut chars[range];
        chars_slice[0..3].fill('.');

        let mut result = String::new();
        for c in chars_slice {
            result.push(c.to_owned());
        }
        return result;
    }
    return path_string.to_string();
}

pub fn get_app_temp_dir() -> PathBuf {
    env::temp_dir().join("rainlane")
}

pub fn create_song_dirs(import_data: &ImportData) -> Result<PathBuf, io::Error> {
    let output_dir = PathBuf::new().join("songs/Rizline");

    for song in &import_data.abn_songs.songs {
        let song_dir = output_dir.join(song.named_id.clone());

        // TODO: handle errors
        fs::create_dir_all(song_dir)?;
    }

    return Ok(output_dir);
}

pub fn find_unitycache_shared_path(path: &Path, check_dir: &str) -> Option<PathBuf> {
    let mut current_path = PathBuf::new().join(path);
    if !current_path.is_dir() {
        return None;
    }

    for dirname in [
        "Android",
        "data",
        "com.PigeonGames.Rizline",
        "files",
        "UnityCache",
        "Shared",
    ] {
        let new_path = current_path.join(dirname);
        if new_path.is_dir() {
            current_path = new_path;
        }
    }

    if current_path.join(check_dir).is_dir() {
        Some(current_path)
    } else {
        None
    }
}

pub fn find_first_directory_in_path(path: &Path) -> Option<PathBuf> {
    fs::read_dir(path)
        .ok()
        .and_then(|mut read_dir| {
            read_dir.find(|dir_entry| dir_entry.as_ref().ok().map_or(false, |x| x.path().is_dir()))
        })
        .and_then(|x| x.ok())
        .map(|x| x.path())
}

pub fn find_first_file_in_path(path: &Path) -> Option<PathBuf> {
    fs::read_dir(path)
        .ok()
        .and_then(|mut read_dir| {
            read_dir.find(|dir_entry| {
                dir_entry
                    .as_ref()
                    .ok()
                    .map_or(false, |x| x.path().is_file())
            })
        })
        .and_then(|x| x.ok())
        .map(|x| x.path())
}

pub fn find_acb_file(asset_bundle_dir: &Path) -> Option<PathBuf> {
    find_first_directory_in_path(&asset_bundle_dir)
        .and_then(|subdir_path| find_first_file_in_path(&subdir_path))
}

pub fn copy_and_log(source: &Path, destination: &Path) {
    println!(
        "Copying: {} -> {}",
        shorten_path(&source),
        shorten_path(&destination)
    );
    match fs::copy(source, destination) {
        // Ok(bytes_copied) => println!("Successfully copied {} bytes", bytes_copied),
        Err(e) => println!("Error: an error ocurred while copying: {:?}", e),
        _ => {}
    };
}

pub fn move_and_log(source: &Path, destination: &Path) {
    println!(
        "Moving: {} -> {}",
        shorten_path(&source),
        shorten_path(&destination)
    );
    match fs::rename(source, destination) {
        // Ok(()) => println!("Successfully moved file"),
        Err(e) => println!("Error: an error ocurred while copying: {:?}", e),
        _ => {}
    };
}

pub fn move_or_copy(source: &Path, destination: &Path, import_settings: &ImportSettings) {
    if import_settings.copy_instead_of_move {
        copy_and_log(source, destination);
    } else {
        move_and_log(source, destination);
    }
}

pub fn find_all_files_with_filename(
    directory: &Path,
    filename: String,
    file_extension: String,
) -> Vec<PathBuf> {
    let mut result = Vec::new();
    let mut current_number = None;
    let mut current_file_path = directory.join(format!("{}{}", filename, file_extension));

    while current_file_path.is_file() {
        result.push(current_file_path);
        current_number = current_number.map_or(Some(0), |num| Some(num + 1));
        current_file_path = match current_number {
            Some(num) => directory.join(format!("{}_{}{}", filename, num, file_extension)),
            None => directory.join(format!("{}{}", filename, file_extension)),
        };
    }
    return result;
}

pub fn create_file_bundle_map_from_files(files: &Vec<PathBuf>) -> HashMap<String, PathBuf> {
    let mut result = HashMap::new();
    for path in files {
        // TODO: handle errors
        let meta_filename = path
            .parent()
            .unwrap()
            .join(path.file_name().unwrap().to_string_lossy().to_string() + ".meta");
        let meta_file = AssetRipperMeta::from_file(&meta_filename);
        result.insert(meta_file.get_abn(), path.to_owned());
    }
    result
}

pub fn create_file_bundle_map(directory: &Path, filename: &str) -> HashMap<String, PathBuf> {
    let segments = Path::new(filename);
    let files = find_all_files_with_filename(
        directory,
        segments.file_stem().unwrap().to_string_lossy().to_string(),
        String::from(".") + &segments.extension().unwrap().to_string_lossy().to_string(),
    );
    let result = create_file_bundle_map_from_files(&files);
    println!("Created file bundle map for {}", filename);
    return result;
}

pub fn get_all_bundle_dirs(
    unitycache_shared_dir: &Path,
    import_data: &ImportData,
    import_settings: &ImportSettings,
) -> Vec<PathBuf> {
    // TODO: add the option to filter out items which have already been exported to speed up import times

    let mut asset_bundle_dirs = Vec::new();
    for song in &import_data.abn_songs.songs {
        for chart_kind in &import_settings.charts_to_export {
            if let Some(abn) = song.get_chart_abn(chart_kind.to_owned()) {
                if unitycache_shared_dir.join(abn).is_dir() {
                    asset_bundle_dirs.push(unitycache_shared_dir.join(abn));
                }
            }
        }
        for illustration_kind in &import_settings.illustrations_to_export {
            if let Some(abn) = song.get_illustration_abn(illustration_kind.to_owned()) {
                if unitycache_shared_dir.join(abn).is_dir() {
                    asset_bundle_dirs.push(unitycache_shared_dir.join(abn));
                }
            }
        }
    }
    if unitycache_shared_dir
        .join(&import_data.main.default_asset_abn)
        .is_dir()
    {
        asset_bundle_dirs.push(unitycache_shared_dir.join(&import_data.main.default_asset_abn));
    }
    asset_bundle_dirs
}

pub fn extract_all(
    unitycache_shared_dir: &Path,
    import_data: &ImportData,
    import_settings: &ImportSettings,
) {
    let temp_dir = get_app_temp_dir();
    let extracted_bundles_dir = temp_dir.join("extracted/all_bundles");
    let assets_dir = extracted_bundles_dir.join("ExportedProject/Assets");
    let extracted_audio_dir = temp_dir.join("extracted/audio");

    // Find all relevant asset bundles, save them to asset_bundle_dirs
    let asset_bundle_dirs =
        get_all_bundle_dirs(unitycache_shared_dir, import_data, import_settings);

    // Extract all asset bundles needed to {tmpdir}/extracted_bundles/all
    // TODO: Handle errors
    let _ = assetripper_extract_multiple(asset_bundle_dirs, &extracted_bundles_dir);

    // Get default asset (contains song metadata)
    let default_asset = DefaultAsset::from_file(&assets_dir.join("Default.asset"));

    // Get charts
    // Chart files are named: "Chart_EZ.json", "Chart_EZ_0.json", "Chart_EZ_1.json", "Chart_EZ_2.json"... etc.
    // The charts exported by AssetRipper should be in the order reverse to the console arguments,
    // but I don't know if that's guaranteed or not. To be absolutely sure, we need to read the .meta file
    // (which has the same name as the file, but with .meta at the end) to see what asset bundle the file is coming from.
    // `create_file_bundle_map` takes care of that.
    // The function returns a map in which keys are asset bundle names and the values are the file paths:
    // {
    //     "e790e5d9e7501dbb7d25cc9c6beca4cf": "{tmp}/extracted_bundles/all/ExportedProject/Assets/Chart_EZ_20.json",
    //     "91c643ec97bdf6b8236d7baf5c03cf3f": "{tmp}/extracted_bundles/all/ExportedProject/Assets/Chart_EZ_11.json",
    //     ... (etc)
    // }

    let charts: HashMap<ChartKind, HashMap<String, PathBuf>> = import_settings
        .charts_to_export
        .iter()
        .map(|chart_kind| {
            (
                chart_kind.to_owned(),
                create_file_bundle_map(&assets_dir, &chart_kind.get_bundled_filename()),
            )
        })
        .into_iter()
        .collect();

    for song in &import_data.abn_songs.songs {
        let song_output_dir = PathBuf::new()
            .join("songs/Rizline")
            .join(song.named_id.clone());

        let illustration_id_opt = &default_asset
            .level(&song.named_id)
            .and_then(|x| Some(x.illustration_id.clone()));

        if let Some(illustration_id) = illustration_id_opt {
            let illustrations =
                create_file_bundle_map(&assets_dir, &(illustration_id.to_owned() + ".png"));
            let alt_illustrations = create_file_bundle_map(
                &assets_dir,
                &(illustration_id
                    .replace("illustration", "altIllustration")
                    .replace(".0", ".0.0")
                    + ".png"),
            );

            for illustration_kind in &import_settings.illustrations_to_export {
                if let Some(abn) = song
                    .get_illustration_abn(illustration_kind.to_owned())
                    .clone()
                {
                    if let Some(illustration_file) = match illustration_kind {
                        IllustrationKind::Standard => illustrations.get(&abn).to_owned(),
                        IllustrationKind::StandardBig => illustrations.get(&abn).to_owned(),
                        IllustrationKind::Alt => alt_illustrations.get(&abn).to_owned(),
                        IllustrationKind::AltBig => alt_illustrations.get(&abn).to_owned(),
                    } {
                        move_or_copy(
                            &illustration_file,
                            &song_output_dir.join(illustration_kind.get_standard_filename()),
                            import_settings,
                        );
                    }
                }
            }
        }

        for chart_kind in &import_settings.charts_to_export {
            if let Some(abn) = song.get_chart_abn(chart_kind.to_owned()).clone() {
                if let Some(chart_file) = charts
                    .get(&chart_kind)
                    .and_then(|file_map| file_map.get(&abn))
                {
                    move_or_copy(
                        chart_file,
                        &song_output_dir.join(chart_kind.get_standard_filename()),
                        import_settings,
                    );
                }
            }
        }

        if let Some(asset_bundle_name) = song.abn_acb.to_owned() {
            extract_audio(
                unitycache_shared_dir,
                &extracted_audio_dir,
                &song_output_dir,
                asset_bundle_name,
                &song.named_id,
                import_settings,
            );
        }

        // Metadata
        // TODO: error handling
        if let Some(level) = default_asset.level(&song.named_id) {
            let music = default_asset.music(&level.music_id).unwrap();
            let illustration = default_asset.illustration(&level.illustration_id).unwrap();
            let charts: Vec<&Chart> = level
                .chart_ids
                .iter()
                .map(|chart_id| default_asset.chart(chart_id).unwrap())
                .collect();

            let song_info = SongInfo {
                title: music.music_name.clone(),
                duration: ffprobe_get_duration(
                    &song_output_dir.join(import_settings.audio_filename()),
                )
                .ok()
                .and_then(|output| output.get_duration_ms().ok())
                .unwrap_or(0),
                source: String::from("rizline"),
                preview_start: Some((music.preview_start_time * 1000.0) as i64),
                preview_duration: ((music.preview_over_time - music.preview_start_time) * 1000.0)
                    as u64,
                song_credits: CreditInfo {
                    artist: music.artist.clone(),
                    ..Default::default()
                },
                image_credits: CreditInfo {
                    artist: illustration.artist.clone(),
                    ..Default::default()
                },
                charts: charts
                    .iter()
                    .map(|chart| {
                        let difficulty_type = chart.get_standard_difficulty_type();
                        ChartInfo {
                            difficulty_type: difficulty_type.clone(),
                            difficulty_level: chart.difficulty,
                            charter: chart.designer.clone(),
                            filename: format!("chart_{}.json", difficulty_type),
                        }
                    })
                    // TODO: this filter performs a filesystem access, this is unnecessary because the program should already know what is and isn't exported.
                    // Previous imports also have to be taken into account though
                    .filter(|c| song_output_dir.join(&c.filename).is_file())
                    .collect(),
            };

            let metadata_filename = song_output_dir.join("info.json");
            // Handle errors
            let _ = song_info.to_file(&metadata_filename);
            println!(
                "Created metadata file: {}",
                shorten_path(&metadata_filename).to_string()
            );
        }
    }
}

pub fn start_import(input_path: &Path, import_settings: ImportSettings) {
    println!("Starting import with settings: {:#?}", import_settings);

    let import_data = ImportData::from_files(
        "res/data/import/import_data.json",
        "res/data/import/abn_songs.tsv",
    )
    .expect("error while loading from files");

    // println!("{:#?}", abn_file);

    // Input directory is .../UnityCache/Shared/
    let check_dir = &import_data.main.default_asset_abn;
    if let Some(input_dir) = find_unitycache_shared_path(input_path, &check_dir) {
        println!("Found UnityCache dir: {}", shorten_path(&input_dir));

        if let Ok(output_dir) = create_song_dirs(&import_data) {
            println!("Created output dir at: {}", shorten_path(&output_dir));
            extract_all(&input_dir, &import_data, &import_settings);
        } else {
            println!("Error: couldn't create output dir");
        }
    } else {
        println!("Error: couldn't find .../UnityCache/Shared/ dir");
    }
}
