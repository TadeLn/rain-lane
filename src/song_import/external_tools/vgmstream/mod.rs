pub mod acb_metadata;

use self::acb_metadata::AcbMetadata;
use crate::song_import::import::shorten_path;
use std::{
    path::Path,
    process::{Command, Output},
    string::FromUtf8Error,
};

#[derive(Debug)]
pub enum VgmStreamError {
    LaunchError,
    NonZeroStatusCode((i32, Output)),
    TerminatedBySignal(Output),
    FromUtf8Error(FromUtf8Error),
    SerdeJsonError(serde_json::Error),
}

impl From<FromUtf8Error> for VgmStreamError {
    fn from(value: FromUtf8Error) -> Self {
        Self::FromUtf8Error(value)
    }
}

impl From<serde_json::Error> for VgmStreamError {
    fn from(value: serde_json::Error) -> Self {
        Self::SerdeJsonError(value)
    }
}

pub fn vgmstream_convert(acb_file: &Path, wav_file: &Path) -> Result<Output, VgmStreamError> {
    println!(
        "Converting ACB: {} -> {}",
        shorten_path(&acb_file),
        shorten_path(&wav_file)
    );

    // TODO: display output to user
    let output = Command::new("vgmstream-cli")
        .arg("-o")
        .arg(wav_file)
        .arg(acb_file)
        .output()
        .or(Err(VgmStreamError::LaunchError))?;

    return match output.status.code() {
        Some(0) => Ok(output), // Success
        Some(error_code) => Err(VgmStreamError::NonZeroStatusCode((error_code, output))),
        None => Err(VgmStreamError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}

#[deprecated = "for some reason vgmstream-cli doesn't support the -I flag on windows, making this function not work crossplatform"]
pub fn vgmstream_get_metadata(acb_file: &Path) -> Result<AcbMetadata, VgmStreamError> {
    let output = Command::new("vgmstream-cli")
        .arg("-mI") // m - print metadata, don't export audio; I - print metadata in JSON format
        .arg(acb_file)
        .output()
        .or(Err(VgmStreamError::LaunchError))?;

    return match output.status.code() {
        Some(0) => {
            // Success
            // Read output, parse JSON and deserialize to AcbMetadata
            let json_string = String::from_utf8(output.stdout.to_owned())?;
            let metadata = serde_json::from_str::<AcbMetadata>(&json_string)?;
            Ok(metadata)
        }
        Some(error_code) => Err(VgmStreamError::NonZeroStatusCode((error_code, output))),
        None => Err(VgmStreamError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}
