use serde::Deserialize;

#[derive(Clone, Deserialize)]
pub struct StreamInfo {
    pub index: u64,
    pub name: String,
    pub total: u64,
}

#[derive(Clone, Deserialize)]
pub struct AcbMetadata {
    pub version: String,
    #[serde(rename = "sampleRate")]
    pub sample_rate: u64,
    pub channels: u64,
    #[serde(rename = "mixingInfo")]
    pub mixing_info: (), // null
    #[serde(rename = "channelLayout")]
    pub channel_layout: u64,
    #[serde(rename = "loopingInfo")]
    pub looping_info: (), // null
    #[serde(rename = "interleaveInfo")]
    pub interleave_info: (), // null
    #[serde(rename = "numberOfSamples")]
    pub number_of_samples: u64,
    pub encoding: String,
    pub layout: String,
    #[serde(rename = "frameSize")]
    pub frame_size: (), // null
    #[serde(rename = "metadataSource")]
    pub metadata_source: String,
    pub bitrate: u64,
    #[serde(rename = "streamInfo")]
    pub stream_info: StreamInfo,
}

impl AcbMetadata {
    pub fn duration_seconds(&self) -> f64 {
        (self.number_of_samples as f64) / (self.sample_rate as f64)
    }
    pub fn duration_milliseconds(&self) -> u64 {
        (self.number_of_samples * 1000) / self.sample_rate
    }
}
