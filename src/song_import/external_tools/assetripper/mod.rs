pub mod assetripper_meta;
pub mod path_id_map;

use crate::song_import::import::shorten_path;
use std::{
    path::{Path, PathBuf},
    process::{Command, Output, Stdio},
};

#[derive(Clone, Debug)]
pub enum AssetRipperError {
    LaunchError,
    NonZeroStatusCode((i32, Output)),
    TerminatedBySignal(Output),
}

pub fn assetripper_extract(
    asset_bundle_dir: &Path,
    extracted_project_dir: &Path,
) -> Result<Output, AssetRipperError> {
    println!(
        "Extracting Asset Bundle:\n  - from: {}\n  - into: {}",
        shorten_path(&asset_bundle_dir),
        shorten_path(&extracted_project_dir)
    );

    // TODO: display assetripper output to user
    let output = Command::new("AssetRipper")
        .arg(asset_bundle_dir)
        .arg("--output")
        .arg(extracted_project_dir)
        .arg("--quit") // Ensures automatic program exit, without needing to press a key or crash the program
        .output()
        .or(Err(AssetRipperError::LaunchError))?;

    return match output.status.code() {
        Some(0) => Ok(output), // Success
        Some(error_code) => Err(AssetRipperError::NonZeroStatusCode((error_code, output))),
        None => Err(AssetRipperError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}

pub fn assetripper_extract_multiple(
    asset_bundle_dirs: Vec<PathBuf>,
    extracted_project_dir: &Path,
) -> Result<Output, AssetRipperError> {
    println!(
        "Extracting {} asset bundles to: {}",
        asset_bundle_dirs.len(),
        shorten_path(&extracted_project_dir)
    );

    // TODO: display assetripper output to user
    let output = Command::new("AssetRipper")
        .args(asset_bundle_dirs)
        .arg("--output")
        .arg(extracted_project_dir)
        .arg("--quit") // Ensures automatic program exit, without needing to press a key or crash the program
        .stdout(Stdio::inherit())
        .output()
        .or(Err(AssetRipperError::LaunchError))?;

    return match output.status.code() {
        Some(0) => Ok(output), // Success
        Some(error_code) => Err(AssetRipperError::NonZeroStatusCode((error_code, output))),
        None => Err(AssetRipperError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}
