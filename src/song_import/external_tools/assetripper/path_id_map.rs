use crate::chart::JsonFileError;
use serde::Deserialize;
use std::{fs, path::Path};

#[derive(Clone, Debug, Deserialize)]
pub struct AssetEntry {
    #[serde(rename = "PathID")]
    pub path_id: i64,

    #[serde(rename = "Name")]
    pub name: String,

    #[serde(rename = "Type")]
    pub kind: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct FileEntry {
    #[serde(rename = "Name")]
    pub name: String,

    #[serde(rename = "Assets")]
    pub assets: Vec<AssetEntry>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct PathIdMap {
    #[serde(rename = "Files")]
    pub files: Vec<FileEntry>,
}

impl PathIdMap {
    pub fn from_file(path: &Path) -> Result<PathIdMap, JsonFileError> {
        let file_contents = fs::read_to_string(path)?;
        let path_id_map = serde_json::from_str::<PathIdMap>(&file_contents)?;
        Ok(path_id_map)
    }

    pub fn has_any_files(&self) -> bool {
        return !self.files.is_empty();
    }

    pub fn get_first_asset_name(&self) -> Option<String> {
        self.files
            .get(0)
            .and_then(|first_file| first_file.assets.get(0))
            .and_then(|first_asset| Some(first_asset.name.to_owned()))
    }
}
