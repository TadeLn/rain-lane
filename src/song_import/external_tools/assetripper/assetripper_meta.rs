use serde::Deserialize;
use std::{fs, path::Path};

#[derive(Clone, Debug, Deserialize)]
pub struct GenericImporter {
    #[serde(rename = "userData")]
    pub user_data: (),

    #[serde(rename = "assetBundleName")]
    pub asset_bundle_name: String,

    #[serde(rename = "assetBundleVariant")]
    pub asset_bundle_variant: (),
}

#[derive(Clone, Debug, Deserialize)]
pub struct AssetRipperMeta {
    #[serde(rename = "fileFormatVersion")]
    pub file_format_version: i32,
    #[serde(rename = "guid")]
    pub guid: String,
    #[serde(rename = "timeCreated")]
    pub time_created: u64,
    #[serde(rename = "licenseType")]
    pub license_type: String,
    #[serde(rename = "TextScriptImporter")]
    pub text_script_importer: Option<GenericImporter>,
    #[serde(rename = "TextureImporter")]
    pub texture_importer: Option<GenericImporter>,
}

impl AssetRipperMeta {
    pub fn from_file(path: &Path) -> AssetRipperMeta {
        // TODO: Handle errors
        let file_contents = fs::read_to_string(path).unwrap();
        let assetripper_meta = serde_yaml::from_str::<AssetRipperMeta>(&file_contents).unwrap();
        assetripper_meta
    }
    pub fn get_full_abn(&self) -> String {
        if let Some(importer) = &self.text_script_importer {
            importer.asset_bundle_name.clone()
        } else if let Some(importer) = &self.texture_importer {
            importer.asset_bundle_name.clone()
        } else {
            // TODO: Handle Errors
            panic!("a");
        }
    }
    pub fn get_abn(&self) -> String {
        // TODO: Handle errors
        self.get_full_abn()
            .strip_suffix(".bundle")
            .unwrap()
            .to_string()
    }
}
