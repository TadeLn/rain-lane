use std::num::ParseFloatError;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Format {
    pub duration: String,
}

#[derive(Debug, Deserialize)]
pub struct FFprobeOutput {
    pub format: Format,
}

impl FFprobeOutput {
    pub fn from_string(string: &String) -> Result<Self, serde_json::Error> {
        serde_json::from_str(string)
    }

    pub fn get_duration_ms(&self) -> Result<i64, ParseFloatError> {
        Ok((self.format.duration.parse::<f64>()? * 1000.0) as i64)
    }
}
