pub mod ffprobe_output;

use crate::song_import::import::shorten_path;
use std::{
    path::Path,
    process::{Command, Output},
    string::FromUtf8Error,
};

use self::ffprobe_output::FFprobeOutput;

#[derive(Debug)]
pub enum FFmpegError {
    LaunchError,
    NonZeroStatusCode((i32, Output)),
    TerminatedBySignal(Output),
    FromUtf8Error(FromUtf8Error),
    JsonParseError(serde_json::Error),
}

impl From<FromUtf8Error> for FFmpegError {
    fn from(value: FromUtf8Error) -> Self {
        Self::FromUtf8Error(value)
    }
}

impl From<serde_json::Error> for FFmpegError {
    fn from(value: serde_json::Error) -> Self {
        Self::JsonParseError(value)
    }
}

pub fn ffmpeg_convert(input_file: &Path, output_file: &Path) -> Result<Output, FFmpegError> {
    println!(
        "Converting using ffmpeg: {} -> {}",
        shorten_path(&input_file),
        shorten_path(&output_file)
    );

    // TODO: display ffmpeg output to user
    let output = Command::new("ffmpeg")
        .arg("-i")
        .arg(input_file)
        .arg(output_file)
        .output()
        .or(Err(FFmpegError::LaunchError))?;

    return match output.status.code() {
        Some(0) => Ok(output), // Success
        Some(error_code) => Err(FFmpegError::NonZeroStatusCode((error_code, output))),
        None => Err(FFmpegError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}

// Returns the duration of a file in seconds
pub fn ffprobe_get_duration(input_file: &Path) -> Result<FFprobeOutput, FFmpegError> {
    let output = Command::new("ffprobe")
        .arg("-i") // i - specify input file
        .arg(input_file)
        .arg("-show_entries")
        .arg("format=duration") // Print duration in seconds
        // .arg("-v")
        // .arg("quiet") // Quiet
        .arg("-of")
        .arg("json") // Better output format
        .output()
        .or(Err(FFmpegError::LaunchError))?;

    return match output.status.code() {
        Some(0) => {
            // Success
            Ok(FFprobeOutput::from_string(&String::from_utf8(
                output.stdout,
            )?)?)
        }
        Some(error_code) => Err(FFmpegError::NonZeroStatusCode((error_code, output))),
        None => Err(FFmpegError::TerminatedBySignal(output)), // Terminated by signal - assume failure
    };
}
