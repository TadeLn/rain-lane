pub mod abn_songs_file;
pub mod import_data_file;

use self::{abn_songs_file::ABNSongsFile, import_data_file::MainImportDataFile};
use crate::chart::JsonFileError;

#[derive(Clone, Debug)]
pub struct ImportData {
    pub main: MainImportDataFile,
    pub abn_songs: ABNSongsFile,
}

impl ImportData {
    pub fn from_files(main_filepath: &str, songs_filepath: &str) -> Result<Self, JsonFileError> {
        let import_data = Self {
            main: MainImportDataFile::from_file(main_filepath)?,
            abn_songs: ABNSongsFile::from_file(songs_filepath)?,
        };
        Ok(import_data)
    }
}
