use once_cell::sync::Lazy;
use regex::Regex;

use crate::song_import::{chart_kind::ChartKind, illustration_kind::IllustrationKind};
use std::{fs, io, ops::Not};

#[derive(Clone, Debug)]
pub struct ABNSongsEntry {
    pub number: i64,
    pub named_id: String,
    pub abn_chart_ez: Option<String>,
    pub abn_chart_hd: Option<String>,
    pub abn_chart_in: Option<String>,
    pub abn_illustration: Option<String>,
    pub abn_illustration_big: Option<String>,
    pub abn_altillustration: Option<String>,
    pub abn_altillustration_big: Option<String>,
    pub abn_acb: Option<String>,
    pub abn_acb_old: Option<String>,
}

impl ABNSongsEntry {
    pub fn get_chart_abn(&self, chart_kind: ChartKind) -> &Option<String> {
        match chart_kind {
            ChartKind::EZ => &self.abn_chart_ez,
            ChartKind::HD => &self.abn_chart_hd,
            ChartKind::IN => &self.abn_chart_in,
        }
    }
    pub fn get_illustration_abn(&self, illustration_kind: IllustrationKind) -> &Option<String> {
        match illustration_kind {
            IllustrationKind::Standard => &self.abn_illustration,
            IllustrationKind::StandardBig => &self.abn_illustration_big,
            IllustrationKind::Alt => &self.abn_altillustration,
            IllustrationKind::AltBig => &self.abn_altillustration_big,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum RowLoadError {
    NumberNotPresent,
    NamedIdNotPresent,
    NumberParseError,
}

pub fn sanitize_abn(text: &String) -> String {
    static RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"[^a-f0-9]").unwrap());
    RE.replace_all(text, "").to_string()
}

pub fn sanitize_named_id(text: &String) -> String {
    static RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"[^a-zA-Z0-9.]").unwrap());
    RE.replace_all(text, "").to_string()
}

impl ABNSongsEntry {
    pub fn from_line(line: String, header: &Vec<String>) -> Result<Self, RowLoadError> {
        let segments = parse_tsv_line(line);
        let get_field = |field: &str| {
            header
                .iter()
                .position(|x| x == &field)
                .and_then(|column_index| segments.get(column_index))
                .and_then(|x| x.is_empty().not().then_some(x.to_owned()))
        };
        let get_field_abn = |field: &str| get_field(field).and_then(|abn| Some(sanitize_abn(&abn)));
        let get_field_named_id =
            |field: &str| get_field(field).and_then(|abn| Some(sanitize_named_id(&abn)));

        Ok(Self {
            number: get_field("#")
                .ok_or(RowLoadError::NamedIdNotPresent)?
                .parse::<i64>()
                .or(Err(RowLoadError::NumberParseError))?,
            named_id: get_field_named_id("named_id")
                .ok_or(RowLoadError::NamedIdNotPresent)?
                .to_owned(),
            abn_chart_ez: get_field_abn("abn_chart_ez"),
            abn_chart_hd: get_field_abn("abn_chart_hd"),
            abn_chart_in: get_field_abn("abn_chart_in"),
            abn_illustration: get_field_abn("abn_illustration"),
            abn_illustration_big: get_field_abn("abn_illustration_big"),
            abn_altillustration: get_field_abn("abn_altillustration"),
            abn_altillustration_big: get_field_abn("abn_altillustration_big"),
            abn_acb: get_field_abn("abn_acb"),
            abn_acb_old: get_field_abn("abn_acb_old"),
        })
    }
}

#[derive(Clone, Debug)]
pub struct ABNSongsFile {
    pub songs: Vec<ABNSongsEntry>,
}

impl ABNSongsFile {
    pub fn from_file(path: &str) -> Result<ABNSongsFile, io::Error> {
        let mut entries = Vec::new();
        let file_contents = fs::read_to_string(path)?;
        let mut header_opt = None;

        // Process all lines
        for line in file_contents.split("\n") {
            // Skip empty lines
            if line.is_empty() {
                continue;
            }

            if let Some(header) = &header_opt {
                // Every line other than the first one is parsed by ABNEntry::from_line
                if let Ok(entry) = ABNSongsEntry::from_line(line.to_owned(), &header) {
                    entries.push(entry);
                }
            } else {
                // First line is header
                header_opt = Some(parse_tsv_line(line.to_owned()));
            }
        }

        // Sort entries by number, in case the .tsv data is not sorted yet
        entries.sort_by(|a, b| {
            a.number
                .partial_cmp(&b.number)
                .unwrap_or(std::cmp::Ordering::Less)
        });
        Ok(ABNSongsFile { songs: entries })
    }
}

// TODO: Currently this function does not deal with escaped characters (\t, \n, \r, \\).
// Because of \\, it's not as simple as `x.replace("\\t", "\t")`
//
// The input '\\t' should result in a backslash followed by the letter t
// If you do `x.replace("\\\\", "\\").replace("\\t", "\t")` then input string '\\t' gets interpreted as the tab char
// If you do `x.replace("\\t", "\t").replace("\\\\", "\\")` then input string '\\t' gets interpreted as a backslash followed by a tab char
// (both orderings are incorrect)
//
// A better solution is to loop through the string... or to just ignore the escaped characters for now
fn parse_tsv_line(line: String) -> Vec<String> {
    line.trim()
        .split("\t")
        .map(|x| x.trim().to_owned())
        .collect()
}
