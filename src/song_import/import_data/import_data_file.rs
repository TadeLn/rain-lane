use crate::chart::JsonFileError;
use serde::Deserialize;
use std::fs;

use super::abn_songs_file::sanitize_abn;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct MainImportDataFile {
    pub default_asset_abn: String,
}

impl MainImportDataFile {
    pub fn from_file(path: &str) -> Result<MainImportDataFile, JsonFileError> {
        let file_contents = fs::read_to_string(path)?;
        let mut main_file = serde_json::from_str::<MainImportDataFile>(&file_contents)?;
        main_file.default_asset_abn = sanitize_abn(&main_file.default_asset_abn);
        Ok(main_file)
    }
}
