use std::{
    fs, io,
    path::{Path, PathBuf},
};

use serde::{Deserialize, Serialize};

use crate::{
    chart::{note::NoteType, theme::Theme, Chart, JsonFileError},
    game::PathError,
    song_info::SongInfo,
};

pub const SONG_LIST_CACHE_FILENAME: &str = "song_list_cache.json";

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SongEntry {
    pub path: String,
    pub song_info: SongInfo,
    pub colors: Theme,
    pub _colors_alternate: Theme,
    pub hits: Vec<i64>,
}

impl SongEntry {
    pub fn get_path(&self) -> PathBuf {
        Path::new(&self.path).to_path_buf()
    }
}

#[derive(Debug)]
pub enum SongScanError {
    IoError(io::Error),
    JsonFileError(JsonFileError),
    PathError(PathError),
}

impl From<io::Error> for SongScanError {
    fn from(value: io::Error) -> Self {
        Self::IoError(value)
    }
}

impl From<JsonFileError> for SongScanError {
    fn from(value: JsonFileError) -> Self {
        Self::JsonFileError(value)
    }
}

impl From<PathError> for SongScanError {
    fn from(value: PathError) -> Self {
        Self::PathError(value)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SongListCache {
    pub songs: Vec<SongEntry>,
}

impl SongListCache {
    pub fn new() -> Self {
        SongListCache { songs: vec![] }
    }

    pub fn load_or_create(path: PathBuf) -> Self {
        if path.is_file() {
            match SongListCache::from_file(path) {
                Ok(a) => a,
                Err(_) => SongListCache::new(),
            }
        } else {
            SongListCache::new()
        }
    }

    pub fn from_file(path: PathBuf) -> Result<SongListCache, JsonFileError> {
        let json = fs::read_to_string(path)?;
        Ok(serde_json::from_str::<SongListCache>(&json)?)
    }

    pub fn to_file(&self, path: PathBuf) -> Result<(), JsonFileError> {
        let json = serde_json::to_string::<SongListCache>(&self)?;
        fs::write(path, json)?;
        Ok(())
    }

    fn check_if_dir_is_song(path: &PathBuf) -> bool {
        if !path.is_dir() {
            return false;
        }

        if !path.join(Path::new("info.json")).is_file() {
            return false;
        }
        return true;
    }

    pub fn rescan_songs(&mut self, path: PathBuf) -> Result<(), SongScanError> {
        println!("Scanning songs...");
        self.songs = vec![];
        self.scan_subdir(path)?;
        println!(
            "Song scanning complete, saving to {}...",
            SONG_LIST_CACHE_FILENAME
        );
        self.to_file(SONG_LIST_CACHE_FILENAME.into())?;
        println!("Song scanning complete");
        Ok(())
    }

    fn scan_subdir(&mut self, path: PathBuf) -> Result<(), SongScanError> {
        let read_dir = fs::read_dir(path)?;
        for dir_entry_result in read_dir {
            match dir_entry_result {
                Ok(dir_entry) => {
                    let path = dir_entry.path();
                    if SongListCache::check_if_dir_is_song(&path) {
                        // Found song!
                        let song_info_result = SongInfo::from_file(&path.clone().join("info.json"));
                        match song_info_result {
                            Ok(song_info) => {
                                let mut hits = vec![];
                                let (mut colors, mut colors_alternate) = Default::default();

                                for chart_info in &song_info.charts {
                                    let chart_filename = chart_info.filename.clone();
                                    let chart_result = Chart::from_file(
                                        path.clone()
                                            .join(Path::new(&chart_filename.clone()))
                                            .to_string_lossy()
                                            .to_string(),
                                    );
                                    if let Ok(chart) = chart_result {
                                        let mut hit_count = 0;
                                        for line in chart.lines {
                                            for note in line.notes {
                                                hit_count += match note.get_note_type() {
                                                    NoteType::HoldNote => 2,
                                                    _ => 1,
                                                }
                                            }
                                        }
                                        colors = chart.themes[0].clone();
                                        colors_alternate = chart.themes[1].clone();
                                        hits.push(hit_count);
                                    } else {
                                        hits.push(0);
                                    }
                                }
                                self.songs.push(SongEntry {
                                    path: path.to_string_lossy().to_string(),
                                    song_info,
                                    colors,
                                    _colors_alternate: colors_alternate,
                                    hits,
                                });
                            }
                            Err(e) => {
                                println!("WARNING: while scanning songs an error occurred for path \"{}\": {:#?}", path.display(), e);
                            }
                        }
                    } else if path.is_dir() {
                        // Scan directtory recursively
                        self.scan_subdir(path)?;
                    }
                }
                _ => {}
            }
        }
        Ok(())
    }
}
