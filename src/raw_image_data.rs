use sfml::{graphics::Image, system::Vector2u};

#[derive(Debug, Clone)]
pub struct RawImageData {
    size: Vector2u,
    pixels: Vec<u8>,
}

impl From<Image> for RawImageData {
    fn from(value: Image) -> Self {
        RawImageData {
            size: value.size(),
            pixels: value.pixel_data().into(),
        }
    }
}
pub enum RawImageDataIntoError {
    CreateImageError,
    RawImageDataFormatError,
}

impl TryInto<Image> for RawImageData {
    type Error = RawImageDataIntoError;
    fn try_into(self) -> Result<Image, Self::Error> {
        unsafe {
            if self.pixels.len() == 4 * (self.size.x as usize) * (self.size.y as usize) {
                Ok(
                    Image::create_from_pixels(self.size.x, self.size.y, &self.pixels)
                        .ok_or(RawImageDataIntoError::CreateImageError)?,
                )
            } else {
                Err(RawImageDataIntoError::RawImageDataFormatError)
            }
        }
    }
}
