use std::time::{Duration, SystemTime};

use sfml::{
    graphics::{
        Color, Font, RectangleShape, RenderTarget, RenderWindow, Shape, Text, Texture,
        Transformable,
    },
    system::Vector2f,
    SfBox,
};

use crate::{animate::Animate, interpolate::EaseType};

const TOAST_WIDTH: f32 = 300.0;
const TOAST_HEIGHT: f32 = 50.0;
const TOAST_GAP: f32 = 10.0;
const TOAST_MARGIN_LEFT: f32 = 10.0;
const TOAST_MARGIN_TOP: f32 = 10.0;
const TOAST_ANIMATION_IN_MS: u64 = 500;
const TOAST_ANIMATION_OUT_MS: u64 = 300;
const TOAST_ANIMATION_MOVE_MS: u64 = 300;

pub struct Toast {
    background_color: Color,
    text: String,
    spawn_time: SystemTime,
    duration: Duration,
    animation_in: Animate<f64>,
    animation_out: Animate<f64>,
    animation_move: Animate<f64>,
}

impl Toast {
    pub fn end_time(&self) -> SystemTime {
        self.spawn_time + self.duration
    }

    pub fn new(text: &str, background_color: Color, duration: Duration) -> Toast {
        Self {
            background_color,
            text: text.to_string(),
            spawn_time: SystemTime::now(),
            duration,
            animation_in: Animate::default(),
            animation_out: Animate::default(),
            animation_move: Animate::default(),
        }
    }

    pub fn regular(text: &str) -> Toast {
        Self::new(text, Color::WHITE, Duration::from_secs(5))
    }

    pub fn short(text: &str) -> Toast {
        Self::new(text, Color::WHITE, Duration::from_millis(1500))
    }

    pub fn success(text: &str) -> Toast {
        Self::new(text, Color::GREEN, Duration::from_secs(5))
    }

    pub fn success_short(text: &str) -> Toast {
        Self::new(text, Color::GREEN, Duration::from_millis(1500))
    }

    pub fn error(text: &str) -> Toast {
        Self::new(text, Color::RED, Duration::from_secs(5))
    }

    pub fn warning(text: &str) -> Toast {
        Self::new(text, Color::YELLOW, Duration::from_secs(5))
    }
}

pub struct ToastManager {
    toasts: Vec<Toast>,
    toast_texture: SfBox<Texture>,
}

impl ToastManager {
    fn get_toast_y_position(index: usize) -> f64 {
        let index_float = index as f64;
        let toast_size_with_margin = (TOAST_HEIGHT + TOAST_GAP) as f64;
        let initial_y = TOAST_MARGIN_TOP as f64;
        (index_float * toast_size_with_margin) + initial_y
    }

    pub fn new() -> Self {
        Self {
            toasts: Vec::new(),
            toast_texture: Texture::from_file("res/texture/toast_notification.png").unwrap(), // TODO: handle errors
        }
    }

    pub fn update(&mut self) {
        let current_time = SystemTime::now();
        let toast_count_before = self.toasts.len();
        self.toasts.retain(|toast| toast.end_time() > current_time);
        if toast_count_before != self.toasts.len() {
            for (i, toast) in self.toasts.iter_mut().enumerate() {
                toast.animation_move.animate_to(
                    Self::get_toast_y_position(i),
                    Duration::from_millis(TOAST_ANIMATION_MOVE_MS),
                    current_time,
                    EaseType::OutSine,
                )
            }
        }
    }

    pub fn draw(&mut self, window: &mut RenderWindow, font: &Font) {
        for toast in &self.toasts {
            let toast_position = Vector2f::new(
                TOAST_MARGIN_LEFT
                    + (toast.animation_in.get_current_value()
                        + toast.animation_out.get_current_value()) as f32,
                toast.animation_move.get_current_value() as f32,
            );

            let mut rectangle = RectangleShape::new();
            rectangle.set_position(toast_position);
            rectangle.set_size(Vector2f::new(TOAST_WIDTH, TOAST_HEIGHT));
            rectangle.set_fill_color(toast.background_color);
            rectangle.set_texture(&self.toast_texture, true);
            window.draw(&rectangle);

            let mut text = Text::new(toast.text.as_str(), font, 14);
            text.set_fill_color(Color::BLACK);
            text.set_position(toast_position + Vector2f::new(5.0, 5.0));
            window.draw(&text);
        }
    }

    pub fn add_toast(&mut self, mut toast: Toast) {
        let toast_count = self.toasts.len();
        toast.animation_in.start_at_time(
            -(TOAST_WIDTH + TOAST_MARGIN_LEFT) as f64,
            0.0,
            Duration::from_millis(TOAST_ANIMATION_IN_MS),
            toast.spawn_time,
            EaseType::OutExpo,
        );
        toast.animation_out.start_at_time(
            0.0,
            -(TOAST_WIDTH + TOAST_MARGIN_LEFT) as f64,
            Duration::from_millis(TOAST_ANIMATION_OUT_MS),
            toast.end_time() - Duration::from_millis(500),
            EaseType::OutExpo,
        );
        toast.animation_move = Animate::still(Self::get_toast_y_position(toast_count));
        self.toasts.push(toast);
    }

    pub fn active_toast_count(&self) -> usize {
        self.toasts.len()
    }
}
