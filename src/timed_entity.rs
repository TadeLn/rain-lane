pub trait TimedEntity {
    fn get_time(&self) -> f64;
}

pub fn get_t(begin: f64, end: f64, point: f64) -> f64 {
    (point - begin) / (end - begin)
}

pub fn find_neighboring_points<T>(keypoints: &[T], point_time: f64) -> (&T, &T, f64)
where
    T: TimedEntity,
{
    let (previous_index, next_index) = find_neighboring_point_indeces(keypoints, point_time);
    let previous = &keypoints[previous_index];
    let next = &keypoints[next_index];
    let t = get_t(previous.get_time(), next.get_time(), point_time);
    return (previous, next, t);
}

pub fn find_neighboring_point_indeces<T>(keypoints: &[T], point_time: f64) -> (usize, usize)
where
    T: TimedEntity,
{
    let mut next_index = keypoints.len() - 1;
    for (i, point) in keypoints.iter().enumerate() {
        if point.get_time() >= point_time {
            next_index = i;
            break;
        }
    }
    let next_index = next_index;
    let previous_index = if next_index == 0 { 0 } else { next_index - 1 };
    return (previous_index, next_index);
}
