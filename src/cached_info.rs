use colors_transform::Hsl;
use sfml::graphics::Color;

use crate::chart::Chart;

pub struct CachedInfo {
    canvas_colors: Vec<Color>,
}

impl CachedInfo {
    pub fn new() -> Self {
        CachedInfo {
            canvas_colors: vec![],
        }
    }
    pub fn get_canvas_color(&self, index: usize) -> Color {
        self.canvas_colors[index]
    }
    pub fn generate_canvas_colors(&mut self, canvas_count: usize) {
        self.canvas_colors = vec![];
        for i in 0..canvas_count {
            let canvas_color = colors_transform::Color::to_rgb(&Hsl::from(
                (i as f32 / canvas_count as f32) * 360.0,
                100.0,
                50.0,
            ));
            let color = Color::rgba(
                colors_transform::Color::get_red(&canvas_color) as u8,
                colors_transform::Color::get_green(&canvas_color) as u8,
                colors_transform::Color::get_blue(&canvas_color) as u8,
                (256.0 / ((canvas_count + 1) as f64)) as u8,
            );
            self.canvas_colors.push(color);
        }
    }
    pub fn from_chart(chart: &Chart) -> CachedInfo {
        let mut cached_info = Self::new();
        cached_info.generate_canvas_colors(chart.canvas_moves.len());
        cached_info
    }
}
