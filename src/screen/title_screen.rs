use std::path::Path;

use crate::game::{Game, GAME_VERSION_STRING};
use crate::translate;
use sfml::{
    graphics::{Color, RenderTarget, Text, TextStyle, Transformable},
    system::Vector2f,
    window::{Event, Key},
};

use super::{
    game_screen::GameScreen,
    gui_screen::{GuiScreen, Guiable},
    settings_screen::SettingsScreen,
    song_list_screen::SongListScreen,
    Screen,
};

#[derive(Clone, Copy)]
pub enum GuiOption {
    SongList,
    Quickplay,
    Settings,
    Quit,
}

impl Guiable for GuiOption {
    fn label(&self) -> String {
        match self {
            Self::SongList => translate!("screen.title.song_list"),
            Self::Quickplay => translate!("screen.title.quickplay"),
            Self::Settings => translate!("screen.title.settings"),
            Self::Quit => translate!("screen.title.quit"),
        }
        .to_string()
    }
}

pub struct TitleScreen {
    gui_screen: GuiScreen<GuiOption>,
}

impl TitleScreen {
    pub fn new() -> Self {
        TitleScreen {
            gui_screen: GuiScreen::new(
                &translate!("screen.title.title"),
                Color::BLUE,
                &[
                    GuiOption::SongList,
                    GuiOption::Quickplay,
                    GuiOption::Settings,
                    GuiOption::Quit,
                ],
            ),
        }
    }
}

impl Screen for TitleScreen {
    fn draw(&mut self, game: &mut Game) -> Result<(), super::ScreenDrawError> {
        game.window.set_view(&game.get_default_view());
        self.gui_screen.draw(game);

        const VERSION_TEXT_MARGIN: f32 = 4.0;
        let window_size = game.window.size();

        let mut version_text = Text::new(
            format!("Rainlane v{}", GAME_VERSION_STRING).as_str(),
            &game.monospace_font,
            12,
        );
        version_text.set_style(TextStyle::BOLD);

        let bounds = version_text.local_bounds();
        version_text.set_origin(Vector2f::new(bounds.width, bounds.height));

        version_text.set_fill_color(Color::WHITE);
        version_text.set_outline_color(Color::BLACK);
        version_text.set_outline_thickness(1.0);
        version_text.set_position(Vector2f::new(
            window_size.x as f32 - VERSION_TEXT_MARGIN,
            window_size.y as f32 - VERSION_TEXT_MARGIN,
        ));
        game.window.draw(&version_text);
        Ok(())
    }
    fn handle_event(
        &mut self,
        game: &mut crate::game::Game,
        event: Event,
    ) -> Result<(), super::ScreenEventError> {
        self.gui_screen.handle_event(event);
        match event {
            Event::KeyPressed { code, .. } => match code {
                Key::Enter => match self.gui_screen.selected {
                    0 => {
                        game.push_screen(Box::new(SongListScreen::new()));
                    }
                    1 => {
                        game.push_screen(Box::new(GameScreen::new(
                            Path::new(game.settings.songs_directory.as_str())
                                .join(Path::new("Hunter Milo - Toys")),
                            Path::new("chart_ez.json").to_owned(),
                        )));
                    }
                    2 => {
                        game.push_screen(Box::new(SettingsScreen::new()));
                    }
                    3 => {
                        game.window.close();
                    }
                    _ => {}
                },
                _ => {}
            },
            _ => {}
        }
        Ok(())
    }
}
