use sfml::{
    graphics::{Color, RenderTarget, Text, TextStyle, Transformable},
    system::Vector2f,
    window::{Event, Key},
};

use crate::game::Game;

use super::settings_screen::GuiOption;

pub trait Guiable: Clone {
    fn label(&self) -> String;
}

pub struct GuiScreen<GuiOption: Guiable> {
    pub title: String,
    pub title_color: Color,
    pub options: Vec<GuiOption>,
    pub selected: isize,
}

impl GuiScreen<GuiOption> {
    pub fn selected_option(&self) -> &GuiOption {
        &self.options[self.selected as usize]
    }
}

impl<GuiOption: Guiable> GuiScreen<GuiOption> {
    pub fn new(title: &str, title_color: Color, options: &[GuiOption]) -> Self {
        let mut options_vec = vec![];
        for option in options {
            options_vec.push(option.to_owned());
        }
        GuiScreen {
            title: title.to_string(),
            title_color,
            options: options_vec,
            selected: 0,
        }
    }
    pub fn draw(&mut self, game: &mut Game) {
        game.window.clear(Color::BLACK);

        let window_size = game.window.size();

        let mut title_text = Text::new(self.title.as_str(), &game.font, 70);
        title_text.set_fill_color(Color::WHITE);
        title_text.set_outline_color(self.title_color);
        title_text.set_outline_thickness(2.0);
        title_text.set_style(TextStyle::BOLD);
        let title_text_bounds = title_text.global_bounds();
        title_text.set_position(Vector2f::new(
            (window_size.x as f32 - title_text_bounds.size().x) * 0.5,
            30.0,
        ));
        game.window.draw(&title_text);

        let mut option_text = Text::new("Placeholder", &game.font, 30);
        option_text.set_position(Vector2f::new(30.0, 150.0));
        option_text.set_outline_color(Color::rgb(128, 128, 128));
        option_text.set_outline_thickness(1.0);
        for (i, gui_option) in self.options.iter().enumerate() {
            option_text.set_string(&gui_option.label());

            if i as isize == self.selected {
                option_text.set_style(TextStyle::UNDERLINED);
                option_text.set_fill_color(Color::CYAN);
            } else {
                option_text.set_style(TextStyle::default());
                option_text.set_fill_color(Color::WHITE);
            }
            game.window.draw(&option_text);
            option_text.move_(Vector2f::new(0.0, 50.0));
        }
    }
    pub fn handle_event(&mut self, event: Event) {
        match event {
            Event::KeyPressed { code, .. } => match code {
                Key::Down => {
                    self.selected += 1;
                    if self.selected >= self.options.len() as isize {
                        self.selected = 0;
                    }
                }
                Key::Up => {
                    self.selected -= 1;
                    if self.selected < 0 {
                        self.selected = self.options.len() as isize - 1;
                    }
                }
                _ => {}
            },
            _ => {}
        }
    }
}
