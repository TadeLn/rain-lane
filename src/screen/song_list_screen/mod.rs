pub mod draw;

use std::{
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
    thread::JoinHandle,
    time::{Duration, SystemTime},
};

use rand::{rngs::ThreadRng, Rng};

use sfml::{
    audio::{Music, SoundSource, TimeSpan},
    graphics::{Color, Image, IntRect, RenderTarget, Texture},
    system::{Time, Vector2u},
    window::{Event, Key},
    SfBox,
};

use self::draw::SONG_ENTRY_INDENT_ANIMATION_DURATION;

use super::{game_screen::GameScreen, Screen};
use crate::{
    animate::Animate,
    chart::{color::Color as ChartColor, theme::Theme, Chart},
    game::{try_open_file, Game, PathError},
    interpolate::EaseType,
    particle::{
        circle_particle::CircleParticle,
        particle_engine::{AnyParticle, ParticleEngine},
        Particle,
    },
    raw_image_data::RawImageData,
    song_cache::{SongEntry, SongListCache},
};

pub const SONG_LIST_NO_ICONS: bool = false;
pub const PREVIEW_MUSIC_FADE_OUT_MS: u64 = 100;
pub const PREVIEW_MUSIC_FADE_IN_MS: u64 = 300;
pub const DEBUG_SIMULATE_LONG_IMAGE_LOADS: bool = false;
pub const MUSIC_FILENAMES: [&str; 3] = ["song.flac", "song.wav", "song.ogg"];
pub const IMAGE_FILENAMES: [&str; 3] = ["image.png", "image.jpg", "image.jpeg"];

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum MusicChangeAnimationState {
    FadeIn,
    FadeOut,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum SortType {
    Title,
    Artist,
    Path,
}

#[derive(Debug, Clone)]
pub enum SongImageState {
    NotLoaded,
    Missing,
    Loaded(SfBox<Texture>),
}

#[derive(Debug, Clone)]
pub struct SongImageToLoad {
    song_index: usize,
    image_data: Option<RawImageData>,
}

pub struct SongListScreen<'a> {
    selected_song: isize,
    selected_difficulty: isize,
    song_count: isize,
    difficulty_count: isize,

    // Song image textures that are either:
    // - NotLoaded (and need to be loaded from a file)
    // - Missing (the file doesn't exist)
    // - Loaded (the file exists and the texture is loaded)
    song_textures: Vec<SongImageState>,

    // Song images that need to be loaded from pixels in memory to a texture
    song_images_to_load: Arc<Mutex<Vec<SongImageToLoad>>>,

    // Song images that need to be loaded from a file to the memory
    // Specifically, this vector stores indeces of song entries where the image needs to be loaded
    song_images_requested: Vec<usize>,

    // Thread for loading song images
    // There should be at most one loading thread at once
    song_image_loading_thread: Option<JoinHandle<Result<(), PathError>>>,

    rng: ThreadRng,
    preview_music: Option<Music<'a>>,
    particle_engine: ParticleEngine,
    old_window_size: Vector2u,

    camera_position_animation: Animate<f64>,
    song_entries_indent_animations: Vec<Animate<f64>>,
    song_entries_color_animations: Vec<Animate<ChartColor, ChartColor<f64>>>,
    song_image_x_offset_animation: Animate<f64>,
    song_image_y_offset_animation: Animate<f64>,
    song_image_rotation_animation: Animate<f64>,
    song_image_transparency_animation: Animate<f64>,
    background_color_animation: Animate<ChartColor, ChartColor<f64>>,
    active_color_animation: Animate<ChartColor, ChartColor<f64>>,
    particle_color_animation: Animate<ChartColor, ChartColor<f64>>,
    music_change_animation_state: MusicChangeAnimationState,
    music_change_animation: Animate<f64>,
}

impl<'a> SongListScreen<'_> {
    pub fn new() -> SongListScreen<'a> {
        SongListScreen {
            selected_song: 0,
            selected_difficulty: 0,
            song_count: 0,
            difficulty_count: 0,
            song_textures: vec![],
            song_images_to_load: Arc::new(Mutex::new(Vec::new())),
            song_images_requested: Vec::new(),
            song_image_loading_thread: None,
            rng: rand::thread_rng(),
            preview_music: None,
            particle_engine: ParticleEngine::new(),
            old_window_size: Vector2u::new(1, 1),

            camera_position_animation: Animate::still(0.0),
            song_entries_indent_animations: vec![],
            song_entries_color_animations: vec![],
            song_image_x_offset_animation: Animate::still(0.0),
            song_image_y_offset_animation: Animate::still(0.0),
            song_image_rotation_animation: Animate::still(0.0),
            song_image_transparency_animation: Animate::still(1.0),
            background_color_animation: Animate::still(Color::WHITE.into()),
            active_color_animation: Animate::still(Color::WHITE.into()),
            particle_color_animation: Animate::still(Color::WHITE.into()),
            music_change_animation_state: MusicChangeAnimationState::FadeIn,
            music_change_animation: Animate::still(0.0),
        }
    }

    fn set_selected_song(&mut self, game: &Game, to: isize) {
        self.animate_end_indent(self.selected_song as usize);
        self.selected_song = to;
        if self.selected_song < 0 {
            self.selected_song = 0;
        }
        if self.selected_song >= self.song_count {
            self.selected_song = self.song_count - 1;
        }
        self.update_selected_song(game);
    }

    fn change_selected_song(&mut self, game: &Game, by: isize) {
        self.animate_end_indent(self.selected_song as usize);
        self.selected_song += by;
        if self.selected_song < 0 {
            self.selected_song = self.song_count - 1;
        }
        if self.selected_song >= self.song_count {
            self.selected_song = 0;
        }
        self.update_selected_song(game);
    }

    fn change_selected_difficulty(&mut self, by: isize) {
        self.selected_difficulty =
            (self.selected_difficulty + by).clamp(0, (self.difficulty_count - 1).max(0));
    }

    pub fn unload_song_image(&mut self, song_image_index: usize) {
        self.song_textures[song_image_index] = SongImageState::NotLoaded;
        self.song_images_requested
            .retain(|x| *x != song_image_index);
    }

    pub fn request_load_song_image(&mut self, song_image_index: isize) {
        if !self
            .song_images_requested
            .contains(&(song_image_index as usize))
        {
            if let SongImageState::NotLoaded = self.song_textures[song_image_index as usize] {
                self.song_images_requested.push(song_image_index as usize);
            }
        }
    }

    // This is executed on another thread
    pub fn load_song_image(
        song_index: usize,
        song_images_to_load: Arc<Mutex<Vec<SongImageToLoad>>>,
        song_list_cache: Arc<Mutex<SongListCache>>,
    ) -> Result<(), PathError> {
        let path = {
            let song_entry = &song_list_cache.lock().unwrap().songs[song_index];
            song_entry.get_path()
        };

        if DEBUG_SIMULATE_LONG_IMAGE_LOADS {
            std::thread::sleep(Duration::from_millis(500));
        }

        let song_image_option = if SONG_LIST_NO_ICONS {
            None
        } else {
            try_open_file(
                &path,
                Vec::from(IMAGE_FILENAMES),
                |item_path| -> Option<Image> { Image::from_file(item_path) },
            )?
        };
        song_images_to_load.lock().unwrap().push(SongImageToLoad {
            song_index,
            image_data: if let Some(image) = song_image_option {
                Some(image.into())
            } else {
                None
            },
        });
        Ok(())
    }

    pub fn update_song_list(&mut self, game: &Game) {
        self.song_textures.clear();
        self.song_entries_indent_animations.clear();
        self.song_entries_color_animations.clear();
        {
            let song_list_cache = game.song_list_cache.lock().unwrap();
            self.song_count = song_list_cache.songs.len() as isize;
            for _ in &song_list_cache.songs {
                self.song_textures.push(SongImageState::NotLoaded);
                self.song_entries_indent_animations.push(Default::default());
                self.song_entries_color_animations.push(Default::default());
            }
        }
        self.set_selected_song(game, self.selected_song);
    }

    pub fn update_load_images(&mut self, game: &Game) {
        if self.song_images_requested.is_empty() {
            return;
        }
        self.song_images_requested.retain(|x| {
            if let SongImageState::NotLoaded = self.song_textures[*x] {
                true
            } else {
                false
            }
        });
        if self.song_images_requested.is_empty() {
            return;
        }
        if let Some(thread) = &self.song_image_loading_thread {
            if thread.is_finished() {
                self.song_image_loading_thread = None;
            }
        } else {
            let song_index = self.song_images_requested[0];
            let song_images_to_load = self.song_images_to_load.clone();
            let song_list_cache = game.song_list_cache.clone();
            self.song_image_loading_thread = Some(std::thread::spawn(move || {
                Self::load_song_image(song_index, song_images_to_load, song_list_cache)
            }));
        }
    }

    fn sort_songs(&mut self, game: &mut Game, sort_type: SortType) -> Result<(), PathError> {
        game.song_list_cache
            .lock()
            .unwrap()
            .songs
            .sort_by(match sort_type {
                SortType::Title => {
                    |a: &SongEntry, b: &SongEntry| a.song_info.title.cmp(&b.song_info.title)
                }
                SortType::Artist => |a: &SongEntry, b: &SongEntry| {
                    a.song_info
                        .song_credits
                        .artist
                        .cmp(&b.song_info.song_credits.artist)
                },
                SortType::Path => |a: &SongEntry, b: &SongEntry| a.path.cmp(&b.path),
            });
        self.update_song_list(game);
        Ok(())
    }

    fn update_selected_song(&mut self, game: &Game) {
        if let Some(song_entry) = game
            .song_list_cache
            .lock()
            .unwrap()
            .songs
            .get(self.selected_song as usize)
        {
            self.difficulty_count = song_entry.song_info.charts.len() as isize;
        } else {
            self.difficulty_count = 0;
        }
        self.selected_difficulty = self.selected_difficulty.clamp(0, self.difficulty_count);
        self.animate_begin_indent(self.selected_song as usize);
        self.animate_to_selected_song();
        self.animate_song_image();
        self.animate_color_theme(game);
        self.animate_song_entry_colors(game);
        self.start_reloading_preview_music();
    }

    fn animate_begin_indent(&mut self, song_entry_index: usize) {
        if song_entry_index >= self.song_entries_indent_animations.len() {
            return;
        }

        self.song_entries_indent_animations[song_entry_index].animate_now_to(
            40.0,
            Duration::from_millis(SONG_ENTRY_INDENT_ANIMATION_DURATION),
            EaseType::OutQuad,
        );
    }

    fn animate_end_indent(&mut self, song_entry_index: usize) {
        if song_entry_index >= self.song_entries_indent_animations.len() {
            return;
        }

        self.song_entries_indent_animations[song_entry_index].animate_now_to(
            0.0,
            Duration::from_millis(SONG_ENTRY_INDENT_ANIMATION_DURATION),
            EaseType::OutQuad,
        );
    }

    fn animate_to_selected_song(&mut self) {
        self.camera_position_animation.animate_now_to(
            self.selected_song as f64,
            Duration::from_millis(1000),
            EaseType::OutExpo,
        )
    }

    fn animate_song_image(&mut self) {
        let duration = self.rng.gen_range(200..400);
        let mut generate_random = |from: f64, to| -> f64 {
            self.rng.gen_range(from..=to) * if self.rng.gen_bool(0.5) { 1.0 } else { -1.0 }
        };

        self.song_image_x_offset_animation.animate_now(
            generate_random(30.0, 60.0),
            generate_random(0.0, 10.0),
            Duration::from_millis(duration),
            EaseType::OutQuad,
        );
        self.song_image_y_offset_animation.animate_now(
            generate_random(30.0, 60.0),
            generate_random(0.0, 10.0),
            Duration::from_millis(duration),
            EaseType::OutQuad,
        );
        self.song_image_rotation_animation.animate_now(
            generate_random(10.0, 20.0),
            generate_random(4.0, 8.0),
            Duration::from_millis(self.rng.gen_range(200..400)),
            EaseType::OutQuad,
        );
        self.song_image_transparency_animation.animate_now(
            0.0,
            1.0,
            Duration::from_millis(500),
            EaseType::OutQuad,
        )
    }

    fn animate_color_theme(&mut self, game: &Game) {
        let theme = if let Some(song_entry) = &game
            .song_list_cache
            .lock()
            .unwrap()
            .songs
            .get(self.selected_song as usize)
        {
            song_entry.colors.clone()
        } else {
            Theme::new()
        };
        self.background_color_animation.animate_now_to(
            theme.get_background_color(),
            Duration::from_millis(200),
            EaseType::OutQuad,
        );
        self.active_color_animation.animate_now_to(
            theme.get_note_color(),
            Duration::from_millis(200),
            EaseType::OutQuad,
        );
        self.particle_color_animation.animate_now_to(
            theme.get_particle_color(),
            Duration::from_millis(200),
            EaseType::OutQuad,
        );
    }

    fn animate_song_entry_colors(&mut self, game: &Game) {
        let theme = if let Some(song_entry) = &game
            .song_list_cache
            .lock()
            .unwrap()
            .songs
            .get(self.selected_song as usize)
        {
            song_entry.colors.clone()
        } else {
            Theme::new()
        };
        for i in 0..self.song_entries_color_animations.len() {
            self.song_entries_color_animations[i].animate_now_to(
                if i as isize == self.selected_song {
                    theme.get_note_color()
                } else {
                    Color::rgba(240, 240, 250, 128).into()
                },
                Duration::from_millis(500),
                EaseType::OutQuad,
            )
        }
    }

    fn start_reloading_preview_music(&mut self) {
        self.music_change_animation_state = MusicChangeAnimationState::FadeOut;
        self.music_change_animation.animate_now_to(
            0.0,
            Duration::from_millis(PREVIEW_MUSIC_FADE_OUT_MS),
            EaseType::Linear,
        );
    }

    fn swap_preview_music(&mut self, game: &Game) -> Result<(), PathError> {
        let mut music_option = None;
        if let Some(song_entry) = &game
            .song_list_cache
            .lock()
            .unwrap()
            .songs
            .get(self.selected_song as usize)
        {
            music_option = try_open_file(
                &PathBuf::from(&song_entry.path),
                Vec::from(MUSIC_FILENAMES),
                |item_path| -> Option<Music> { Music::from_file(item_path) },
            )?;

            if let Some(music) = &mut music_option {
                let offset = if let Some(preview_start) = song_entry.song_info.preview_start {
                    Time::milliseconds(preview_start as i32)
                } else {
                    Time::milliseconds(music.duration().as_milliseconds() * 2 / 3)
                };

                music.set_volume(0.0);
                music.set_loop_points(TimeSpan {
                    offset,
                    length: Time::milliseconds(song_entry.song_info.preview_duration as i32),
                });
                music.set_looping(true);
                music.play();
                music.set_playing_offset(offset);
            }
        }
        self.preview_music = music_option;

        self.music_change_animation_state = MusicChangeAnimationState::FadeIn;
        self.music_change_animation.animate_now(
            0.0,
            1.0,
            Duration::from_millis(PREVIEW_MUSIC_FADE_IN_MS),
            EaseType::Linear,
        );
        Ok(())
    }

    fn add_background_particle(&mut self, game: &mut Game) {
        let window_size = game.window.size();
        let x = self.rng.gen::<f64>() * (window_size.x as f64);
        let y = self.rng.gen::<f64>() * (window_size.y as f64);
        let now = SystemTime::now();
        self.particle_engine
            .particles
            .push(AnyParticle::CircleParticle(
                CircleParticle::new_background_particle(
                    now,
                    x,
                    y,
                    self.particle_color_animation.get_value_at_time(now),
                ),
            ));
    }

    fn update_particle_colors(&mut self) {
        let now = SystemTime::now();
        for particle in &mut self.particle_engine.particles {
            particle.set_colors(CircleParticle::get_colors_for_background_particle(
                self.particle_color_animation.get_value_at_time(now),
            ))
        }
    }

    fn load_song_images_as_textures(&mut self) {
        // Load images as textures
        while let Some(song_image_to_load) = self.song_images_to_load.lock().unwrap().pop() {
            let mut texture_option = None;
            if let Some(raw_image_data) = song_image_to_load.image_data {
                if let Ok(image) = raw_image_data.try_into() {
                    texture_option = Texture::new();
                    if let Some(texture) = &mut texture_option {
                        match texture.load_from_image(
                            &image,
                            IntRect::new(0, 0, image.size().x as i32, image.size().y as i32),
                        ) {
                            Ok(_) => {}
                            Err(_) => texture_option = None,
                        };
                    }
                }
            }
            self.song_textures[song_image_to_load.song_index] = match texture_option {
                Some(texture) => SongImageState::Loaded(texture),
                None => SongImageState::Missing,
            };
        }
    }
}

impl Screen for SongListScreen<'_> {
    fn open(&mut self, game: &mut Game) -> Result<(), super::ScreenOpenError> {
        self.old_window_size = game.window.size().clone();
        if game.song_list_cache.lock().unwrap().songs.is_empty() {
            game.rescan_songs()?;
        }
        self.update_song_list(game);
        Ok(())
    }

    fn cover(&mut self, _game: &mut Game) -> Result<(), super::ScreenCoverError> {
        if let Some(music) = &mut self.preview_music {
            music.stop();
        }
        Ok(())
    }

    fn uncover(&mut self, _game: &mut Game) -> Result<(), super::ScreenUncoverError> {
        if let Some(_) = &mut self.preview_music {
            self.start_reloading_preview_music();
        }
        Ok(())
    }

    fn update(&mut self, game: &mut Game) -> Result<(), super::ScreenUpdateError> {
        if self.music_change_animation_state == MusicChangeAnimationState::FadeOut
            && self.music_change_animation.get_current_value() <= 0.0
        {
            self.swap_preview_music(game)?;
        }
        if let Some(music) = &mut self.preview_music {
            music.set_volume(
                (self.music_change_animation.get_current_value() * game.settings.preview_volume)
                    as f32,
            )
        }
        Ok(())
    }

    fn draw(&mut self, game: &mut Game) -> Result<(), super::ScreenDrawError> {
        let window_size = game.window.size();

        let animated_selected_song = self
            .camera_position_animation
            .get_value_at_time(SystemTime::now());

        self.update_load_images(game);

        game.window
            .clear(self.background_color_animation.get_current_value().into());

        game.window.set_view(&game.get_default_view());

        if self.particle_engine.particles.len() < 20 {
            self.add_background_particle(game);
        }
        self.update_particle_colors();
        self.particle_engine.draw(&mut game.window, &game.font)?;

        self.load_song_images_as_textures();

        self.draw_current_song_image(&mut game.window, window_size);
        Self::setup_camera_songlist(&mut game.window, window_size, animated_selected_song);
        self.draw_songlist(
            &mut game.window,
            &game.font,
            &game.monospace_font,
            game.song_list_cache.clone(),
        );
        Self::setup_camera_default(game);

        game.add_debug_text(self.get_chartinfo_text(game.song_list_cache.clone()));

        Ok(())
    }

    fn handle_event(
        &mut self,
        game: &mut Game,
        event: sfml::window::Event,
    ) -> Result<(), super::ScreenEventError> {
        match event {
            Event::KeyPressed { code, ctrl, .. } => match code {
                Key::Up => self.change_selected_song(game, -1),
                Key::Down => self.change_selected_song(game, 1),
                Key::PageUp => self.change_selected_song(game, -5),
                Key::PageDown => self.change_selected_song(game, 5),
                Key::Home => self.set_selected_song(game, 0),
                Key::End => self.set_selected_song(game, self.song_count - 1),
                Key::Left => self.change_selected_difficulty(-1),
                Key::Right => self.change_selected_difficulty(1),
                Key::Enter => {
                    let song_entry_option = {
                        let song_cache = game.song_list_cache.lock().unwrap();
                        song_cache.songs.get(self.selected_song as usize).cloned()
                    };
                    if let Some(song_entry) = song_entry_option {
                        let selected_song_path = song_entry.path.clone();
                        let selected_chart_path = song_entry.song_info.charts
                            [self.selected_difficulty as usize]
                            .filename
                            .clone();
                        game.push_screen(Box::new(GameScreen::new(
                            Path::new(&selected_song_path).to_path_buf(),
                            Path::new(&selected_chart_path).to_owned(),
                        )));
                    }
                }
                Key::Escape => {
                    game.pop_screen();
                }
                Key::R => {
                    if ctrl {
                        game.rescan_songs()?;
                        self.update_song_list(game);
                    }
                }
                Key::F1 => {
                    self.sort_songs(game, SortType::Title)?;
                }
                Key::F2 => {
                    self.sort_songs(game, SortType::Artist)?;
                }
                Key::F4 => {
                    self.sort_songs(game, SortType::Path)?;
                }
                Key::F10 => {
                    let selected_song_path = game.song_list_cache.lock().unwrap().songs
                        [self.selected_song as usize]
                        .path
                        .clone();
                    let selected_chart_path = game.song_list_cache.lock().unwrap().songs
                        [self.selected_song as usize]
                        .song_info
                        .charts[0]
                        .filename
                        .clone();

                    if let Ok(chart) = Chart::from_file(
                        Path::new(&selected_song_path)
                            .join(selected_chart_path)
                            .to_string_lossy()
                            .to_string(),
                    ) {
                        chart.verify();
                    }
                }
                _ => {}
            },
            Event::Resized { width, height } => {
                for particle in &mut self.particle_engine.particles {
                    let old_particle_x = particle.get_x() / self.old_window_size.x as f64;
                    let old_particle_y = particle.get_y() / self.old_window_size.y as f64;
                    particle.set_x(old_particle_x * (width as f64));
                    particle.set_y(old_particle_y * (height as f64));
                }
                self.old_window_size = Vector2u::new(width, height);
            }
            Event::MouseWheelScrolled { delta, .. } => {
                self.change_selected_song(game, -delta as isize);
            }
            _ => {}
        }
        Ok(())
    }
}
