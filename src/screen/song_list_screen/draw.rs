use std::{
    sync::{Arc, Mutex},
    time::SystemTime,
};

use sfml::{
    graphics::{
        CircleShape, Color, Font, PrimitiveType, RectangleShape, RenderStates, RenderTarget,
        RenderWindow, Shape, Text, TextStyle, Transformable, Vertex, View,
    },
    system::{Vector2f, Vector2u},
};

use crate::{game::Game, song_cache::SongListCache, translate, translate_fmt};

use super::{SongImageState, SongListScreen};

pub const SONG_ENTRY_INDENT_ANIMATION_DURATION: u64 = 300;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_START: f64 = -200.0;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_END: f64 = 400.0;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_END_BOTTOM_OFFSET: f64 = 20.0;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT: f64 = 100.0;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_GAP: f64 = 10.0;
pub const SONG_ENTRY_BACKGROUND_RECTANGLE_INDENT: f64 = 25.0;
pub const SONG_ENTRY_BACKGROUND_GRADIENT_WIDTH: f64 = 300.0;
pub const SONG_ENTRY_IMAGE_WIDTH: f64 = SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT;
pub const SONG_ENTRY_IMAGE_HEIGHT: f64 = SONG_ENTRY_IMAGE_WIDTH;
pub const SONG_ENTRY_IMAGE_MARGIN: f64 = 10.0;
pub const SONG_ENTRY_IMAGE_TITLE_GAP: f64 = 10.0;
pub const SONG_ENTRY_TITLE_ARTIST_GAP: f64 = 15.0;
pub const SONG_CHART_CIRCLE_RADIUS: f64 = 15.0;
pub const SONG_CHART_CIRCLE_GAP: f64 = 5.0;
pub const DIFFICULTY_EZ_COLOR: Color = Color::rgb(87, 227, 198);
pub const DIFFICULTY_HD_COLOR: Color = Color::rgb(252, 187, 97);
pub const DIFFICULTY_IN_COLOR: Color = Color::rgb(253, 133, 98);
pub const DIFFICULTY_UNKNOWN_COLOR: Color = Color::rgb(200, 200, 200);
pub const CURRENT_SONG_IMAGE_WIDTH: f64 = 300.0;
pub const CURRENT_SONG_IMAGE_HEIGHT: f64 = CURRENT_SONG_IMAGE_WIDTH;

impl SongListScreen<'_> {
    pub fn draw_current_song_image(&self, window: &mut RenderWindow, window_size: Vector2u) {
        let current_song_image_scale = window_size.x.min(window_size.y) as f32 / 500.0;
        let mut current_song_image = RectangleShape::new();
        current_song_image.set_size(Vector2f::new(
            CURRENT_SONG_IMAGE_WIDTH as f32 * current_song_image_scale,
            CURRENT_SONG_IMAGE_HEIGHT as f32 * current_song_image_scale,
        ));
        current_song_image.set_origin(Vector2f::new(
            (CURRENT_SONG_IMAGE_WIDTH as f32 / 2.0) * current_song_image_scale,
            (CURRENT_SONG_IMAGE_HEIGHT as f32 / 2.0) * current_song_image_scale,
        ));
        current_song_image.set_position(Vector2f::new(
            window_size.x as f32
                - (CURRENT_SONG_IMAGE_WIDTH as f32 * current_song_image_scale * 0.6)
                + self.song_image_x_offset_animation.get_current_value() as f32,
            window_size.y as f32 / 2.0
                + self.song_image_y_offset_animation.get_current_value() as f32,
        ));
        current_song_image.set_outline_color(Color::WHITE);
        current_song_image.set_outline_thickness(10.0);
        current_song_image
            .set_rotation(self.song_image_rotation_animation.get_current_value() as f32);
        current_song_image.set_fill_color(Color::WHITE);
        window.draw(&current_song_image);

        let song_image_texture_option = &self.song_textures.get(self.selected_song as usize);

        if let Some(SongImageState::Loaded(texture)) = song_image_texture_option {
            current_song_image.set_fill_color(Color::rgba(
                255,
                255,
                255,
                (self.song_image_transparency_animation.get_current_value() * 255.0) as u8,
            ));
            current_song_image.set_texture(&texture, true);
        } else {
            current_song_image.set_fill_color(Color::rgba(
                192,
                192,
                192,
                (self.song_image_transparency_animation.get_current_value() * 255.0) as u8,
            ));
            current_song_image.disable_texture();
        }

        window.draw(&current_song_image);
    }

    fn draw_song_entry_background(
        &self,
        window: &mut RenderWindow,
        song_entry_number: usize,
        base_position: Vector2f,
    ) {
        let mut song_entry_background = vec![];
        let song_entry_background_color: Color = self.song_entries_color_animations
            [song_entry_number]
            .get_current_value()
            .into();
        let song_entry_background_color_gradient = {
            let mut color = song_entry_background_color.clone();
            color.a = 0;
            color
        };

        song_entry_background.push(Vertex::new(
            base_position + Vector2f::new(SONG_ENTRY_BACKGROUND_RECTANGLE_START as f32, 0.0),
            song_entry_background_color,
            Vector2f::new(0.0, 0.0),
        ));
        song_entry_background.push(Vertex::new(
            base_position
                + Vector2f::new(
                    SONG_ENTRY_BACKGROUND_RECTANGLE_START as f32,
                    SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT as f32,
                ),
            song_entry_background_color,
            Vector2f::new(0.0, 0.0),
        ));
        song_entry_background.push(Vertex::new(
            base_position + Vector2f::new(SONG_ENTRY_BACKGROUND_RECTANGLE_END as f32, 0.0),
            song_entry_background_color,
            Vector2f::new(0.0, 0.0),
        ));
        song_entry_background.push(Vertex::new(
            base_position
                + Vector2f::new(
                    (SONG_ENTRY_BACKGROUND_RECTANGLE_END
                        + SONG_ENTRY_BACKGROUND_RECTANGLE_END_BOTTOM_OFFSET)
                        as f32,
                    SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT as f32,
                ),
            song_entry_background_color,
            Vector2f::new(0.0, 0.0),
        ));
        song_entry_background.push(Vertex::new(
            base_position
                + Vector2f::new(
                    (SONG_ENTRY_BACKGROUND_RECTANGLE_END + SONG_ENTRY_BACKGROUND_GRADIENT_WIDTH)
                        as f32,
                    0.0,
                ),
            song_entry_background_color_gradient,
            Vector2f::new(0.0, 0.0),
        ));
        song_entry_background.push(Vertex::new(
            base_position
                + Vector2f::new(
                    (SONG_ENTRY_BACKGROUND_RECTANGLE_END
                        + SONG_ENTRY_BACKGROUND_RECTANGLE_END_BOTTOM_OFFSET
                        + SONG_ENTRY_BACKGROUND_GRADIENT_WIDTH) as f32,
                    SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT as f32,
                ),
            song_entry_background_color_gradient,
            Vector2f::new(0.0, 0.0),
        ));
        window.draw_primitives(
            &song_entry_background,
            PrimitiveType::TRIANGLE_STRIP,
            &RenderStates::default(),
        );
    }

    fn draw_song_entry_image(
        &self,
        window: &mut RenderWindow,
        song_entry_number: usize,
        base_position: Vector2f,
    ) {
        let mut song_entry_image = RectangleShape::new();
        song_entry_image.set_size(Vector2f::new(
            SONG_ENTRY_IMAGE_WIDTH as f32,
            SONG_ENTRY_IMAGE_HEIGHT as f32,
        ));
        song_entry_image
            .set_position(base_position + Vector2f::new(SONG_ENTRY_IMAGE_MARGIN as f32, 0.0));
        if let SongImageState::Loaded(texture) = &self.song_textures[song_entry_number] {
            song_entry_image.set_texture(&texture, true);
            song_entry_image.set_fill_color(Color::WHITE);
        } else {
            song_entry_image.disable_texture();
            song_entry_image.set_fill_color(Color::rgb(240, 240, 240));
        }
        window.draw(&song_entry_image);
    }

    pub fn draw_songlist(
        &mut self,
        window: &mut RenderWindow,
        general_font: &Font,
        chart_path_font: &Font,
        song_list_cache: Arc<Mutex<SongListCache>>,
    ) {
        let view = window.view();
        let view_center = view.center();
        let view_size = view.size();
        let view_position = view_center - (view_size * 0.5);

        let mut song_entry_title = Text::new("Title", general_font, 30);
        song_entry_title.set_style(TextStyle::BOLD);
        song_entry_title.set_outline_color(Color::rgba(0, 0, 0, 200));
        song_entry_title.set_outline_thickness(1.5);

        let mut song_entry_artist = Text::new("Artist", general_font, 20);
        song_entry_artist.set_style(TextStyle::ITALIC | TextStyle::BOLD);
        song_entry_artist.set_outline_color(Color::rgba(0, 0, 0, 200));
        song_entry_artist.set_outline_thickness(1.0);

        let mut song_entry_path = Text::new("Path", chart_path_font, 15);
        song_entry_path.set_style(TextStyle::BOLD);
        song_entry_path.set_fill_color(Color::rgb(200, 200, 200));
        song_entry_path.set_outline_color(Color::rgba(0, 0, 0, 200));
        song_entry_path.set_outline_thickness(1.0);

        let mut song_chart_circle = CircleShape::new(SONG_CHART_CIRCLE_RADIUS as f32, 30);
        song_chart_circle.set_origin(Vector2f::new(
            SONG_CHART_CIRCLE_RADIUS as f32,
            SONG_CHART_CIRCLE_RADIUS as f32,
        ));

        let mut song_chart_text = Text::new("99+", general_font, 18);
        song_chart_text.set_fill_color(Color::WHITE);

        for (song_entry_number, song_entry) in
            song_list_cache.lock().unwrap().songs.iter().enumerate()
        {
            let mut base_position = Vector2f::new(
                0.0,
                (song_entry_number as f64
                    * (SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT
                        + SONG_ENTRY_BACKGROUND_RECTANGLE_GAP)) as f32,
            );
            if (base_position.y as f64)
                < (view_position.y as f64) - SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT
            {
                self.unload_song_image(song_entry_number);
                continue;
            }
            if (base_position.y as f64) > (view_position.y + view_size.y) as f64 {
                self.unload_song_image(song_entry_number);
                continue;
            }

            base_position.x = ((song_entry_number as f64 * SONG_ENTRY_BACKGROUND_RECTANGLE_INDENT)
                + (self.song_entries_indent_animations[song_entry_number as usize]
                    .get_value_at_time(SystemTime::now()))) as f32;
            let base_position = base_position;

            self.draw_song_entry_background(window, song_entry_number, base_position);
            self.draw_song_entry_image(window, song_entry_number, base_position);
            self.request_load_song_image(song_entry_number as isize);

            song_entry_title.set_string(song_entry.song_info.title.as_str());
            song_entry_title.set_position(
                base_position
                    + Vector2f::new(
                        (SONG_ENTRY_IMAGE_MARGIN
                            + SONG_ENTRY_IMAGE_WIDTH
                            + SONG_ENTRY_IMAGE_TITLE_GAP) as f32,
                        5.0,
                    ),
            );
            window.draw(&song_entry_title);

            song_entry_artist.set_string(song_entry.song_info.song_credits.artist.as_str());
            song_entry_artist.set_origin(
                song_entry_artist
                    .local_bounds()
                    .size()
                    .cwise_mul(Vector2f::new(0.0, 0.5)),
            );
            song_entry_artist.set_position(
                base_position
                    + Vector2f::new(
                        (SONG_ENTRY_IMAGE_MARGIN
                            + SONG_ENTRY_IMAGE_WIDTH
                            + SONG_ENTRY_IMAGE_TITLE_GAP
                            + SONG_ENTRY_TITLE_ARTIST_GAP) as f32
                            + song_entry_title.local_bounds().size().x,
                        5.0 + (0.5 * song_entry_title.local_bounds().size().y),
                    ),
            );
            window.draw(&song_entry_artist);

            song_entry_path.set_position(
                base_position
                    + Vector2f::new(
                        (SONG_ENTRY_IMAGE_MARGIN
                            + SONG_ENTRY_IMAGE_WIDTH
                            + SONG_ENTRY_IMAGE_TITLE_GAP) as f32,
                        40.0,
                    ),
            );
            song_entry_path.set_string(song_entry.path.as_str());
            window.draw(&song_entry_path);

            song_chart_circle.set_position(
                base_position
                    + Vector2f::new(
                        (SONG_ENTRY_IMAGE_MARGIN
                            + SONG_ENTRY_IMAGE_WIDTH
                            + SONG_ENTRY_IMAGE_TITLE_GAP
                            + SONG_CHART_CIRCLE_RADIUS) as f32,
                        80.0 as f32,
                    ),
            );
            for (chart_number, chart) in song_entry.song_info.charts.iter().enumerate() {
                song_chart_circle.set_fill_color(
                    if song_entry_number as isize == self.selected_song
                        && chart_number as isize == self.selected_difficulty
                    {
                        Color::WHITE
                    } else {
                        match &chart.difficulty_type {
                            x if x == "ez" => DIFFICULTY_EZ_COLOR,
                            x if x == "hd" => DIFFICULTY_HD_COLOR,
                            x if x == "in" => DIFFICULTY_IN_COLOR,
                            _ => DIFFICULTY_UNKNOWN_COLOR,
                        }
                    },
                );
                window.draw(&song_chart_circle);

                song_chart_text.set_string(chart.get_difficulty_level_string().as_str());
                song_chart_text.set_origin(Vector2f::new(
                    song_chart_text.local_bounds().left
                        + (song_chart_text.local_bounds().width / 2.0),
                    song_chart_text.local_bounds().top
                        + (song_chart_text.local_bounds().height / 2.0),
                ));
                song_chart_text.set_position(song_chart_circle.position());
                song_chart_text.set_fill_color(
                    if song_entry_number as isize == self.selected_song
                        && chart_number as isize == self.selected_difficulty
                    {
                        match &chart.difficulty_type {
                            x if x == "ez" => DIFFICULTY_EZ_COLOR,
                            x if x == "hd" => DIFFICULTY_HD_COLOR,
                            x if x == "in" => DIFFICULTY_IN_COLOR,
                            _ => DIFFICULTY_UNKNOWN_COLOR,
                        }
                    } else {
                        Color::WHITE
                    },
                );
                window.draw(&song_chart_text);

                song_chart_circle.move_(Vector2f::new(
                    ((SONG_CHART_CIRCLE_RADIUS * 2.0) + SONG_CHART_CIRCLE_GAP) as f32,
                    0.0,
                ));
            }
        }
    }

    pub fn get_chartinfo_text(&self, song_list_cache: Arc<Mutex<SongListCache>>) -> String {
        let mutex_guard = song_list_cache.lock().unwrap();
        let song_entry_option = mutex_guard.songs.get(self.selected_song as usize);
        if let Some(song_entry) = song_entry_option {
            let song_info = &song_entry.song_info;
            let mut charts_string = String::new();
            for (i, chart_info) in song_info.charts.iter().enumerate() {
                charts_string += translate_fmt!(
                    "song_list.song_info.chart",
                    chart_info.difficulty_type,
                    chart_info.difficulty_level,
                    chart_info.charter,
                    chart_info.filename,
                    song_entry.hits[i]
                )
                .as_str()
            }
            translate_fmt!(
                "song_list.song_info.general",
                song_info.title,
                song_info.song_credits.artist,
                charts_string
            )
        } else {
            translate!("song_list.song_info.no_songs")
        }
    }

    pub fn setup_camera_songlist(
        window: &mut RenderWindow,
        window_size: Vector2u,
        animated_selected_song: f64,
    ) {
        window.set_view(&View::new(
            Vector2f::new(
                ((window_size.x as f64 / 2.0)
                    + (SONG_ENTRY_BACKGROUND_RECTANGLE_INDENT * animated_selected_song))
                    as f32,
                (((SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT + SONG_ENTRY_BACKGROUND_RECTANGLE_GAP)
                    * (animated_selected_song as f64))
                    + (SONG_ENTRY_BACKGROUND_RECTANGLE_HEIGHT / 2.0)) as f32,
            ),
            Vector2f::new(window_size.x as f32, window_size.y as f32),
        ));
    }

    pub fn setup_camera_default(game: &mut Game) {
        game.window.set_view(&game.get_default_view());
    }
}
