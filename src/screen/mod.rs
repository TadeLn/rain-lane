pub mod game_screen;
pub mod gui_screen;
pub mod results_screen;
pub mod settings_screen;
pub mod song_list_screen;
pub mod title_screen;

use std::time::SystemTimeError;

use sfml::window::Event;

use crate::{
    chart::JsonFileError,
    game::{Game, PathError},
    song_cache::SongScanError,
};

#[derive(Debug)]
pub enum ScreenInitError {}
#[derive(Debug)]
pub enum ScreenOpenError {
    MusicLoadError,
    PathError(PathError),
    JsonFileError(JsonFileError),
    SongScanError(SongScanError),
}

impl From<PathError> for ScreenOpenError {
    fn from(value: PathError) -> Self {
        Self::PathError(value)
    }
}

impl From<JsonFileError> for ScreenOpenError {
    fn from(value: JsonFileError) -> Self {
        Self::JsonFileError(value)
    }
}

impl From<SongScanError> for ScreenOpenError {
    fn from(value: SongScanError) -> Self {
        Self::SongScanError(value)
    }
}

#[derive(Debug)]
pub enum ScreenCloseError {}
#[derive(Debug)]
pub enum ScreenCoverError {}
#[derive(Debug)]
pub enum ScreenUncoverError {
    NoPreviousScreen,
}

#[derive(Debug)]
pub enum ScreenDrawError {
    SystemTimeError(SystemTimeError),
}

impl From<SystemTimeError> for ScreenDrawError {
    fn from(value: SystemTimeError) -> Self {
        Self::SystemTimeError(value)
    }
}

#[derive(Debug)]
pub enum ScreenUpdateError {
    PathError(PathError),
}

impl From<PathError> for ScreenUpdateError {
    fn from(value: PathError) -> Self {
        Self::PathError(value)
    }
}
#[derive(Debug)]
pub enum ScreenEventError {
    JsonFileError(JsonFileError),
    SystemTimeError(SystemTimeError),
    PathError(PathError),
    SongScanError(SongScanError),
}

impl From<JsonFileError> for ScreenEventError {
    fn from(value: JsonFileError) -> Self {
        Self::JsonFileError(value)
    }
}

impl From<SystemTimeError> for ScreenEventError {
    fn from(value: SystemTimeError) -> Self {
        Self::SystemTimeError(value)
    }
}

impl From<PathError> for ScreenEventError {
    fn from(value: PathError) -> Self {
        Self::PathError(value)
    }
}

impl From<SongScanError> for ScreenEventError {
    fn from(value: SongScanError) -> Self {
        Self::SongScanError(value)
    }
}

pub trait Screen {
    fn init(&mut self, _game: &mut Game) -> Result<(), ScreenInitError> {
        Ok(())
    }
    fn open(&mut self, _game: &mut Game) -> Result<(), ScreenOpenError> {
        Ok(())
    }
    fn close(&mut self, _game: &mut Game) -> Result<(), ScreenCloseError> {
        Ok(())
    }
    fn cover(&mut self, _game: &mut Game) -> Result<(), ScreenCoverError> {
        Ok(())
    }
    fn uncover(&mut self, _game: &mut Game) -> Result<(), ScreenUncoverError> {
        Ok(())
    }
    fn draw(&mut self, _game: &mut Game) -> Result<(), ScreenDrawError> {
        Ok(())
    }
    fn update(&mut self, _game: &mut Game) -> Result<(), ScreenUpdateError> {
        Ok(())
    }
    fn handle_event(&mut self, _game: &mut Game, _event: Event) -> Result<(), ScreenEventError> {
        Ok(())
    }
}
