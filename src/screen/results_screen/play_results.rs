#[derive(Debug, Clone)]
pub struct PlayResults {
    pub perfect: u64,
    pub great_early: u64,
    pub great_late: u64,
    pub bad_early: u64,
    pub bad_late: u64,
    pub too_early: u64,
    pub miss: u64,
    pub hit_combo: u64,
}

impl PlayResults {
    pub fn great(&self) -> u64 {
        self.great_early + self.great_late
    }

    pub fn bad(&self) -> u64 {
        self.bad_early + self.bad_late
    }

    pub fn hit_notes(&self) -> u64 {
        self.perfect + self.great() + self.bad()
    }

    pub fn total_notes(&self) -> u64 {
        self.hit_notes() + self.too_early + self.miss
    }
}

impl Default for PlayResults {
    fn default() -> Self {
        Self {
            perfect: 0,
            great_early: 0,
            great_late: 0,
            bad_early: 0,
            bad_late: 0,
            too_early: 0,
            miss: 0,
            hit_combo: 0,
        }
    }
}
