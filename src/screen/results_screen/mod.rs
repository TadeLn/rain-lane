pub mod play_results;

use std::{f64::consts::TAU, time::SystemTime};

use colors_transform::Hsl;
use sfml::{
    graphics::{Color, Font, RenderTarget, RenderWindow, Text, TextStyle, Transformable},
    system::Vector2f,
    window::{Event, Key},
};

use crate::{
    game::{time_difference_ms, Game},
    translate_fmt,
};

use self::play_results::PlayResults;

use super::Screen;

const FONT_SIZE: u32 = 30;

pub struct ResultsScreen {
    screen_open_timestamp: SystemTime,
    play_results: PlayResults,
}

impl ResultsScreen {
    pub fn new(play_results: PlayResults) -> Self {
        Self {
            screen_open_timestamp: SystemTime::now(),
            play_results,
        }
    }
}

pub fn draw_rainbow_text(
    target: &mut RenderWindow,
    text_string: String,
    font: &Font,
    starting_position: Vector2f,
    animation_time_ms: f64,
) {
    const MOVEMENT_AMPLITUDE: f64 = 1.5;
    const LETTER_SPACING: f32 = 5.0;
    const CYCLE_LENGTH: f64 = 1500.0;

    let mut position_changes = Vec::new();
    let cycle_start = animation_time_ms / CYCLE_LENGTH;

    // TODO: handle unicode characters
    for letter in text_string.chars() {
        let letter_text = Text::new(letter.to_string().as_str(), font, FONT_SIZE);
        position_changes.push((letter, letter_text.local_bounds().width + LETTER_SPACING));
    }

    let mut cycle = cycle_start;
    let mut position = starting_position;

    // Draw outlines
    for (letter, position_change) in &position_changes {
        cycle = cycle.fract();

        let mut letter_text = Text::new(letter.to_string().as_str(), font, FONT_SIZE);
        letter_text.move_(position);
        letter_text.move_(Vector2f::new(
            ((cycle * TAU * -2.0).sin() * MOVEMENT_AMPLITUDE) as f32,
            ((cycle * TAU * -2.0).cos() * MOVEMENT_AMPLITUDE) as f32,
        ));
        letter_text.set_style(TextStyle::BOLD);
        letter_text.set_outline_thickness(5.0);
        letter_text.set_outline_color(Color::BLACK);

        letter_text.set_fill_color(Color::BLACK);

        target.draw(&letter_text);

        position.x += position_change;
        cycle += 0.05;
    }

    cycle = cycle_start;
    position = starting_position;

    // Draw letters
    for (letter, position_change) in &position_changes {
        cycle = cycle.fract();

        let mut letter_text = Text::new(letter.to_string().as_str(), font, FONT_SIZE);
        letter_text.move_(position);
        letter_text.move_(Vector2f::new(
            ((cycle * TAU * -2.0).sin() * MOVEMENT_AMPLITUDE) as f32,
            ((cycle * TAU * -2.0).cos() * MOVEMENT_AMPLITUDE) as f32,
        ));
        letter_text.set_style(TextStyle::BOLD);

        let hsl_color =
            colors_transform::Color::to_rgb(&Hsl::from((cycle as f32) * 360.0, 100.0, 75.0));
        let color = Color::rgb(
            colors_transform::Color::get_red(&hsl_color) as u8,
            colors_transform::Color::get_green(&hsl_color) as u8,
            colors_transform::Color::get_blue(&hsl_color) as u8,
        );
        letter_text.set_fill_color(color);

        target.draw(&letter_text);

        position.x += position_change;
        cycle += 0.05;
    }
}

pub fn draw_text(
    target: &mut RenderWindow,
    text_string: String,
    font: &Font,
    position: Vector2f,
    hue: f32,
) {
    let mut text = Text::new(text_string.to_string().as_str(), font, FONT_SIZE);
    text.move_(position);
    text.set_style(TextStyle::BOLD);
    text.set_outline_thickness(5.0);
    text.set_letter_spacing(1.15);

    let hsl_color = colors_transform::Color::to_rgb(&Hsl::from(hue, 100.0, 75.0));
    let color = Color::rgb(
        colors_transform::Color::get_red(&hsl_color) as u8,
        colors_transform::Color::get_green(&hsl_color) as u8,
        colors_transform::Color::get_blue(&hsl_color) as u8,
    );
    text.set_fill_color(color);

    target.draw(&text);
}

impl Screen for ResultsScreen {
    fn open(&mut self, _game: &mut Game) -> Result<(), super::ScreenOpenError> {
        self.screen_open_timestamp = SystemTime::now();
        println!("Results: {:#?}", self.play_results);
        Ok(())
    }

    fn draw(&mut self, game: &mut Game) -> Result<(), super::ScreenDrawError> {
        let current_timestamp = SystemTime::now();
        let animation_time_ms = time_difference_ms(self.screen_open_timestamp, current_timestamp);
        game.window.set_view(&game.get_default_view());
        game.window.clear(Color::WHITE);
        draw_rainbow_text(
            &mut game.window,
            translate_fmt!("results.judgement.perfect", self.play_results.perfect),
            &game.font,
            Vector2f::new(20.0, 20.0),
            animation_time_ms,
        );

        draw_text(
            &mut game.window,
            translate_fmt!(
                "results.judgement.great",
                self.play_results.great(),
                self.play_results.great_early,
                self.play_results.great_late
            ),
            &game.font,
            Vector2f::new(20.0, 60.0),
            130.0,
        );

        draw_text(
            &mut game.window,
            translate_fmt!(
                "results.judgement.bad",
                self.play_results.bad(),
                self.play_results.bad_early,
                self.play_results.bad_late
            ),
            &game.font,
            Vector2f::new(20.0, 100.0),
            20.0,
        );

        draw_text(
            &mut game.window,
            translate_fmt!("results.judgement.miss", self.play_results.miss),
            &game.font,
            Vector2f::new(20.0, 140.0),
            350.0,
        );
        Ok(())
    }

    fn handle_event(
        &mut self,
        game: &mut Game,
        event: sfml::window::Event,
    ) -> Result<(), super::ScreenEventError> {
        match event {
            Event::KeyPressed { code, .. } => match code {
                Key::Escape => {
                    game.pop_screen();
                }
                _ => {}
            },
            _ => {}
        }
        Ok(())
    }
}
