use std::path::Path;

use super::{gui_screen::Guiable, Screen};
use crate::{
    file_dialog::{
        open_current_directory_dialog, open_font_file_dialog, open_translation_file_dialog,
        pathbuf_to_string,
    },
    language::{Translation, TranslationOperation},
    screen::gui_screen::GuiScreen,
    settings::Settings,
    toast_manager::Toast,
    translate, translate_fmt,
};
use sfml::{
    graphics::{Color, Font, RenderTarget},
    window::{Event, Key},
};

#[derive(Clone)]
pub enum GuiOption {
    HitsoundVolume(f64),
    MetronomeVolume(f64),
    MusicVolume(f64),
    MusicPreviewVolume(f64),
    SongsDirectory(String),
    FontFilename(String),
    TranslationFilename(String),
    ReloadFromFile,
    Save,
    RescanSongs,
    ImportSongs,
    Back,
}

impl Guiable for GuiOption {
    fn label(&self) -> String {
        match self {
            GuiOption::HitsoundVolume(volume) => {
                translate_fmt!("settings.hitsound_volume", format!("{:.1}", volume))
            }
            GuiOption::MetronomeVolume(volume) => {
                translate_fmt!("settings.metronome_volume", format!("{:.1}", volume))
            }
            GuiOption::MusicVolume(volume) => {
                translate_fmt!("settings.music_volume", format!("{:.1}", volume))
            }
            GuiOption::MusicPreviewVolume(volume) => {
                translate_fmt!("settings.music_preview_volume", format!("{:.1}", volume))
            }
            GuiOption::SongsDirectory(path) => translate_fmt!("settings.songs_directory", path),
            GuiOption::FontFilename(path) => translate_fmt!("settings.font_filename", path),
            GuiOption::TranslationFilename(path) => {
                translate_fmt!("settings.translation_filename", path)
            }
            GuiOption::RescanSongs => translate!("settings.rescan_songs"),
            GuiOption::ImportSongs => translate!("settings.import_songs"),
            GuiOption::ReloadFromFile => translate!("settings.reload_from_file"),
            GuiOption::Save => translate!("settings.save"),
            GuiOption::Back => translate!("settings.back"),
        }
    }
}

pub struct SettingsScreen {
    gui_screen: GuiScreen<GuiOption>,
}

impl SettingsScreen {
    pub fn new() -> SettingsScreen {
        SettingsScreen {
            gui_screen: GuiScreen::new(
                &translate!("screen.settings.title"),
                Color::GREEN,
                &[
                    GuiOption::HitsoundVolume(0.0),
                    GuiOption::MetronomeVolume(0.0),
                    GuiOption::MusicVolume(0.0),
                    GuiOption::MusicPreviewVolume(0.0),
                    GuiOption::SongsDirectory(String::new()),
                    GuiOption::FontFilename(String::new()),
                    GuiOption::TranslationFilename(String::new()),
                    GuiOption::ReloadFromFile,
                    GuiOption::Save,
                    GuiOption::RescanSongs,
                    GuiOption::ImportSongs,
                    GuiOption::Back,
                ],
            ),
        }
    }
}

impl Screen for SettingsScreen {
    fn draw(&mut self, game: &mut crate::game::Game) -> Result<(), super::ScreenDrawError> {
        game.window.set_view(&game.get_default_view());
        self.gui_screen.options[0] = GuiOption::HitsoundVolume(game.settings.hit_volume);
        self.gui_screen.options[1] = GuiOption::MetronomeVolume(game.settings.metronome_volume);
        self.gui_screen.options[2] = GuiOption::MusicVolume(game.settings.music_volume);
        self.gui_screen.options[3] = GuiOption::MusicPreviewVolume(game.settings.preview_volume);
        self.gui_screen.options[4] =
            GuiOption::SongsDirectory(game.settings.songs_directory.clone());
        self.gui_screen.options[5] = GuiOption::FontFilename(game.settings.font_filename.clone());
        self.gui_screen.options[6] =
            GuiOption::TranslationFilename(game.settings.translation_filename.clone());
        self.gui_screen.draw(game);
        Ok(())
    }
    fn handle_event(
        &mut self,
        game: &mut crate::game::Game,
        event: Event,
    ) -> Result<(), super::ScreenEventError> {
        self.gui_screen.handle_event(event);
        match event {
            Event::KeyPressed { code, .. } => match code {
                Key::Escape => {
                    game.pop_screen();
                }
                Key::Left => match self.gui_screen.selected_option() {
                    GuiOption::HitsoundVolume(_) => game.settings.hit_volume -= 1.0,
                    GuiOption::MetronomeVolume(_) => game.settings.metronome_volume -= 1.0,
                    GuiOption::MusicVolume(_) => game.settings.music_volume -= 1.0,
                    GuiOption::MusicPreviewVolume(_) => game.settings.preview_volume -= 1.0,
                    _ => {}
                },
                Key::Right => match self.gui_screen.selected_option() {
                    GuiOption::HitsoundVolume(_) => game.settings.hit_volume += 1.0,
                    GuiOption::MetronomeVolume(_) => game.settings.metronome_volume += 1.0,
                    GuiOption::MusicVolume(_) => game.settings.music_volume += 1.0,
                    GuiOption::MusicPreviewVolume(_) => game.settings.preview_volume += 1.0,
                    _ => {}
                },
                Key::Enter => match self.gui_screen.selected_option() {
                    GuiOption::SongsDirectory(_) => {
                        if let Some(path) = open_current_directory_dialog() {
                            game.settings.songs_directory = pathbuf_to_string(path);
                        }
                    }
                    GuiOption::FontFilename(_) => {
                        if let Some(path) = open_font_file_dialog(
                            &Path::new(&game.settings.font_filename)
                                .file_name()
                                .unwrap_or_default()
                                .to_string_lossy()
                                .to_string(),
                        ) {
                            let font_path_string = pathbuf_to_string(path);
                            game.settings.font_filename = font_path_string.clone();
                            if let Some(font) = Font::from_file(font_path_string.as_str()) {
                                game.font = font;
                            } else {
                                game.toast_manager.add_toast(Toast::error(&translate!(
                                    "toast.error.font_load_error"
                                )));
                            }
                        }
                    }
                    GuiOption::TranslationFilename(_) => {
                        if let Some(path) = open_translation_file_dialog(
                            &Path::new(&game.settings.translation_filename)
                                .file_name()
                                .unwrap_or_default()
                                .to_string_lossy()
                                .to_string(),
                        ) {
                            game.settings.translation_filename = pathbuf_to_string(path.clone());
                            if let Ok(translation) = Translation::from_file(&path) {
                                Translation::translation(TranslationOperation::Replace(
                                    translation,
                                ));
                            } else {
                                game.toast_manager.add_toast(Toast::error(&translate!(
                                    "toast.error.translation_load_error"
                                )));
                            }
                        }
                    }
                    GuiOption::RescanSongs => {
                        game.rescan_songs()?;
                    }
                    GuiOption::ImportSongs => {
                        if let Some(path) = open_current_directory_dialog() {
                            game.import_songs(&path)?;
                            game.rescan_songs()?;
                        }
                    }
                    GuiOption::ReloadFromFile => match Settings::from_file("settings.json") {
                        Ok(settings) => {
                            game.settings = settings;
                            game.toast_manager
                                .add_toast(Toast::success_short(&translate!(
                                    "toast.message.settings_load_success"
                                )));
                        }
                        Err(error) => {
                            game.toast_manager.add_toast(Toast::error(&translate_fmt!(
                                "toast.error.settings_load_error",
                                format!("{:#?}", error).as_str()
                            )));
                        }
                    },
                    GuiOption::Save => match game.settings.to_file("settings.json") {
                        Ok(_) => {
                            game.toast_manager
                                .add_toast(Toast::success_short(&translate!(
                                    "toast.message.settings_save_success"
                                )));
                        }
                        Err(error) => {
                            game.toast_manager.add_toast(Toast::error(&translate_fmt!(
                                "toast.error.settings_save_error",
                                format!("{:#?}", error).as_str()
                            )));
                        }
                    },
                    GuiOption::Back => {
                        game.pop_screen();
                    }
                    _ => {}
                },
                _ => {}
            },
            _ => {}
        }
        Ok(())
    }
}
