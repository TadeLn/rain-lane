use sfml::window::Key;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Finger {
    KeyboardFinger(Key),
}

impl From<Key> for Finger {
    fn from(value: Key) -> Self {
        Self::KeyboardFinger(value)
    }
}
