pub mod finger;

use sfml::{graphics::FloatRect, system::Vector2u};

use crate::{
    chart::note::{MarkerType, NoteType},
    particle::{
        particle_engine::{AnyParticle, ParticleEngine},
        text_particle::TextParticle,
    },
    screen::results_screen::play_results::PlayResults,
};

use self::finger::Finger;

use super::{note_locator::NoteLocator, GameScreen, JUDGE_LINE_POSITION};

pub const HIT_WINDOW_PERFECT_MS: i64 = 30;
pub const HIT_WINDOW_GREAT_MS: i64 = 70;
pub const HIT_WINDOW_BAD_MS: i64 = 120;
pub const ENABLE_AUTOPLAY: bool = false;

pub struct Gameplay {
    pub fingers_pressed: Vec<Finger>,
    pub active_notes: Vec<NoteLocator>,
    pub play_results: PlayResults,
}

impl Gameplay {
    pub fn new() -> Self {
        Self {
            fingers_pressed: Vec::new(),
            active_notes: Vec::new(),
            play_results: PlayResults::default(),
        }
    }

    pub fn calculate_judgement_text_particle_position(play_area: FloatRect) -> (f64, f64, f64) {
        (
            (play_area.left + (play_area.width * 0.5)) as f64,
            (play_area.top + (play_area.height * (1.0 - (JUDGE_LINE_POSITION as f32 + 0.1))))
                as f64,
            GameScreen::calculate_scale(play_area.size()),
        )
    }

    // "Timing important" is set to:
    // - true for hit notes and beginnings of hold notes
    // - false for drag notes and ends of hold notes
    // Timing unimportant hits should not be taken into account when calculating average hit error or similar

    pub fn hit_perfect(
        &mut self,
        timing: i64,
        timing_important: bool,
        particle_engine: &mut ParticleEngine,
        play_area: FloatRect,
    ) {
        self.play_results.perfect += 1;
        println!("PERFECT! {}ms", timing);

        let pos = Self::calculate_judgement_text_particle_position(play_area);
        particle_engine.particles.push(AnyParticle::TextParticle(
            TextParticle::new_judgement_perfect_particle(
                pos.0,
                pos.1,
                pos.2,
                if timing_important { timing } else { 0 },
            ),
        ));
    }

    pub fn hit_great(
        &mut self,
        timing: i64,
        timing_important: bool,
        particle_engine: &mut ParticleEngine,
        play_area: FloatRect,
    ) {
        println!("Great! {}ms", timing);
        if timing < 0 {
            self.play_results.great_early += 1;
        } else {
            self.play_results.great_late += 1;
        }

        let pos = Self::calculate_judgement_text_particle_position(play_area);
        particle_engine.particles.push(AnyParticle::TextParticle(
            TextParticle::new_judgement_great_particle(
                pos.0,
                pos.1,
                pos.2,
                if timing_important { timing } else { 0 },
            ),
        ));
    }

    pub fn hit_bad(
        &mut self,
        timing: i64,
        timing_important: bool,
        particle_engine: &mut ParticleEngine,
        play_area: FloatRect,
    ) {
        println!("Bad {}ms", timing);
        if timing < 0 {
            self.play_results.bad_early += 1;
        } else {
            self.play_results.bad_late += 1;
        }

        let pos = Self::calculate_judgement_text_particle_position(play_area);
        particle_engine.particles.push(AnyParticle::TextParticle(
            TextParticle::new_judgement_bad_particle(
                pos.0,
                pos.1,
                pos.2,
                if timing_important { timing } else { 0 },
            ),
        ));
    }

    pub fn miss(&mut self, particle_engine: &mut ParticleEngine, play_area: FloatRect) {
        println!("Miss!");
        self.play_results.miss += 1;

        let pos = Self::calculate_judgement_text_particle_position(play_area);
        particle_engine.particles.push(AnyParticle::TextParticle(
            TextParticle::new_judgement_miss_particle(pos.0, pos.1, pos.2, 0),
        ));
    }
}

impl GameScreen<'_> {
    pub fn in_hit_window(ms_error: i64, window: i64) -> bool {
        ms_error >= -window && ms_error <= window
    }

    pub fn press_finger(
        &mut self,
        window_size: Vector2u,
        play_area: FloatRect,
        finger: Finger,
        time_ms: i64,
    ) {
        self.gameplay.fingers_pressed.push(finger);
        let mut note_found_time = None;
        let mut hit_notes = vec![];

        // Find hit note
        for (_, note_locator) in self.chart.get_ordered_notes() {
            let note = &mut self.chart.lines[note_locator.line].notes[note_locator.note];
            if !note.hit_info.done && note.get_note_type() != NoteType::DragNote {
                // Handle hit notes and long note beginnings
                let note_ms = GameScreen::get_ms_from_beat_bpm(note.time, self.previous_bpm);
                let error = time_ms - note_ms;
                if error > -HIT_WINDOW_BAD_MS && error < HIT_WINDOW_BAD_MS {
                    if error > -HIT_WINDOW_GREAT_MS && error < HIT_WINDOW_GREAT_MS {
                        if error > -HIT_WINDOW_PERFECT_MS && error < HIT_WINDOW_PERFECT_MS {
                            self.gameplay.hit_perfect(
                                error,
                                true,
                                &mut self.particle_engine,
                                play_area,
                            );
                        } else {
                            self.gameplay.hit_great(
                                error,
                                true,
                                &mut self.particle_engine,
                                play_area,
                            );
                        }
                    } else {
                        self.gameplay
                            .hit_bad(error, true, &mut self.particle_engine, play_area);
                    }
                    note_found_time = Some(note.time);
                    note.hit();
                    hit_notes.push(note_locator.clone());
                    break;
                }
            }
        }

        // Find other notes in a chord
        if let Some(chord_time) = note_found_time {
            for (line_number, line) in &mut self.chart.lines.iter_mut().enumerate() {
                for (note_number, note) in &mut line.notes.iter_mut().enumerate() {
                    if !note.hit_info.done_end
                        && note.get_note_type() == NoteType::HoldNote
                        && note.time == chord_time
                    {
                        // For hold notes, add the pressed key to "chord hits" vec
                        note.add_chord_hit(finger);
                        let note_locator = NoteLocator::new(line_number, note_number);
                        if !self.gameplay.active_notes.contains(&note_locator) {
                            self.gameplay
                                .active_notes
                                .push(NoteLocator::new(line_number, note_number))
                        }
                    }
                }
            }
        }
        self.add_particles_from_notes(window_size, play_area.size(), hit_notes);
    }

    pub fn release_finger(
        &mut self,
        window_size: Vector2u,
        play_area: FloatRect,
        finger: Finger,
        time_ms: i64,
    ) {
        let mut hit_notes = vec![];

        for active_note in self.gameplay.active_notes.clone() {
            let line = &mut self.chart.lines[active_note.line];
            let note = &mut line.notes[active_note.note];
            note.remove_chord_hit(finger);

            if note.hit_info.chord_hits.is_empty() {
                // All fingers released from hold note!

                let note_end_time_ms = GameScreen::get_ms_from_beat_bpm(
                    note.get_note_end_time().unwrap(),
                    self.previous_bpm,
                );
                let error = time_ms - note_end_time_ms;

                if note.hit_info.marked >= MarkerType::Bad {
                    if note.hit_info.marked == MarkerType::Great {
                        self.gameplay
                            .hit_great(error, false, &mut self.particle_engine, play_area);
                    } else {
                        self.gameplay
                            .hit_bad(error, false, &mut self.particle_engine, play_area);
                    }
                    note.hit_end();
                    hit_notes.push(active_note.clone());
                } else {
                    note.miss_end();
                    self.gameplay.miss(&mut self.particle_engine, play_area);
                }
            }
        }

        self.gameplay.active_notes.retain(|x| {
            let line = &mut self.chart.lines[x.line];
            let note = &mut line.notes[x.note];
            !note.hit_info.chord_hits.is_empty()
        });
        self.gameplay.fingers_pressed.retain(|x| x != &finger);
        self.add_particles_from_notes(window_size, play_area.size(), hit_notes);
    }

    pub fn update_note(&mut self, note_locator: NoteLocator, play_area: FloatRect) -> bool {
        let note = &mut self.chart.lines[note_locator.line].notes[note_locator.note];

        let mut should_spawn_particles = false;

        let note_ms = GameScreen::get_ms_from_beat_bpm(note.time, self.previous_bpm);
        let note_ms_end = GameScreen::get_ms_from_beat_bpm(
            note.get_note_end_time().unwrap_or(note.time),
            self.previous_bpm,
        );
        let error = self.current_time_ms - note_ms;
        let error_end = self.current_time_ms - note_ms_end;
        let note_type = note.get_note_type();

        match note_type {
            NoteType::DragNote => {
                let pressed = self.gameplay.fingers_pressed.len() > 0;

                if pressed {
                    if Self::in_hit_window(error, HIT_WINDOW_PERFECT_MS) {
                        note.mark(MarkerType::Perfect);

                        note.hit();
                        self.gameplay.hit_perfect(
                            error,
                            false,
                            &mut self.particle_engine,
                            play_area,
                        );
                        should_spawn_particles = true;
                    } else if Self::in_hit_window(error, HIT_WINDOW_GREAT_MS) {
                        note.mark(MarkerType::Great);
                    } else if Self::in_hit_window(error, HIT_WINDOW_BAD_MS) {
                        note.mark(MarkerType::Bad);

                        if note.hit_info.marked == MarkerType::Great {
                            note.hit();
                            self.gameplay.hit_great(
                                error,
                                false,
                                &mut self.particle_engine,
                                play_area,
                            );
                            should_spawn_particles = true;
                        }
                    }
                }

                if error > HIT_WINDOW_BAD_MS {
                    if note.hit_info.marked == MarkerType::Bad {
                        note.hit();
                        self.gameplay
                            .hit_bad(error, false, &mut self.particle_engine, play_area);
                        should_spawn_particles = true;
                    } else {
                        note.miss();
                        self.gameplay.miss(&mut self.particle_engine, play_area);
                    }
                }
            }

            NoteType::TapNote => {
                if error > HIT_WINDOW_BAD_MS {
                    note.miss();
                    self.gameplay.miss(&mut self.particle_engine, play_area);
                }
            }

            NoteType::HoldNote => {
                let pressed = note.hit_info.chord_hits.len() > 0;

                if pressed {
                    if Self::in_hit_window(error_end, HIT_WINDOW_PERFECT_MS) {
                        note.mark(MarkerType::Perfect);

                        note.hit_end();
                        self.gameplay.hit_perfect(
                            error_end,
                            false,
                            &mut self.particle_engine,
                            play_area,
                        );
                        should_spawn_particles = true;
                    } else if Self::in_hit_window(error_end, HIT_WINDOW_GREAT_MS) {
                        note.mark(MarkerType::Great);
                    } else if Self::in_hit_window(error_end, HIT_WINDOW_BAD_MS) {
                        note.mark(MarkerType::Bad);

                        if note.hit_info.marked == MarkerType::Great {
                            note.hit_end();
                            self.gameplay.hit_great(
                                error_end,
                                false,
                                &mut self.particle_engine,
                                play_area,
                            );
                            should_spawn_particles = true;
                        }
                    }
                } else {
                    if !note.hit_info.done && error > HIT_WINDOW_BAD_MS {
                        note.miss();
                        note.miss_end();
                        self.gameplay.miss(&mut self.particle_engine, play_area);
                        self.gameplay.miss(&mut self.particle_engine, play_area);
                    }
                }

                if !note.hit_info.done_end && error_end > HIT_WINDOW_BAD_MS {
                    if note.hit_info.marked == MarkerType::Bad {
                        note.hit_end();
                        self.gameplay.hit_bad(
                            error_end,
                            false,
                            &mut self.particle_engine,
                            play_area,
                        );
                        should_spawn_particles = true;
                    } else {
                        note.miss_end();
                        self.gameplay.miss(&mut self.particle_engine, play_area);
                    }
                }
            }

            NoteType::UnknownNote => {}
        }

        return should_spawn_particles;
    }

    pub fn update_gameplay(&mut self, window_size: Vector2u, play_area: FloatRect) {
        let mut hit_notes = vec![];

        for line_number in 0..self.chart.lines.len() {
            let note_count_in_line = self.chart.lines[line_number].notes.len();
            for note_number in 0..note_count_in_line {
                if ENABLE_AUTOPLAY {
                    // TODO: autoplay

                    // if !note.hit_info.done && note.time < self.current_time {
                    //     note.hit();
                    //     hit_notes.push(NoteLocator::new(line_number, note_number));
                    // }
                    // if !note.hit_info.done_end
                    //     && note.get_note_end_time().unwrap_or(note.time) < self.current_time
                    // {
                    //     note.hit_end();
                    //     hit_notes.push(NoteLocator::new(line_number, note_number));
                    // }
                } else {
                    if !self.chart.lines[line_number].notes[note_number]
                        .hit_info
                        .done_end
                    {
                        let note_locator = NoteLocator {
                            line: line_number,
                            note: note_number,
                        };
                        let should_spawn_particles =
                            Self::update_note(self, note_locator, play_area);
                        if should_spawn_particles {
                            hit_notes.push(note_locator)
                        }
                    }
                }
            }
        }

        // Remove long notes that have been hit on update (because they ended already) from active notes
        self.gameplay.active_notes.retain(|x| {
            let line = &mut self.chart.lines[x.line];
            let note = &mut line.notes[x.note];
            !note.hit_info.chord_hits.is_empty()
        });
        self.add_particles_from_notes(window_size, play_area.size(), hit_notes);
    }
}
