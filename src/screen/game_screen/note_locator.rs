#[derive(Debug, Clone, Copy, PartialEq)]
pub struct NoteLocator {
    pub line: usize,
    pub note: usize,
}

impl NoteLocator {
    pub fn new(line: usize, note: usize) -> Self {
        NoteLocator { line, note }
    }
}
