#[derive(Debug, Clone, Copy)]
pub struct BpmChangeData {
    bpm: f64,
    pub index: usize,
    pub time: f64,
    pub ms: i64,
    pub multiplier: f64,
}

impl BpmChangeData {
    pub fn bpm(&self) -> f64 {
        self.bpm
    }
    pub fn new(index: usize, time: f64, ms: i64, multiplier: f64, chart_bpm: f64) -> Self {
        Self {
            bpm: chart_bpm * multiplier,
            index,
            time,
            ms,
            multiplier,
        }
    }
}

impl Default for BpmChangeData {
    fn default() -> Self {
        Self::new(0, 0.0, 0, 1.0, 120.0)
    }
}
