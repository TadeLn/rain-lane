use sfml::{
    graphics::{
        CircleShape, Color, FloatRect, Font, PrimitiveType, RectangleShape, RenderStates,
        RenderTarget, RenderWindow, Shape, Text, Transformable, Vertex,
    },
    system::{Vector2f, Vector2u},
};

use crate::{
    cached_info::CachedInfo,
    chart::note::NoteType,
    draw::is_point_in_window,
    game::Game,
    interpolate::EaseType,
    line::{curved_line::CurvedLine, straight_line::StraightLine, DrawableLine},
    translate_fmt,
};

use super::{GameScreen, DRAW_LINE_INF_END, DRAW_LINE_INF_START};

pub const STYLE_SCREEN_WIDTH: f32 = 864.0;
pub const COLOR_TRANSPARENT: Color = Color::rgba(0, 0, 0, 0);
pub const COLOR_BLACK: Color = Color::rgb(0, 0, 0);
pub const COLOR_WHITE: Color = Color::rgb(255, 255, 255);
pub const COLOR_TRANSPARENT_GRAY: Color = Color::rgba(128, 128, 128, 128);
pub const LINE_WIDTH: f64 = 8.0;
pub const LINE_COLOR: Color = COLOR_BLACK;
pub const SCREEN_EDGE_GRADIENT_SIZE_HORIZONTAL: f32 = 0.1;
pub const SCREEN_EDGE_GRADIENT_SIZE_VERTICAL: f32 = 0.1;
pub const JUDGE_RING_OUTLINE_THICKNESS: f32 = 8.0;
pub const JUDGE_RING_OUTLINE_COLOR: Color = COLOR_BLACK;
pub const JUDGE_RING_DIAMETER: f32 = 78.0;
pub const JUDGE_RING_RADIUS: f32 = JUDGE_RING_DIAMETER / 2.0;
pub const JUDGE_RING_COLOR: Color = COLOR_TRANSPARENT;
pub const NOTE_TAP_OUTER_CIRCLE_OUTLINE_THICKNESS: f32 = 3.0;
pub const NOTE_TAP_OUTER_CIRCLE_OUTLINE_COLOR: Color = COLOR_TRANSPARENT_GRAY;
pub const NOTE_TAP_OUTER_CIRCLE_DIAMETER: f32 = 68.0;
pub const NOTE_TAP_OUTER_CIRCLE_RADIUS: f32 = NOTE_TAP_OUTER_CIRCLE_DIAMETER / 2.0;
pub const NOTE_TAP_OUTER_CIRCLE_COLOR: Color = COLOR_BLACK;
pub const NOTE_TAP_INNER_CIRCLE_DIAMETER: f32 = 40.0;
pub const NOTE_TAP_INNER_CIRCLE_RADIUS: f32 = NOTE_TAP_INNER_CIRCLE_DIAMETER / 2.0;
// pub const NOTE_TAP_INNER_CIRCLE_COLOR: Color = (theme note color);
pub const NOTE_DRAG_OUTLINE_THICKNESS: f32 = 7.0;
pub const NOTE_DRAG_OUTLINE_COLOR: Color = COLOR_BLACK;
pub const NOTE_DRAG_DIAMETER: f32 = 36.0;
pub const NOTE_DRAG_RADIUS: f32 = NOTE_DRAG_DIAMETER / 2.0;
pub const NOTE_DRAG_COLOR: Color = COLOR_WHITE;
pub const NOTE_HOLD_OUTER_CIRCLE_OUTLINE_THICKNESS: f32 = 3.0;
pub const NOTE_HOLD_OUTER_CIRCLE_OUTLINE_COLOR: Color = COLOR_TRANSPARENT_GRAY;
pub const NOTE_HOLD_OUTER_CIRCLE_DIAMETER: f32 = 68.0;
pub const NOTE_HOLD_OUTER_CIRCLE_RADIUS: f32 = NOTE_HOLD_OUTER_CIRCLE_DIAMETER / 2.0;
pub const NOTE_HOLD_OUTER_CIRCLE_COLOR: Color = COLOR_BLACK;
pub const NOTE_HOLD_RECTANGLE_OUTLINE_THICKNESS: f32 = 8.0;
pub const NOTE_HOLD_RECTANGLE_OUTLINE_COLOR: Color = COLOR_BLACK;
pub const NOTE_HOLD_RECTANGLE_WIDTH: f32 = 30.0;
// pub const NOTE_HOLD_RECTANGLE_COLOR: Color = (theme note color) + gradient;
pub const NOTE_HOLD_INNER_CIRCLE_OUTLINE_THICKNESS: f32 = 8.0;
pub const NOTE_HOLD_INNER_CIRCLE_OUTLINE_COLOR: Color = COLOR_BLACK;
pub const NOTE_HOLD_INNER_CIRCLE_DIAMETER: f32 = 36.0;
pub const NOTE_HOLD_INNER_CIRCLE_RADIUS: f32 = NOTE_HOLD_INNER_CIRCLE_DIAMETER / 2.0;
pub const NOTE_HOLD_INNER_CIRCLE_COLOR: Color = COLOR_WHITE;
pub const DEBUG_KEY_POINT_LINE_RADIUS: f64 = 5.0;
pub const DEBUG_KEY_POINT_LINE_FILL_COLOR: Color = Color::BLACK;
pub const DEBUG_KEY_POINT_LINE_OUTLINE_THICKNESS: f32 = 2.0;
pub const DEBUG_KEY_POINT_COLOR_RADIUS: f64 = 7.5;
pub const DEBUG_KEY_POINT_COLOR_OUTLINE_COLOR: Color = Color::WHITE;
pub const DEBUG_KEY_POINT_COLOR_OUTLINE_THICKNESS: f32 = 0.0;
pub const DEBUG_KEY_POINT_CANVAS_RADIUS: f64 = 5.0;
pub const DEBUG_KEY_POINT_CANVAS_OUTLINE_COLOR: Color = Color::BLACK;
pub const DEBUG_KEY_POINT_CANVAS_OUTLINE_THICKNESS: f32 = 2.0;

#[derive(Debug)]
struct DrawNoteError();

impl GameScreen<'_> {
    fn draw_note_tap(
        render_window: &mut RenderWindow,
        scale: f32,
        x: f64,
        y: f64,
        note_color: Color,
    ) {
        let outer_circle_radius = NOTE_TAP_OUTER_CIRCLE_RADIUS * scale;
        let outer_circle_outline_thickness = NOTE_TAP_OUTER_CIRCLE_OUTLINE_THICKNESS * scale;
        let inner_circle_radius = NOTE_TAP_INNER_CIRCLE_RADIUS * scale;

        let mut outer_circle = CircleShape::new(outer_circle_radius, 30);
        outer_circle.set_outline_thickness(outer_circle_outline_thickness);
        outer_circle.set_outline_color(NOTE_TAP_OUTER_CIRCLE_OUTLINE_COLOR);
        outer_circle.set_fill_color(NOTE_TAP_OUTER_CIRCLE_COLOR);
        outer_circle.set_position(Vector2f::new(
            x as f32 - outer_circle_radius,
            y as f32 - outer_circle_radius,
        ));

        let mut inner_circle = CircleShape::new(inner_circle_radius, 30);
        inner_circle.set_fill_color(note_color);
        inner_circle.set_position(Vector2f::new(
            x as f32 - inner_circle_radius,
            y as f32 - inner_circle_radius,
        ));

        render_window.draw(&outer_circle);
        render_window.draw(&inner_circle);
    }

    fn draw_note_drag(render_window: &mut RenderWindow, scale: f32, x: f64, y: f64) {
        let radius = NOTE_DRAG_RADIUS * scale;
        let outline_thickness = NOTE_DRAG_OUTLINE_THICKNESS * scale;

        let mut circle = CircleShape::new(radius, 30);
        circle.set_outline_thickness(outline_thickness);
        circle.set_outline_color(NOTE_DRAG_OUTLINE_COLOR);
        circle.set_fill_color(NOTE_DRAG_COLOR);
        circle.set_position(Vector2f::new(x as f32 - radius, y as f32 - radius));

        render_window.draw(&circle);
    }

    fn draw_note_hold(
        render_window: &mut RenderWindow,
        scale: f32,
        x: f64,
        y: f64,
        end_y: f64,
        note_color: Color,
    ) {
        let note_length: f64 = (end_y - y).abs();

        let outer_circle_radius = NOTE_HOLD_OUTER_CIRCLE_RADIUS * scale;
        let outer_circle_outline_thickness = NOTE_HOLD_OUTER_CIRCLE_OUTLINE_THICKNESS * scale;
        let rectangle_outline_thickness = NOTE_HOLD_RECTANGLE_OUTLINE_THICKNESS * scale;
        let rectangle_width = NOTE_HOLD_RECTANGLE_WIDTH * scale;
        let inner_circle_radius = NOTE_HOLD_INNER_CIRCLE_RADIUS * scale;
        let inner_circle_outline_thickness = NOTE_HOLD_INNER_CIRCLE_OUTLINE_THICKNESS * scale;

        let mut outer_circle = CircleShape::new(outer_circle_radius, 32);
        outer_circle.set_outline_thickness(outer_circle_outline_thickness);
        outer_circle.set_outline_color(NOTE_HOLD_OUTER_CIRCLE_OUTLINE_COLOR);
        outer_circle.set_fill_color(NOTE_HOLD_OUTER_CIRCLE_COLOR);
        outer_circle.set_position(Vector2f::new(
            x as f32 - outer_circle_radius,
            y as f32 - outer_circle_radius,
        ));

        // TODO: replace rectangleshape with custom shape from vertex array to make a gradient
        let mut rectangle = RectangleShape::new();
        rectangle.set_outline_thickness(rectangle_outline_thickness);
        rectangle.set_outline_color(NOTE_HOLD_RECTANGLE_OUTLINE_COLOR);
        rectangle.set_size(Vector2f::new(rectangle_width, note_length as f32));
        rectangle.set_fill_color(note_color);
        rectangle.set_position(Vector2f::new(
            x as f32 - (rectangle_width / 2.0),
            (y - note_length) as f32,
        ));

        let mut inner_circle = CircleShape::new(inner_circle_radius, 32);
        inner_circle.set_outline_thickness(inner_circle_outline_thickness);
        inner_circle.set_outline_color(NOTE_HOLD_INNER_CIRCLE_OUTLINE_COLOR);
        inner_circle.set_fill_color(NOTE_HOLD_INNER_CIRCLE_COLOR);
        inner_circle.set_position(Vector2f::new(
            x as f32 - inner_circle_radius,
            y as f32 - inner_circle_radius,
        ));

        render_window.draw(&outer_circle);
        render_window.draw(&rectangle);
        render_window.draw(&inner_circle);
    }

    fn draw_note(
        render_window: &mut RenderWindow,
        scale: f32,
        x: f64,
        y: f64,
        end_y: Option<f64>,
        note_color: Color,
        note_type: NoteType,
    ) -> Result<(), DrawNoteError> {
        match note_type {
            NoteType::TapNote => Self::draw_note_tap(render_window, scale, x, y, note_color),
            NoteType::DragNote => Self::draw_note_drag(render_window, scale, x, y),
            NoteType::HoldNote => Self::draw_note_hold(
                render_window,
                scale,
                x,
                y,
                end_y.ok_or(DrawNoteError {})?,
                note_color,
            ),
            NoteType::UnknownNote => {
                Self::draw_note_tap(render_window, scale, x, y, sfml::graphics::Color::RED)
            }
        };
        Ok(())
    }

    fn draw_judge_ring(render_window: &mut RenderWindow, scale: f32, x: f64, y: f64, color: Color) {
        let circle_outline_thickness = JUDGE_RING_OUTLINE_THICKNESS * scale;
        let circle_radius = JUDGE_RING_RADIUS * scale;

        let mut circle = CircleShape::new(circle_radius as f32, 30);
        circle.set_position(Vector2f::new(
            x as f32 - circle_radius,
            y as f32 - circle_radius,
        ));
        circle.set_fill_color(Color::TRANSPARENT);
        circle.set_outline_color(color);
        circle.set_outline_thickness(circle_outline_thickness);
        render_window.draw(&circle);
    }

    fn draw_debug_line_point(render_window: &mut RenderWindow, x: f64, y: f64, color: Color) {
        if !is_point_in_window(render_window.size(), Vector2f::new(x as f32, y as f32)) {
            return;
        }

        let mut circle = CircleShape::new(DEBUG_KEY_POINT_LINE_RADIUS as f32, 30);
        circle.set_position(Vector2f::new(
            (x - DEBUG_KEY_POINT_LINE_RADIUS) as f32,
            (y - DEBUG_KEY_POINT_LINE_RADIUS) as f32,
        ));
        circle.set_fill_color(DEBUG_KEY_POINT_LINE_FILL_COLOR);
        circle.set_outline_color(color);
        circle.set_outline_thickness(DEBUG_KEY_POINT_LINE_OUTLINE_THICKNESS);
        render_window.draw(&circle);
    }

    fn draw_debug_color_key_point(
        render_window: &mut RenderWindow,
        x: f64,
        y: f64,
        fill_color: Color,
    ) {
        if !is_point_in_window(render_window.size(), Vector2f::new(x as f32, y as f32)) {
            return;
        }

        let mut circle = CircleShape::new(DEBUG_KEY_POINT_COLOR_RADIUS as f32, 30);
        circle.set_position(Vector2f::new(
            (x - DEBUG_KEY_POINT_COLOR_RADIUS) as f32,
            (y - DEBUG_KEY_POINT_COLOR_RADIUS) as f32,
        ));
        circle.set_fill_color(fill_color);
        circle.set_outline_color(DEBUG_KEY_POINT_COLOR_OUTLINE_COLOR);
        circle.set_outline_thickness(DEBUG_KEY_POINT_COLOR_OUTLINE_THICKNESS);
        render_window.draw(&circle);
    }

    fn draw_debug_canvas(
        render_window: &mut RenderWindow,
        font: &Font,
        index: usize,
        cached_info: &CachedInfo,
        canvas_width: f64,
        canvas_center: f64,
    ) {
        let mut canvas_shape = RectangleShape::new();
        canvas_shape.set_position(Vector2f::new(
            (canvas_center - (canvas_width / 2.0)) as f32,
            0.0,
        ));
        canvas_shape.set_size(Vector2f::new(
            canvas_width as f32,
            render_window.size().y as f32,
        ));
        canvas_shape.set_fill_color(cached_info.get_canvas_color(index));
        canvas_shape.set_outline_color(Color::rgba(0, 0, 0, 128));
        canvas_shape.set_outline_thickness(1.0);
        render_window.draw(&canvas_shape);

        let mut text_shape = Text::new(
            translate_fmt!("debug.gameplay.canvas", &index).as_str(),
            font,
            10,
        );
        text_shape.set_position(canvas_shape.position());
        text_shape.move_(Vector2f::new(0.0, index as f32 * 10.0));
        render_window.draw(&text_shape);
    }

    fn draw_debug_canvas_x_key_point(
        render_window: &mut RenderWindow,
        x: f64,
        y: f64,
        canvas_index: usize,
        cached_info: &CachedInfo,
    ) {
        if !is_point_in_window(render_window.size(), Vector2f::new(x as f32, y as f32)) {
            return;
        }

        let file_color: Color = cached_info.get_canvas_color(canvas_index);
        let mut shape = RectangleShape::new();
        shape.set_origin(Vector2f::new(
            DEBUG_KEY_POINT_CANVAS_RADIUS as f32,
            DEBUG_KEY_POINT_CANVAS_RADIUS as f32,
        ));
        shape.set_position(Vector2f::new(x as f32, y as f32));
        shape.rotate(45.0);
        shape.set_size(Vector2f::new(
            (DEBUG_KEY_POINT_CANVAS_RADIUS * 2.0) as f32,
            (DEBUG_KEY_POINT_CANVAS_RADIUS * 2.0) as f32,
        ));
        shape.set_fill_color(file_color);
        shape.set_outline_color(DEBUG_KEY_POINT_CANVAS_OUTLINE_COLOR);
        shape.set_outline_thickness(DEBUG_KEY_POINT_CANVAS_OUTLINE_THICKNESS);
        render_window.draw(&shape);
    }

    pub fn draw_all_lines(
        &self,
        window: &mut RenderWindow,
        scale: f64,
        window_size: Vector2u,
        play_area_size: Vector2f,
        debug_mode: bool,
        debug_line_id_font: &Font,
    ) {
        // Draw all lines
        for (line_index, line) in self.chart.lines.iter().enumerate() {
            {
                let mut last_point_info: Option<(f64, f64, Color, EaseType)> = None;
                let line_width = LINE_WIDTH * scale;
                let current_line_color = line.get_line_color_from_time(self.current_time);
                let current_judge_ring_color =
                    line.get_judge_ring_color_from_time(self.current_time);

                for (point_index, point) in line.line_points.iter().enumerate() {
                    let x = Self::get_screen_x_from_x(
                        window_size,
                        play_area_size,
                        line.get_x_position_from_time(
                            point.time,
                            self.current_time,
                            &self.chart.canvas_moves,
                        ),
                    );
                    let y = Self::get_y_from_time(
                        play_area_size,
                        self.current_time,
                        point.time,
                        self.chart.canvas_moves[point.canvas_index as usize]
                            .get_speed_from_time(point.time),
                    );
                    let color = point.color;

                    if y > play_area_size.y as f64 {
                        last_point_info =
                            Some((x, y, color.into(), EaseType::from_id(point.ease_type)));
                        continue;
                    }

                    if let Some((_, last_y, ..)) = last_point_info {
                        if last_y < 0.0 {
                            break;
                        }
                    }

                    if let Some((last_x_value, last_y_value, last_color_value, last_ease_type)) =
                        last_point_info
                    {
                        // Draw a line between this point and previous point
                        CurvedLine::new(last_x_value, last_y_value, x, y, last_ease_type).draw(
                            window,
                            play_area_size,
                            line_width,
                            current_line_color.unwrap_or(last_color_value.into()).into(),
                            current_line_color.unwrap_or(color).into(),
                            debug_mode
                                && self.debug_selected_line == line_index as isize
                                && self.debug_selected_line_segment == point_index as isize - 1,
                        );
                    } else {
                        // First line - draw from current point to bottom of the screen
                        if DRAW_LINE_INF_START {
                            StraightLine::new(x, window_size.y as f64, x, y).draw(
                                window,
                                play_area_size,
                                line_width,
                                current_line_color.unwrap_or(color).into(),
                                current_line_color.unwrap_or(color).into(),
                                debug_mode
                                    && self.debug_selected_line == line_index as isize
                                    && self.debug_selected_line_segment == -1,
                            );
                        }
                    }

                    last_point_info =
                        Some((x, y, color.into(), EaseType::from_id(point.ease_type)));
                }
                if let Some((last_x_value, last_y_value, last_color_value, _)) = last_point_info {
                    // Last line - draw from last point to top of the screen
                    if DRAW_LINE_INF_END {
                        StraightLine::new(last_x_value, last_y_value, last_x_value, 0.0).draw(
                            window,
                            play_area_size,
                            line_width,
                            current_line_color.unwrap_or(last_color_value.into()).into(),
                            current_line_color.unwrap_or(last_color_value.into()).into(),
                            debug_mode
                                && self.debug_selected_line == line_index as isize
                                && self.debug_selected_line_segment
                                    == line.line_points.len() as isize - 1,
                        );
                    }
                }

                // Draw judge ring
                if let Some(color) = current_judge_ring_color {
                    Self::draw_judge_ring(
                        window,
                        scale as f32,
                        Self::get_screen_x_from_x(
                            window_size,
                            play_area_size,
                            line.get_x_position_from_time(
                                self.current_time,
                                self.current_time,
                                &self.chart.canvas_moves,
                            ),
                        ),
                        Self::get_y_from_time(
                            play_area_size,
                            self.current_time,
                            self.current_time,
                            1.0,
                        ),
                        color.into(),
                    )
                }
            }

            if debug_mode {
                for color_key_point in &line.line_color {
                    let x = Self::get_screen_x_from_x(
                        window_size,
                        play_area_size,
                        line.get_x_position_from_time(
                            color_key_point.time,
                            self.current_time,
                            &self.chart.canvas_moves,
                        ),
                    );
                    let y = Self::get_y_from_time(
                        play_area_size,
                        self.current_time,
                        color_key_point.time,
                        1.0,
                    );
                    let color = color_key_point.start_color;
                    Self::draw_debug_color_key_point(window, x, y, color.into());
                }

                for (line_point_index, point) in line.line_points.iter().enumerate() {
                    let x = Self::get_screen_x_from_x(
                        window_size,
                        play_area_size,
                        line.get_x_position_from_time(
                            point.time,
                            self.current_time,
                            &self.chart.canvas_moves,
                        ),
                    );
                    let y = Self::get_y_from_time(
                        play_area_size,
                        self.current_time,
                        point.time,
                        line.get_speed_from_time(point.time, &self.chart.canvas_moves),
                    );
                    let color = point.color;
                    Self::draw_debug_line_point(window, x, y, color.into());
                    if line_point_index == 0 {
                        let mut text =
                            Text::new(format!("L{}", line_index).as_str(), debug_line_id_font, 20);
                        text.set_position(Vector2f::new(x as f32, y as f32));
                        text.set_fill_color(Color::WHITE);
                        text.set_outline_color(Color::BLACK);
                        text.set_outline_thickness(1.0);
                        window.draw(&text);
                    }
                }
            }
        }
    }

    pub fn draw_all_notes(
        &self,
        window: &mut RenderWindow,
        window_size: Vector2u,
        play_area_size: Vector2f,
        scale: f64,
    ) {
        // Draw all notes of all lines
        // This is separate from the main for loop because the notes need to be on top of all the lines
        for (_, line) in self.chart.lines.iter().enumerate() {
            for note in &line.notes {
                let hit_hold = note.get_note_type() == NoteType::HoldNote && note.hit_info.done;
                if note.hit_info.done_end {
                    continue;
                }

                let speed = line.get_speed_from_time(note.time, &self.chart.canvas_moves);
                let end_y = match note.get_note_end_time() {
                    Some(end_time) => {
                        if self.current_time > end_time {
                            continue;
                        }
                        let end_y = Self::get_y_from_time(
                            play_area_size,
                            self.current_time,
                            end_time,
                            speed,
                        );
                        Some(end_y)
                    }
                    None => None,
                };

                let x = if hit_hold {
                    Self::get_screen_x_from_x(
                        window_size,
                        play_area_size,
                        line.get_x_position_from_time(
                            self.current_time,
                            self.current_time,
                            &self.chart.canvas_moves,
                        ),
                    )
                } else {
                    Self::get_screen_x_from_x(
                        window_size,
                        play_area_size,
                        line.get_x_position_from_time(
                            note.time,
                            self.current_time,
                            &self.chart.canvas_moves,
                        ),
                    )
                };
                let y = if hit_hold {
                    Self::get_y_from_time(
                        play_area_size,
                        self.current_time,
                        self.current_time,
                        speed,
                    )
                } else {
                    Self::get_y_from_time(play_area_size, self.current_time, note.time, speed)
                };

                Self::draw_note(
                    window,
                    scale as f32,
                    x,
                    y,
                    end_y,
                    self.current_theme.get_note_color().into(),
                    note.get_note_type(),
                )
                .unwrap();
            }
        }
    }

    pub fn draw_screen_edges(
        &self,
        window: &mut RenderWindow,
        window_size: Vector2u,
        play_area: FloatRect,
        background_color: Color,
    ) {
        let window_size_float = Vector2f::new(window_size.x as f32, window_size.y as f32);
        let background_color_transparent = {
            let mut color = background_color.clone();
            color.a = 0;
            color
        };

        let play_area_left_edge = play_area.left;
        let play_area_right_edge = play_area_left_edge + play_area.width;
        let play_area_top_edge = play_area.top;
        let play_area_bottom_edge = play_area.top + play_area.height;

        let draw_gradient = |vertex_positions: [(f32, f32); 6]| {
            let mut vertices = Vec::new();
            for (i, position) in vertex_positions.iter().enumerate() {
                vertices.push(Vertex::new(
                    Vector2f::new(position.0, position.1),
                    if i >= 4 {
                        background_color_transparent
                    } else {
                        background_color
                    },
                    Vector2f::default(),
                ));
            }
            window.draw_primitives(
                &vertices,
                PrimitiveType::TRIANGLE_STRIP,
                &RenderStates::DEFAULT,
            );
        };

        // Left edge
        draw_gradient([
            (0.0, 0.0),
            (0.0, window_size_float.y),
            (play_area_left_edge, 0.0),
            (play_area_left_edge, window_size_float.y),
            (
                play_area_left_edge + (play_area.width * SCREEN_EDGE_GRADIENT_SIZE_HORIZONTAL),
                0.0,
            ),
            (
                play_area_left_edge + (play_area.width * SCREEN_EDGE_GRADIENT_SIZE_HORIZONTAL),
                window_size_float.y,
            ),
        ]);

        // Right edge
        draw_gradient([
            (window_size_float.x, 0.0),
            (window_size_float.x, window_size_float.y),
            (play_area_right_edge, 0.0),
            (play_area_right_edge, window_size_float.y),
            (
                play_area_right_edge - (play_area.width * SCREEN_EDGE_GRADIENT_SIZE_HORIZONTAL),
                0.0,
            ),
            (
                play_area_right_edge - (play_area.width * SCREEN_EDGE_GRADIENT_SIZE_HORIZONTAL),
                window_size_float.y,
            ),
        ]);

        // Top edge
        draw_gradient([
            (0.0, 0.0),
            (window_size_float.x, 0.0),
            (0.0, play_area_top_edge),
            (window_size_float.x, play_area_top_edge),
            (
                0.0,
                play_area_top_edge + (play_area.height * SCREEN_EDGE_GRADIENT_SIZE_VERTICAL),
            ),
            (
                window_size_float.x,
                play_area_top_edge + (play_area.height * SCREEN_EDGE_GRADIENT_SIZE_VERTICAL),
            ),
        ]);

        // Bottom edge
        draw_gradient([
            (0.0, window_size_float.y),
            (window_size_float.x, window_size_float.y),
            (0.0, play_area_bottom_edge),
            (window_size_float.x, play_area_bottom_edge),
            (
                0.0,
                play_area_bottom_edge - (play_area.height * SCREEN_EDGE_GRADIENT_SIZE_VERTICAL),
            ),
            (
                window_size_float.x,
                play_area_bottom_edge - (play_area.height * SCREEN_EDGE_GRADIENT_SIZE_VERTICAL),
            ),
        ]);
    }

    pub fn draw_judge_line(
        &self,
        window: &mut RenderWindow,
        window_size: Vector2u,
        play_area_size: Vector2f,
    ) {
        // Draw judge line
        // TODO: allow for enabling this in preferences
        if false {
            let y =
                Self::get_y_from_time(play_area_size, self.current_time, self.current_time, 1.0);
            let line = StraightLine::new(0.0, y, window_size.x as f64, y);
            let mut color: Color = self.current_theme.get_particle_color().into();
            color.a = 64;
            line.draw(window, play_area_size, 1.0, color, color, false);
            line.draw(window, play_area_size, 2.0, color, color, false);
            line.draw(window, play_area_size, 3.0, color, color, false);
            line.draw(window, play_area_size, 4.0, color, color, false);
            line.draw(window, play_area_size, 5.0, color, color, false);
            line.draw(window, play_area_size, 6.0, color, color, false);
        }
    }

    pub fn draw_debug_canvases(
        &self,
        window: &mut RenderWindow,
        window_size: Vector2u,
        play_area_size: Vector2f,
        font: &Font,
        debug_mode: bool,
    ) {
        for canvas in &self.chart.canvas_moves {
            if self.debug_view_canvas {
                let screen_width = play_area_size.x as f64;
                let canvas_position = Self::get_screen_x_from_x(
                    window_size,
                    play_area_size,
                    canvas.get_x_position_from_time(self.current_time),
                );
                Self::draw_debug_canvas(
                    window,
                    font,
                    canvas.index,
                    &self.cached_info,
                    screen_width,
                    canvas_position,
                );
            }

            if debug_mode {
                for key_point in &canvas.x_position_key_points {
                    let x = Self::get_screen_x_from_x(window_size, play_area_size, key_point.value);
                    let y = Self::get_y_from_time(
                        play_area_size,
                        self.current_time,
                        key_point.time,
                        1.0,
                    );
                    Self::draw_debug_canvas_x_key_point(
                        window,
                        x,
                        y,
                        canvas.index,
                        &self.cached_info,
                    );
                }
            }
        }
    }

    pub fn draw_debug_screen_boundaries(&self, window: &mut RenderWindow, play_area: FloatRect) {
        let rectangle_screen_boundaries_outline_thickness = 2.0;
        let mut rectangle_screen_boundaries = RectangleShape::new();
        rectangle_screen_boundaries.set_position(
            play_area.position()
                + Vector2f::new(
                    rectangle_screen_boundaries_outline_thickness,
                    rectangle_screen_boundaries_outline_thickness,
                ),
        );
        rectangle_screen_boundaries.set_size(
            play_area.size()
                - Vector2f::new(
                    rectangle_screen_boundaries_outline_thickness * 2.0,
                    rectangle_screen_boundaries_outline_thickness * 2.0,
                ),
        );
        rectangle_screen_boundaries.set_fill_color(Color::TRANSPARENT);
        rectangle_screen_boundaries.set_outline_color(Color::RED);
        rectangle_screen_boundaries
            .set_outline_thickness(rectangle_screen_boundaries_outline_thickness);
        window.draw(&rectangle_screen_boundaries);
    }

    pub fn draw_debug_tooltip(
        &self,
        window: &mut RenderWindow,
        window_size: Vector2u,
        play_area_size: Vector2f,
        font: &Font,
    ) {
        let mouse_position = window.mouse_position();

        let line = StraightLine::new(
            0.0,
            mouse_position.y as f64,
            window_size.x as f64,
            mouse_position.y as f64,
        );

        let mouse_y_time = Self::get_time_from_y(
            play_area_size,
            self.current_time,
            mouse_position.y as f64,
            1.0,
        );
        let selected_line = &self.chart.lines[self.debug_selected_line as usize];
        let mut text = Text::new(
            &translate_fmt!(
                "debug.gameplay.cursor",
                format!("{:.3}", mouse_y_time),
                format!(
                    "{:.3}",
                    selected_line.get_x_position_from_time(
                        mouse_y_time,
                        self.current_time,
                        &self.chart.canvas_moves
                    )
                )
            ),
            font,
            10,
        );
        text.set_position(Vector2f::new(0.0, mouse_position.y as f32));

        let mut rectangle = RectangleShape::new();
        rectangle.set_position(text.position());
        rectangle.move_(Vector2f::new(-3.0, -3.0));
        let size = text.local_bounds().size();
        rectangle.set_size(Vector2f::new(size.x + 6.0, size.y + 6.0));
        rectangle.set_fill_color(Color::BLUE);

        line.draw(window, play_area_size, 2.0, Color::BLUE, Color::BLUE, false);
        window.draw(&rectangle);
        window.draw(&text);
    }

    pub fn add_debug_text(&self, game: &mut Game) {
        game.add_debug_text(format!(
            "Song: {}\n\
            Particles: {}\n\
            Ms: {:.3} / {:.3}\n\
            BPM: {:.3} -> {:.3}, Time: {:.3}\n\
            Selected: L{}S{}\n\
            Fingers: {:?}\n\
            Active notes: {:?}\n",
            self.chart.songs_name,
            self.particle_engine.particles.len(),
            self.current_time_ms as f64 / 1000.0,
            self.music_duration.unwrap_or(0) as f64 / 1000.0,
            self.previous_bpm.bpm(),
            self.next_bpm.bpm(),
            self.current_time,
            self.debug_selected_line,
            self.debug_selected_line_segment,
            self.gameplay.fingers_pressed,
            self.gameplay.active_notes
        ));
    }
}
