pub mod bpm_change_data;
pub mod draw;
pub mod gameplay;
pub mod note_locator;

use std::{
    path::PathBuf,
    time::{Duration, SystemTime, SystemTimeError},
};

use sfml::{
    audio::{Music, SoundSource, SoundStatus},
    graphics::{Color, FloatRect, RenderTarget},
    system::{Time, Vector2f, Vector2u},
    window::{mouse, Event, Key},
};

use crate::{
    cached_info::CachedInfo,
    chart::{theme::Theme, Chart},
    game::{time_difference_ms, try_open_file, Game},
    particle::{
        circle_particle::CircleParticle,
        particle_engine::{AnyParticle, ParticleEngine},
    },
};

use self::{
    bpm_change_data::BpmChangeData, draw::STYLE_SCREEN_WIDTH, gameplay::Gameplay,
    note_locator::NoteLocator,
};

use super::{
    results_screen::ResultsScreen, song_list_screen::MUSIC_FILENAMES, Screen, ScreenDrawError,
    ScreenUpdateError,
};

pub const SCROLL_SPEED: f64 = 400.0;
pub const JUDGE_LINE_POSITION: f64 = 0.1;
pub const SCREEN_CENTER_OFFSET: f64 = 0.0;
pub const SCREEN_MARGIN_X: f64 = 100.0;
pub const TRUST_FLOOR_POSITION: bool = false;
pub const DRAW_LINE_INF_START: bool = false;
pub const DRAW_LINE_INF_END: bool = false;
pub const DEBUG_PRINT_TIME_INFO: bool = false;
pub const PLAY_AREA_ASPECT_RATIO: f32 = 9.0 / 16.0;
pub const BREAK_TIME_AFTER_CHART_END: u64 = 2000;

pub struct GameScreen<'a> {
    pub debug_view_canvas: bool,
    pub debug_selected_line: isize,
    pub debug_selected_line_segment: isize,

    pub last_metronome_beat_int: i64,
    pub challenge_time: bool,

    pub song_start_timestamp: SystemTime,
    pub skip_time_ms: i64,
    pub paused: bool,
    pub pause_timestamp: SystemTime,
    pub pause_time_ms: i64,
    pub current_timestamp: SystemTime,
    pub current_time_ms: i64,
    pub current_time: f64,
    pub current_theme: Theme,
    pub previous_bpm: BpmChangeData,
    pub next_bpm: BpmChangeData,

    pub particle_engine: ParticleEngine,

    pub cached_info: CachedInfo,

    pub chart_dir_path: PathBuf,
    pub chart_filename: PathBuf,
    pub chart: Chart,
    pub music: Option<Music<'a>>,
    pub music_duration: Option<u64>,
    pub should_realign_audio_on_update: bool,

    pub gameplay: Gameplay,
}

impl GameScreen<'_> {
    pub fn new(chart_dir_path: PathBuf, chart_filename: PathBuf) -> Self {
        let song_start_timestamp = SystemTime::now() + Duration::from_secs(2);
        GameScreen {
            debug_view_canvas: false,
            debug_selected_line: 0,
            debug_selected_line_segment: 0,

            song_start_timestamp,
            skip_time_ms: 0,
            paused: false,
            pause_timestamp: SystemTime::now(),
            pause_time_ms: 0,
            previous_bpm: Default::default(),
            next_bpm: Default::default(),

            last_metronome_beat_int: -5,

            current_timestamp: SystemTime::now(),
            current_time_ms: 0,
            current_time: 0.0,
            current_theme: Theme::default(),
            challenge_time: false,

            particle_engine: ParticleEngine::new(),

            cached_info: CachedInfo::new(),

            chart_dir_path,
            chart_filename,
            chart: Chart::new(),
            music: None,
            music_duration: None,
            should_realign_audio_on_update: false,

            gameplay: Gameplay::new(),
        }
    }

    // Get `milliseconds per beat` from `beats per minute`
    pub fn get_mspb_from_bpm(bpm: f64) -> f64 {
        60000.0 / bpm
    }

    /*
    previous_bpm_change_time and previous_bpm_change_ms - reference to the last known `beat -> ms` correspondence.
    At the beginning of the song the values for those are `beat 0 -> 0ms`, but they can change over time,
    for example, when the BPM changes.
     */
    pub fn get_beat_from_ms(
        time_ms: i64,
        previous_bpm: f64,
        previous_bpm_change_ms: i64,
        previous_bpm_change_time: f64,
    ) -> f64 {
        let ms_since_previous_bpm = time_ms - previous_bpm_change_ms;
        let time = (ms_since_previous_bpm as f64 / Self::get_mspb_from_bpm(previous_bpm))
            + previous_bpm_change_time;

        if DEBUG_PRINT_TIME_INFO {
            println!(
                "get_beat_from_ms(@{:.3} ms): BPM: {:.1} :: @ {:.3}",
                time_ms, previous_bpm, previous_bpm_change_time
            );
        }

        return time;
    }

    pub fn get_beat_from_ms_bpm(time_ms: i64, previous_bpm: BpmChangeData) -> f64 {
        Self::get_beat_from_ms(
            time_ms,
            previous_bpm.bpm(),
            previous_bpm.ms,
            previous_bpm.time,
        )
    }

    pub fn get_ms_from_beat(
        time: f64,
        previous_bpm: f64,
        previous_bpm_change_ms: i64,
        previous_bpm_change_time: f64,
    ) -> i64 {
        let time_since_previous_bpm = time - previous_bpm_change_time;
        let time_ms = previous_bpm_change_ms
            + (time_since_previous_bpm * Self::get_mspb_from_bpm(previous_bpm)) as i64;

        if DEBUG_PRINT_TIME_INFO {
            println!(
                "get_ms_from_beat(@{:.3}): BPM: {:.1} :: @ {:.3} \n  time_since_previous_bpm: {}\n  >> RESULT: {} ms",
                time, previous_bpm, previous_bpm_change_time, time_since_previous_bpm, time_ms
            );
        }
        time_ms
    }

    pub fn get_ms_from_beat_bpm(time: f64, previous_bpm: BpmChangeData) -> i64 {
        Self::get_ms_from_beat(time, previous_bpm.bpm(), previous_bpm.ms, previous_bpm.time)
    }

    // This function shouldn't need to be used multiple times in a frame
    // because the screen width value doesn't change during a frame
    pub fn calculate_play_area(window_size: Vector2u) -> FloatRect {
        let width = (window_size.y as f32) * PLAY_AREA_ASPECT_RATIO;
        if width > window_size.x as f32 {
            let height = (window_size.x as f32) * PLAY_AREA_ASPECT_RATIO.recip();
            let width = window_size.x as f32;
            FloatRect::new((window_size.x as f32 - width) / 2.0, 0.0, width, height)
        } else {
            let height = window_size.y as f32;
            FloatRect::new((window_size.x as f32 - width) / 2.0, 0.0, width, height)
        }
    }

    pub fn calculate_scale(play_area_size: Vector2f) -> f64 {
        play_area_size.x as f64 / (STYLE_SCREEN_WIDTH as f64)
    }

    pub fn get_y_from_time(
        play_area_size: Vector2f,
        current_time: f64,
        point_time: f64,
        scroll_speed_multiplier: f64,
    ) -> f64 {
        let scroll_speed =
            SCROLL_SPEED * Self::calculate_scale(play_area_size) * scroll_speed_multiplier;
        (play_area_size.y as f64 * (1.0 - JUDGE_LINE_POSITION))
            - (scroll_speed * (point_time - current_time))
    }

    pub fn get_time_from_y(
        play_area_size: Vector2f,
        current_time: f64,
        y_position: f64,
        scroll_speed_multiplier: f64,
    ) -> f64 {
        let scroll_speed =
            SCROLL_SPEED * Self::calculate_scale(play_area_size) * scroll_speed_multiplier;
        (((play_area_size.y as f64 * (1.0 - JUDGE_LINE_POSITION)) - y_position) / scroll_speed)
            + current_time
    }

    pub fn get_screen_x_from_x(window_size: Vector2u, play_area_size: Vector2f, x: f64) -> f64 {
        (window_size.x as f64 / 2.0) + SCREEN_CENTER_OFFSET + (play_area_size.x as f64 * x)
    }

    pub fn skip_time(&mut self, time_change_ms: i64) {
        self.skip_time_ms += time_change_ms;
        self.should_realign_audio_on_update = true;
    }

    pub fn set_time(&mut self, new_time_ms: i64) {
        self.skip_time_ms = new_time_ms;
        self.pause_time_ms = 0;
        self.song_start_timestamp = SystemTime::now();
        self.should_realign_audio_on_update = true;
    }

    pub fn toggle_pause(&mut self) -> Result<(), SystemTimeError> {
        if self.paused {
            self.paused = false;
            self.pause_time_ms += self
                .current_timestamp
                .duration_since(self.pause_timestamp)?
                .as_millis() as i64;

            self.should_realign_audio_on_update = true;
            if let Some(music) = &mut self.music {
                music.play();
            }
        } else {
            self.paused = true;
            self.pause_timestamp = SystemTime::now();

            if let Some(music) = &mut self.music {
                music.pause();
            }
        }
        Ok(())
    }

    pub fn get_current_audio_ms(&self) -> f64 {
        self.current_time_ms as f64 + (self.chart.offset * 1000.0)
    }

    pub fn add_click_particles(&mut self, play_area_size: Vector2f, x_position: f64) {
        let x = x_position;
        let y =
            Self::get_y_from_time(play_area_size, self.current_time, self.current_time, 1.0) as f64;
        let scale = Self::calculate_scale(play_area_size);
        for _ in 0..5 {
            self.particle_engine
                .particles
                .push(AnyParticle::CircleParticle(
                    CircleParticle::new_small_hit_particle(
                        self.current_timestamp,
                        x,
                        y,
                        scale,
                        self.current_theme.get_particle_color(),
                    ),
                ))
        }
        self.particle_engine
            .particles
            .push(AnyParticle::CircleParticle(
                CircleParticle::new_ring_hit_particle(
                    self.current_timestamp,
                    x,
                    y,
                    scale,
                    self.current_theme.get_particle_color(),
                ),
            ));
    }

    fn add_particles_from_notes(
        &mut self,
        window_size: Vector2u,
        play_area_size: Vector2f,
        hit_notes: Vec<NoteLocator>,
    ) {
        for NoteLocator {
            line: line_number,
            note: note_number,
        } in hit_notes
        {
            let line = &self.chart.lines[line_number];
            let note = &line.notes[note_number];
            let x = line.get_x_position_from_time(
                note.time,
                self.current_time,
                &self.chart.canvas_moves,
            );
            // println!("Hit note at {:.3}", x);
            let screen_x = Self::get_screen_x_from_x(window_size, play_area_size, x);
            self.add_click_particles(play_area_size, screen_x);
        }
    }
}

impl Screen for GameScreen<'_> {
    fn open(&mut self, game: &mut Game) -> Result<(), super::ScreenOpenError> {
        let mut music_option = try_open_file(
            &self.chart_dir_path,
            Vec::from(MUSIC_FILENAMES),
            |item_path| -> Option<Music> { Music::from_file(&item_path) },
        )?;

        if let Some(music) = &mut music_option {
            self.music_duration = Some((music.duration().as_seconds() * 1000.0) as u64);
            music.set_volume(game.settings.music_volume as f32);
            music.stop();
        }
        self.music = music_option;
        self.should_realign_audio_on_update = true;

        self.chart = Chart::from_file(
            self.chart_dir_path
                .join(self.chart_filename.clone())
                .to_str()
                .unwrap()
                .to_owned(),
        )?;

        // TODO: show warnings of the verification results in-game
        self.chart.verify();

        self.previous_bpm = if let Some(bpm_shift) = self.chart.bpm_shifts.first() {
            let ms = Self::get_ms_from_beat(bpm_shift.time, self.chart.bpm, 0, 0.0);
            BpmChangeData::new(0, bpm_shift.time, ms, bpm_shift.value, self.chart.bpm)
        } else {
            BpmChangeData::new(0, 0.0, 0, 1.0, self.chart.bpm)
        };
        self.next_bpm = self.previous_bpm.clone();

        if self.music_duration.is_none() {
            self.music_duration = Some(
                (Self::get_ms_from_beat(
                    self.chart.calculate_duration(),
                    self.previous_bpm.bpm(),
                    self.previous_bpm.ms,
                    self.previous_bpm.time,
                )) as u64,
            );
        }

        self.current_theme = self.chart.themes[0].clone();
        self.cached_info = CachedInfo::from_chart(&self.chart);
        Ok(())
    }

    fn draw(&mut self, game: &mut Game) -> Result<(), ScreenDrawError> {
        let background_color = self.current_theme.get_background_color();

        game.window.clear(background_color.into());
        game.window.set_view(&game.get_default_view());

        let window_size = game.window.size();
        let play_area = Self::calculate_play_area(game.window.size());
        let play_area_size = play_area.size();
        let scale = Self::calculate_scale(play_area_size);

        self.draw_all_lines(
            &mut game.window,
            scale,
            window_size,
            play_area_size,
            game.debug_mode,
            &game.font,
        );
        self.draw_all_notes(&mut game.window, window_size, play_area_size, scale);
        self.draw_screen_edges(
            &mut game.window,
            window_size,
            play_area,
            if game.debug_mode {
                Color::TRANSPARENT
            } else {
                background_color.into()
            },
        );
        self.draw_judge_line(&mut game.window, window_size, play_area_size);
        self.draw_debug_canvases(
            &mut game.window,
            window_size,
            play_area_size,
            &game.font,
            game.debug_mode,
        );
        self.particle_engine.draw(&mut game.window, &game.font)?;

        if game.debug_mode {
            self.draw_debug_screen_boundaries(&mut game.window, play_area);
            self.draw_debug_tooltip(&mut game.window, window_size, play_area_size, &game.font);
            self.add_debug_text(game);
        }

        Ok(())
    }

    fn update(&mut self, game: &mut Game) -> Result<(), ScreenUpdateError> {
        self.current_timestamp = SystemTime::now();
        self.current_time_ms = time_difference_ms(
            self.song_start_timestamp,
            if self.paused {
                self.pause_timestamp
            } else {
                self.current_timestamp
            },
        ) as i64
            + self.skip_time_ms
            - self.pause_time_ms;

        self.current_time = Self::get_beat_from_ms_bpm(self.current_time_ms, self.previous_bpm);

        let ms = self.get_current_audio_ms();
        if let Some(music) = &mut self.music {
            if self.should_realign_audio_on_update {
                if ms >= 0.0 {
                    if music.status() == SoundStatus::STOPPED {
                        music.play();
                    }
                    music.set_playing_offset(Time::milliseconds(ms as i32));
                    self.should_realign_audio_on_update = false;
                    println!("Realigned audio to {}ms", ms);
                } else {
                    if music.status() == SoundStatus::PLAYING {
                        music.stop();
                    }
                }
            }
        }

        let window_size = game.window.size();
        let play_area = Self::calculate_play_area(window_size);

        self.update_gameplay(window_size, play_area);

        if (self.next_bpm.index != self.previous_bpm.index) || self.previous_bpm.index == 0 {
            let mut next_bpm_change_optional = self.chart.bpm_shifts.get_mut(self.next_bpm.index);

            while let Some(next_bpm_change) = next_bpm_change_optional {
                if self.current_time >= next_bpm_change.time {
                    // Current time surpassed next bpm change time, so we need to update the bpm.
                    if DEBUG_PRINT_TIME_INFO {
                        println!(
                            ">>>>> [@{:.3}] Found new bpm! @ {:.3}",
                            self.current_time, next_bpm_change.time
                        );
                    }
                    self.previous_bpm = self.next_bpm.clone();

                    // TODO: Cleanup BPM code; instead of using current_bpm_change_index, search for the bpm dynamically maybe, to allow for skipping forwards and backwards
                    // Or maybe search both forwards and backwards, depending if the current time is behind or ahead of the current/next bpm change
                    // TODO: Chart verification: recalculate floor_position for every keypoint in the chart
                    if let Some(new_next_bpm_change) =
                        self.chart.bpm_shifts.get_mut(self.previous_bpm.index + 1)
                    {
                        self.next_bpm = BpmChangeData::new(
                            self.previous_bpm.index + 1,
                            new_next_bpm_change.time,
                            0,
                            new_next_bpm_change.value,
                            self.chart.bpm,
                        );

                        self.next_bpm.ms =
                            Self::get_ms_from_beat_bpm(new_next_bpm_change.time, self.previous_bpm);

                        next_bpm_change_optional = Some(new_next_bpm_change);
                    } else {
                        next_bpm_change_optional = None;
                    }
                } else {
                    break;
                }
            }
        }

        if self.current_time >= self.chart.challenge_times[0].start
            && self.current_time <= self.chart.challenge_times[0].end
        {
            if !self.challenge_time {
                self.challenge_time = true;
                self.current_theme = self.chart.themes[1].clone();
            }
        } else {
            if self.challenge_time {
                self.challenge_time = false;
                self.current_theme = self.chart.themes[0].clone();
            }
        }

        if mouse::Button::is_pressed(mouse::Button::Left) {
            let mouse_position = game.window.mouse_position();
            self.particle_engine
                .particles
                .push(AnyParticle::CircleParticle(
                    CircleParticle::new_test_particle(
                        SystemTime::now(),
                        mouse_position.x as f64,
                        mouse_position.y as f64,
                    ),
                ));
        }

        // Exit after the song has ended
        if let Some(music_duration) = self.music_duration {
            if self.current_time_ms > (music_duration + BREAK_TIME_AFTER_CHART_END) as i64 {
                game.change_screen(Box::new(ResultsScreen::new(
                    self.gameplay.play_results.clone(),
                )));
            }
        }
        Ok(())
    }

    fn handle_event(
        &mut self,
        game: &mut Game,
        event: sfml::window::Event,
    ) -> Result<(), super::ScreenEventError> {
        // TODO: detect input time more precisely
        let input_time = self.current_time_ms;

        let window_size = game.window.size();
        let play_area = Self::calculate_play_area(window_size);

        match event {
            Event::MouseButtonPressed { x, y, .. } => {
                self.particle_engine
                    .particles
                    .push(AnyParticle::CircleParticle(
                        CircleParticle::new_test_particle(SystemTime::now(), x as f64, y as f64),
                    ))
            }
            Event::MouseWheelScrolled { delta, .. } => {
                if game.debug_mode {
                    self.skip_time((300.0 * delta) as i64);
                }
            }
            Event::KeyPressed { code: Key::F4, .. } => {
                self.debug_view_canvas = !self.debug_view_canvas
            }
            Event::KeyPressed {
                code, shift, ctrl, ..
            } => match (game.debug_mode, code, shift, ctrl) {
                (_, Key::Escape, ..) => {
                    game.pop_screen();
                }
                (true, Key::Left, false, false) => self.skip_time(-1000),
                (true, Key::Right, false, false) => self.skip_time(1000),
                (true, Key::Left, true, false) => self.skip_time(-100),
                (true, Key::Right, true, false) => self.skip_time(100),
                (true, Key::Home, ..) => self.set_time(0),
                (true, Key::End, ..) => self.set_time(self.music_duration.unwrap_or(0) as i64),
                (true, Key::Space, ..) => self.toggle_pause()?,
                (true, Key::Left, false, true) => {
                    self.debug_selected_line -= 1;
                    if self.debug_selected_line < 0 {
                        self.debug_selected_line = self.chart.lines.len() as isize - 1;
                    }
                    self.debug_selected_line_segment = 0;
                }
                (true, Key::Right, false, true) => {
                    self.debug_selected_line += 1;
                    if self.debug_selected_line >= self.chart.lines.len() as isize {
                        self.debug_selected_line = 0;
                    }
                    self.debug_selected_line_segment = 0;
                }
                (true, Key::Up, false, true) => {
                    self.debug_selected_line_segment += 1;
                }
                (true, Key::Down, false, true) => {
                    self.debug_selected_line_segment -= 1;
                }
                (_, code, ..) => {
                    self.press_finger(window_size, play_area, code.into(), input_time);
                }
            },
            Event::KeyReleased { code, .. } => {
                self.release_finger(window_size, play_area, code.into(), input_time);
            }
            _ => {}
        }
        Ok(())
    }
}
