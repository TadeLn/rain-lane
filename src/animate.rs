use std::{
    ops::{Add, Mul},
    time::{Duration, SystemTime},
};

use crate::{
    game::time_difference_ms,
    interpolate::{interpolate, EaseType},
};

pub struct Animate<T, U = T, V = U>
where
    T: Copy,
    T: Mul<f64, Output = U>,
    U: Add<U, Output = V>,
    V: Into<T>,
{
    pub begin_value: T,
    pub end_value: T,
    pub duration: Duration,
    pub start_timestamp: SystemTime,
    pub ease_type: EaseType,
}

impl<T, U, V> Animate<T, U, V>
where
    T: Copy,
    T: Mul<f64, Output = U>,
    U: Add<U, Output = V>,
    V: Into<T>,
{
    pub fn get_current_value(&self) -> T {
        self.get_value_at_time(SystemTime::now())
    }

    pub fn get_value_at_time(&self, timestamp: SystemTime) -> T {
        let t = self.get_eased_progress_at_time(timestamp);
        ((self.begin_value * (1.0 - t)) + (self.end_value * t)).into()
    }

    // Returns a linear clamped value from 0.0 to 1.0
    pub fn get_progress_at_time(&self, timestamp: SystemTime) -> f64 {
        let ms_elapsed = time_difference_ms(self.start_timestamp, timestamp);
        let duration_secs = self.duration.as_secs_f64();
        (ms_elapsed / (duration_secs * 1000.0)).clamp(0.0, 1.0)
    }

    // Returns an eased clamped value from 0.0 to 1.0
    pub fn get_eased_progress_at_time(&self, timestamp: SystemTime) -> f64 {
        let t = self.get_progress_at_time(timestamp);
        interpolate(0.0, 1.0, t, self.ease_type)
    }

    pub fn start_at_time(
        &mut self,
        from: T,
        to: T,
        duration: Duration,
        timestamp: SystemTime,
        easing: EaseType,
    ) {
        self.begin_value = from;
        self.end_value = to;
        self.duration = duration;
        self.start_timestamp = timestamp;
        self.ease_type = easing;
    }

    pub fn animate(
        &mut self,
        from: T,
        to: T,
        duration: Duration,
        timestamp: SystemTime,
        ease_type: EaseType,
    ) {
        self.begin_value = from;
        self.end_value = to;
        self.duration = duration;
        self.start_timestamp = timestamp;
        self.ease_type = ease_type;
    }

    pub fn animate_now(&mut self, from: T, to: T, duration: Duration, ease_type: EaseType) {
        self.begin_value = from;
        self.end_value = to;
        self.duration = duration;
        self.start_timestamp = SystemTime::now();
        self.ease_type = ease_type;
    }

    pub fn animate_to(
        &mut self,
        to: T,
        duration: Duration,
        timestamp: SystemTime,
        easing: EaseType,
    ) {
        self.begin_value = self.get_value_at_time(timestamp);
        self.end_value = to;
        self.duration = duration;
        self.start_timestamp = timestamp;
        self.ease_type = easing;
    }

    pub fn animate_now_to(&mut self, to: T, duration: Duration, easing: EaseType) {
        let now = SystemTime::now();
        self.begin_value = self.get_value_at_time(now);
        self.end_value = to;
        self.duration = duration;
        self.start_timestamp = now;
        self.ease_type = easing;
    }

    pub fn still(value: T) -> Self {
        Animate {
            begin_value: value,
            end_value: value,
            duration: Duration::from_secs(1),
            start_timestamp: SystemTime::now(),
            ease_type: EaseType::Linear,
        }
    }

    pub fn new(
        from: T,
        to: T,
        duration: Duration,
        timestamp: SystemTime,
        easing: EaseType,
    ) -> Self {
        Animate {
            begin_value: from,
            end_value: to,
            duration,
            start_timestamp: timestamp,
            ease_type: easing,
        }
    }
}

impl<T, U, V> Default for Animate<T, U, V>
where
    T: Copy,
    T: Default,
    T: Mul<f64, Output = U>,
    U: Add<U, Output = V>,
    V: Into<T>,
{
    fn default() -> Animate<T, U, V> {
        Animate::still(T::default())
    }
}
