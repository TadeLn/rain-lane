use sfml::system::{Vector2f, Vector2u};

pub fn is_point_in_window(window_size: Vector2u, point: Vector2f) -> bool {
    point.x >= 0.0
        && point.x <= window_size.x as f32
        && point.y >= 0.0
        && point.y <= window_size.y as f32
}
