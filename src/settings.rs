use std::fs;

use serde::{Deserialize, Serialize};

use crate::chart::JsonFileError;

#[derive(Serialize, Deserialize, Debug)]
pub struct Settings {
    pub metronome_volume: f64,
    pub hit_volume: f64,
    pub music_volume: f64,
    pub preview_volume: f64,
    pub songs_directory: String,
    pub font_filename: String,
    pub monospace_font_filename: String,
    pub translation_filename: String,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            metronome_volume: 5.0,
            hit_volume: 15.0,
            music_volume: 25.0,
            preview_volume: 15.0,
            songs_directory: "songs".to_owned(),
            font_filename: "res/font/ofl/Orbitron/Orbitron-ExtraBold.ttf".to_owned(),
            monospace_font_filename: "res/font/ofl/Adobe/SourceCodePro-Regular.ttf".to_owned(),
            translation_filename: "res/lang/eng.json".to_owned(),
        }
    }
}

impl Settings {
    pub fn from_file(filename: &str) -> Result<Settings, JsonFileError> {
        let json = fs::read_to_string(filename)?;
        let mut settings = serde_json::from_str::<Settings>(&json)?;
        settings.metronome_volume = settings.metronome_volume.clamp(0.0, 100.0);
        settings.hit_volume = settings.hit_volume.clamp(0.0, 100.0);
        settings.music_volume = settings.music_volume.clamp(0.0, 100.0);
        settings.preview_volume = settings.preview_volume.clamp(0.0, 100.0);
        Ok(settings)
    }

    pub fn to_file(&self, filename: &str) -> Result<(), JsonFileError> {
        let json = serde_json::to_string_pretty::<Settings>(&self)?;
        fs::write(filename, json)?;
        Ok(())
    }
}
