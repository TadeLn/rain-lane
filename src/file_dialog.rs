use std::{
    env::current_dir,
    path::{Path, PathBuf},
};

use native_dialog::FileDialog;

use crate::{translate, translate_fmt};

pub fn pathbuf_to_string(pathbuf: PathBuf) -> String {
    pathbuf.to_str().unwrap().to_string() // TODO: is there a better way to do this?
}

pub fn string_to_pathbuf(string: String) -> PathBuf {
    PathBuf::from(string)
}

// Returns Some with the selected path on success
// Returns None on failure or on user cancel
pub fn open_file_dialog(
    starting_path: &Path,
    filename: &String,
    filters: &[(&str, &[&str])],
) -> Option<PathBuf> {
    let mut file_dialog = FileDialog::new()
        .set_location(starting_path)
        .set_filename(filename);

    for filter in filters {
        file_dialog = file_dialog.add_filter(&filter.0, filter.1);
    }

    file_dialog.show_open_single_file().unwrap_or(None)
}

// Returns Some with the selected path on success
// Returns None on failure or on user cancel
pub fn open_directory_dialog(starting_path: &Path) -> Option<PathBuf> {
    FileDialog::new()
        .set_location(starting_path)
        .show_open_single_dir()
        .unwrap_or(None)
}

// Returns Some with the selected path on success
// Returns None on failure or on user cancel
pub fn open_font_file_dialog(filename: &String) -> Option<PathBuf> {
    open_file_dialog(
        &current_dir().unwrap_or_default().join("res/font"),
        filename,
        &[
            (
                &file_select_filter("dialog.choose_file.filter.font_files", &["ttf", "otf"]),
                &["ttf", "otf"],
            ),
            (
                &file_select_filter("dialog.choose_file.filter.all_files", &["*"]),
                &["*"],
            ),
        ],
    )
}
// Returns Some with the selected path on success
// Returns None on failure or on user cancel
pub fn open_translation_file_dialog(filename: &String) -> Option<PathBuf> {
    open_file_dialog(
        &current_dir().unwrap_or_default().join("res/lang"),
        filename,
        &[
            (
                &file_select_filter("dialog.choose_file.filter.language_files", &["lang.json"]),
                &["lang.json"],
            ),
            (
                &file_select_filter("dialog.choose_file.filter.all_files", &["*"]),
                &["*"],
            ),
        ],
    )
}

fn file_select_filter(translation_key: &str, extensions: &[&str]) -> String {
    let mut formatted_extensions = String::from("(");
    for (i, ext) in extensions.iter().enumerate() {
        if i != 0 {
            formatted_extensions += ", ";
        }
        formatted_extensions += ".";
        formatted_extensions += ext;
    }
    formatted_extensions += ")";
    return translate_fmt!(translation_key, formatted_extensions);
}

// Returns Some with the selected path on success
// Returns None on failure or on user cancel
pub fn open_current_directory_dialog() -> Option<PathBuf> {
    open_directory_dialog(&current_dir().unwrap_or_default())
}
