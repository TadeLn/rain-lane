use crate::{
    chart::JsonFileError,
    language::{Translation, TranslationOperation},
    screen::{
        title_screen::TitleScreen, Screen, ScreenCloseError, ScreenCoverError, ScreenDrawError,
        ScreenEventError, ScreenOpenError, ScreenUncoverError, ScreenUpdateError,
    },
    settings::Settings,
    song_cache::{SongListCache, SongScanError, SONG_LIST_CACHE_FILENAME},
    song_import::import::start_import,
    toast_manager::{Toast, ToastManager},
    translate, translate_fmt, GAME_VERSION,
};
use sfml::{
    audio::{Sound, SoundBuffer, SoundSource},
    graphics::{
        Color, Font, RectangleShape, RenderTarget, RenderWindow, Shape, Text, TextStyle,
        Transformable, View,
    },
    system::Vector2f,
    window::{ContextSettings, Event, Key, Style, VideoMode},
    ResourceLoadError, SfBox,
};
use std::{
    fs,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
    time::{Duration, SystemTime},
    vec,
};

pub const GAME_VERSION_STRING: &str = env!("CARGO_PKG_VERSION");
pub const SETTINGS_FILENAME: &str = "settings.json";
pub const INITIAL_WINDOW_SIZE_X: u32 = 600;
pub const INITIAL_WINDOW_SIZE_Y: u32 = 800;

#[derive(Debug)]
pub struct PathError();

pub fn try_open_file<T, F>(
    dir_path: &PathBuf,
    possible_filenames: Vec<&str>,
    function: F,
) -> Result<Option<T>, PathError>
where
    F: Fn(&str) -> Option<T>,
{
    let mut item_option = None;
    for item_filename in possible_filenames {
        let item_path = dir_path.join(item_filename);
        if !item_path.is_file() {
            continue;
        }

        let item_path_str = item_path.to_str().ok_or(PathError {})?;
        item_option = function(item_path_str);
        if item_option.is_some() {
            break;
        }
    }
    Ok(item_option)
}

pub fn time_difference_ms(from: SystemTime, to: SystemTime) -> f64 {
    match to.duration_since(from) {
        Ok(duration) => duration.as_secs_f64() * 1000.0,
        Err(err) => err.duration().as_secs_f64() * -1000.0,
    }
}

pub struct Game {
    pub window: RenderWindow,
    fullscreen: bool,
    default_view: SfBox<View>,
    pub settings: Settings,
    pub font: SfBox<Font>,
    pub monospace_font: SfBox<Font>,
    pub song_list_cache: Arc<Mutex<SongListCache>>,
    pub toast_manager: ToastManager,

    pub debug_mode: bool,
    debug_text: String,
    fps_counter: i32,
    last_fps_check: SystemTime,
    last_fps_count: i32,

    pub current_screen: Option<Box<dyn Screen>>,
    requested_new_screen: Option<Box<dyn Screen>>,
    requested_previous_screen: bool,
    covered_screens: Vec<Box<dyn Screen>>,
}

#[derive(Debug)]
pub enum GameCreateError {
    JsonFileError(JsonFileError),
    FontLoadError,
    ResourceLoadError(ResourceLoadError),
}

impl From<JsonFileError> for GameCreateError {
    fn from(value: JsonFileError) -> Self {
        Self::JsonFileError(value)
    }
}

impl From<ResourceLoadError> for GameCreateError {
    fn from(value: ResourceLoadError) -> Self {
        Self::ResourceLoadError(value)
    }
}

#[derive(Debug)]
pub enum GameStartError {
    SongScanError(SongScanError),
    ResourceLoadError(ResourceLoadError),
    JsonFileError(JsonFileError),
    ScreenOpenError(ScreenOpenError),
    ScreenCloseError(ScreenCloseError),
    ScreenCoverError(ScreenCoverError),
    ScreenUncoverError(ScreenUncoverError),
    ScreenEventError(ScreenEventError),
    ScreenDrawError(ScreenDrawError),
    ScreenUpdateError(ScreenUpdateError),
}

impl From<SongScanError> for GameStartError {
    fn from(value: SongScanError) -> Self {
        Self::SongScanError(value)
    }
}

impl From<ResourceLoadError> for GameStartError {
    fn from(value: ResourceLoadError) -> Self {
        Self::ResourceLoadError(value)
    }
}

impl From<JsonFileError> for GameStartError {
    fn from(value: JsonFileError) -> Self {
        Self::JsonFileError(value)
    }
}

impl From<ScreenOpenError> for GameStartError {
    fn from(value: ScreenOpenError) -> Self {
        Self::ScreenOpenError(value)
    }
}

impl From<ScreenCloseError> for GameStartError {
    fn from(value: ScreenCloseError) -> Self {
        Self::ScreenCloseError(value)
    }
}

impl From<ScreenCoverError> for GameStartError {
    fn from(value: ScreenCoverError) -> Self {
        Self::ScreenCoverError(value)
    }
}

impl From<ScreenUncoverError> for GameStartError {
    fn from(value: ScreenUncoverError) -> Self {
        Self::ScreenUncoverError(value)
    }
}

impl From<ScreenEventError> for GameStartError {
    fn from(value: ScreenEventError) -> Self {
        Self::ScreenEventError(value)
    }
}

impl From<ScreenDrawError> for GameStartError {
    fn from(value: ScreenDrawError) -> Self {
        Self::ScreenDrawError(value)
    }
}

impl From<ScreenUpdateError> for GameStartError {
    fn from(value: ScreenUpdateError) -> Self {
        Self::ScreenUpdateError(value)
    }
}

impl Game {
    pub fn new() -> Result<Self, GameCreateError> {
        let settings = match Settings::from_file(SETTINGS_FILENAME) {
            Ok(settings) => settings,
            Err(_) => {
                let new_settings = Settings::default();
                new_settings.to_file(SETTINGS_FILENAME)?;
                new_settings
            }
        };
        let font = Font::from_file(settings.font_filename.as_str())
            .ok_or(GameCreateError::FontLoadError)?;
        let monospace_font = Font::from_file(settings.monospace_font_filename.as_str())
            .ok_or(GameCreateError::FontLoadError)?;

        let mut context_settings = ContextSettings::default();
        context_settings.antialiasing_level = 8;

        Ok(Game {
            window: RenderWindow::new(
                (INITIAL_WINDOW_SIZE_X, INITIAL_WINDOW_SIZE_Y),
                &translate_fmt!("window.title", GAME_VERSION),
                Style::DEFAULT,
                &context_settings,
            ),
            settings,
            font,
            monospace_font,
            default_view: View::new(
                Vector2f::new(
                    INITIAL_WINDOW_SIZE_X as f32 / 2.0,
                    INITIAL_WINDOW_SIZE_Y as f32 / 2.0,
                ),
                Vector2f::new(INITIAL_WINDOW_SIZE_X as f32, INITIAL_WINDOW_SIZE_Y as f32),
            ),
            song_list_cache: Arc::new(Mutex::new(SongListCache::load_or_create(
                SONG_LIST_CACHE_FILENAME.into(),
            ))),
            current_screen: Some(Box::new(TitleScreen::new())),
            requested_new_screen: None,
            covered_screens: vec![],
            requested_previous_screen: false,
            debug_mode: false,
            toast_manager: ToastManager::new(),
            debug_text: String::new(),
            fps_counter: 0,
            last_fps_check: SystemTime::now(),
            last_fps_count: 0,
            fullscreen: false,
        })
    }

    pub fn push_screen(&mut self, new_screen: Box<dyn Screen>) {
        self.requested_new_screen = Some(new_screen);
    }

    pub fn change_screen(&mut self, new_screen: Box<dyn Screen>) {
        self.pop_screen();
        self.push_screen(new_screen);
    }

    pub fn pop_screen(&mut self) {
        self.requested_previous_screen = true;
    }

    pub fn get_default_view(&self) -> SfBox<View> {
        self.default_view.clone()
    }

    pub fn draw_loading_screen(&mut self, message: &str) {
        let font_ref = &self.font;
        let mut text = Text::new(
            &translate_fmt!("screen.loading.message", message),
            font_ref,
            20,
        );
        text.set_outline_color(Color::rgb(32, 32, 32));
        text.set_outline_thickness(2.0);
        text.set_fill_color(Color::WHITE);
        let origin = text.global_bounds().size() / 2.0;
        text.set_origin(origin);
        let window_center = self.window.size() / 2;
        text.set_position(Vector2f::new(
            window_center.x as f32,
            window_center.y as f32,
        ));
        self.window.draw(&text);
        self.window.display();
    }

    pub fn rescan_songs(&mut self) -> Result<(), SongScanError> {
        self.draw_loading_screen(&translate!("screen.loading.scanning_songs"));
        let path = Path::new(&self.settings.songs_directory).to_owned();
        match self
            .song_list_cache
            .lock()
            .unwrap()
            .rescan_songs(path.clone())
        {
            Err(SongScanError::IoError(_)) => fs::create_dir_all(path)?,
            result => result?,
        }
        Ok(())
    }

    pub fn import_songs(&mut self, input_path: &Path) -> Result<(), SongScanError> {
        self.draw_loading_screen(&translate!("screen.loading.importing_songs"));
        start_import(input_path, Default::default());
        Ok(())
    }

    pub fn add_debug_text(&mut self, debug_text: String) {
        self.debug_text += &debug_text;
    }

    pub fn handle_event_global(&mut self, event: Event) {
        match event {
            Event::Closed => self.window.close(),
            Event::Resized { width, height } => {
                println!("Changed window size: {}x{}", width, height);
                self.default_view = View::new(
                    Vector2f::new((width / 2) as f32, (height / 2) as f32),
                    Vector2f::new(width as f32, height as f32),
                );
            }
            Event::KeyPressed { code: Key::F3, .. } => {
                self.debug_mode = !self.debug_mode;
                if self.debug_mode {
                    self.toast_manager
                        .add_toast(Toast::short(&translate!("toast.message.debug_mode.enable")));
                } else {
                    self.toast_manager.add_toast(Toast::short(&translate!(
                        "toast.message.debug_mode.disable"
                    )));
                }
            }
            Event::KeyPressed { code: Key::F11, .. } => {
                self.fullscreen = !self.fullscreen;

                let mut context_settings = ContextSettings::default();
                context_settings.antialiasing_level = 8;

                if self.fullscreen {
                    self.window.recreate(
                        VideoMode::default(),
                        &translate_fmt!("window.title", GAME_VERSION),
                        Style::FULLSCREEN,
                        &ContextSettings::default(),
                    );
                    self.init_window();
                } else {
                    self.window.recreate(
                        (INITIAL_WINDOW_SIZE_X, INITIAL_WINDOW_SIZE_Y),
                        &translate_fmt!("window.title", GAME_VERSION),
                        Style::DEFAULT,
                        &context_settings,
                    );
                    self.init_window();
                }
            }
            _ => {}
        }
    }

    pub fn update_global(&mut self) {
        self.toast_manager.update();
    }

    pub fn draw_global(&mut self) {
        // Draw toasts
        self.toast_manager.draw(&mut self.window, &self.font);

        // Draw debug text
        if self.debug_mode {
            let mut debug_text = Text::new(
                self.debug_text
                    .strip_suffix("\n")
                    .unwrap_or(&self.debug_text),
                &self.monospace_font,
                12,
            );

            debug_text.set_style(TextStyle::BOLD);
            debug_text.set_fill_color(Color::WHITE);
            debug_text.set_outline_color(Color::BLACK);
            debug_text.set_outline_thickness(1.0);

            let bounds = debug_text.local_bounds();
            let mut debug_text_bg = RectangleShape::new();
            debug_text_bg.set_position(Vector2f::new(bounds.left, bounds.top));
            debug_text_bg.set_size(Vector2f::new(bounds.width, bounds.height));
            debug_text_bg.set_fill_color(Color::rgba(0, 0, 0, 127));

            self.window.draw(&debug_text_bg);
            self.window.draw(&debug_text);
        }

        self.fps_counter += 1;
        let now = SystemTime::now();
        if now
            .duration_since(self.last_fps_check)
            .unwrap_or(Duration::ZERO)
            > Duration::from_secs(1)
        {
            self.last_fps_count = self.fps_counter;
            self.last_fps_check = now;
            self.fps_counter = 0;
        }

        // Reset text
        self.debug_text = format!(
            "FPS: {}\n\
            Toasts: {}\n\
            ----------\n",
            self.last_fps_count,
            self.toast_manager.active_toast_count()
        );
    }

    pub fn init_window(&mut self) {
        self.window.set_framerate_limit(60);
        self.window.set_key_repeat_enabled(false);
        self.window.clear(Color::BLACK);
        self.draw_loading_screen(&translate!("screen.loading.initializing"));
    }

    pub fn start(mut self) -> Result<(), GameStartError> {
        Translation::from_file(Path::new(&self.settings.translation_filename))
            .ok()
            .map(|t| {
                Translation::translation(TranslationOperation::Replace(t));
                Some(())
            });
        self.init_window();

        let buffer_beep_low = SoundBuffer::from_file("res/sfx/beep_low.wav")?;
        let _buffer_beep_high = SoundBuffer::from_file("res/sfx/beep_high.wav")?;
        let buffer_click = SoundBuffer::from_file("res/sfx/click.wav")?;
        let mut sound_metronome = Sound::new();
        let mut sound_click = Sound::new();
        sound_click.set_looping(false);
        sound_metronome.set_looping(false);
        sound_click.set_buffer(&buffer_click);
        sound_metronome.set_buffer(&buffer_beep_low);

        while self.window.is_open() {
            sound_click.set_volume(self.settings.hit_volume as f32);
            sound_metronome.set_volume(self.settings.metronome_volume as f32);

            while let Some(event) = self.window.poll_event() {
                self.handle_event_global(event);
                if let Some(mut current_screen) = self.current_screen {
                    self.current_screen = None;
                    current_screen.handle_event(&mut self, event)?;
                    self.current_screen = Some(current_screen);
                }
            }

            if let Some(mut current_screen) = self.current_screen {
                // TODO: independent updates from draw
                self.current_screen = None;
                current_screen.update(&mut self)?;
                self.current_screen = Some(current_screen);
            }
            self.update_global();

            // let current_time_int = current_time.floor() as i64;
            // if current_time_int > self.old_beat_int {
            //     if ((current_time_int as f64 / 4.0).floor() * 4.0) as i64 == current_time_int {
            //         sound_metronome.set_buffer(&buffer_beep_high);
            //     } else {
            //         sound_metronome.set_buffer(&buffer_beep_low);
            //     }
            //     sound_metronome.play();
            //     self.old_beat_int = current_time_int;
            // }

            if let Some(mut current_screen) = self.current_screen {
                self.current_screen = None;
                current_screen.draw(&mut self)?;
                self.current_screen = Some(current_screen);
            }
            self.draw_global();
            self.window.display();

            if self.requested_previous_screen {
                if let Some(mut current_screen) = self.current_screen {
                    self.current_screen = None;
                    current_screen.close(&mut self)?;

                    if let Some(mut previous_screen) = self.covered_screens.pop() {
                        previous_screen.uncover(&mut self)?;
                        self.current_screen = Some(previous_screen);
                    }
                }
                self.requested_previous_screen = false;
            }

            if let Some(mut requested_new_screen) = self.requested_new_screen {
                self.requested_new_screen = None;

                if let Some(mut current_screen) = self.current_screen {
                    self.current_screen = None;
                    current_screen.cover(&mut self)?;
                    self.covered_screens.push(current_screen);
                }

                requested_new_screen.open(&mut self)?;
                self.current_screen = Some(requested_new_screen);
            }
        }
        Ok(())
    }
}
