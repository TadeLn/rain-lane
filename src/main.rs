use game::{Game, GameCreateError, GameStartError};
pub mod animate;
pub mod cached_info;
pub mod chart;
pub mod draw;
pub mod file_dialog;
pub mod game;
pub mod interpolate;
pub mod language;
pub mod line;
pub mod particle;
pub mod raw_image_data;
pub mod screen;
pub mod settings;
pub mod song_cache;
pub mod song_import;
pub mod song_info;
pub mod timed_entity;
pub mod toast_manager;

pub const GAME_VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug)]
pub enum GameError {
    GameStartError(GameStartError),
    GameCreateError(GameCreateError),
}

impl From<GameStartError> for GameError {
    fn from(value: GameStartError) -> Self {
        Self::GameStartError(value)
    }
}

impl From<GameCreateError> for GameError {
    fn from(value: GameCreateError) -> Self {
        Self::GameCreateError(value)
    }
}

fn main() -> Result<(), GameError> {
    let game = Game::new()?;
    Ok(game.start()?)
}
