use std::f64::consts::PI;

#[derive(Debug, Clone, Copy)]
pub enum EaseType {
    Linear,
    InSine,
    OutSine,
    InOutSine,
    InQuad,
    OutQuad,
    InOutQuad,
    InCubic,
    OutCubic,
    InOutCubic,
    InQuart,
    OutQuart,
    InOutQuart,
    InQuint,
    OutQuint,
    InOutQuint,
    InExpo,
    OutExpo,
    InOutExpo,
    InCirc,
    OutCirc,
    InOutCirc,
    Unknown,
}

impl EaseType {
    pub fn from_id(interpolation_id: i64) -> Self {
        // TODO: check these IDs with usage in charts
        match interpolation_id {
            0 => Self::Linear,
            1 => Self::InSine,
            2 => Self::OutSine,
            3 => Self::InOutSine,
            4 => Self::InQuad,
            5 => Self::OutQuad,
            6 => Self::InOutQuad,
            7 => Self::InCubic,
            8 => Self::OutCubic,
            9 => Self::InOutCubic,
            10 => Self::InQuart,
            11 => Self::OutQuart,
            12 => Self::InOutQuart,
            13 => Self::InQuint,
            14 => Self::OutQuint,
            15 => Self::InOutQuint,
            // _ => Self::Linear,
            _ => panic!("Unknown ease type: {}", interpolation_id),
        }
    }
}

pub fn interpolate(a: f64, b: f64, t: f64, ease_type: EaseType) -> f64 {
    if t.is_nan() {
        return a;
    }
    if t.is_infinite() {
        return if t.is_sign_negative() { a } else { b };
    }
    let x = t.clamp(0.0, 1.0);
    let result = match ease_type {
        EaseType::Linear => x,
        EaseType::InSine => 1.0 - ((x * PI) / 2.0).cos(),
        EaseType::OutSine => ((x * PI) / 2.0).sin(),
        EaseType::InOutSine => -((x * PI).cos() - 1.0) / 2.0,

        EaseType::InQuad => x * x,
        EaseType::OutQuad => 1.0 - ((1.0 - x) * (1.0 - x)),
        EaseType::InOutQuad => {
            if x < 0.5 {
                2.0 * x * x
            } else {
                1.0 - (-2.0 * x + 2.0).powi(2) / 2.0
            }
        }

        EaseType::InCubic => x * x * x,
        EaseType::OutCubic => 1.0 - ((1.0 - x) * (1.0 - x) * (1.0 - x)),
        EaseType::InOutCubic => {
            if x < 0.5 {
                4.0 * x * x * x
            } else {
                1.0 - (-2.0 * x + 2.0).powi(3) / 2.0
            }
        }

        EaseType::InQuart => x * x * x * x,
        EaseType::OutQuart => 1.0 - ((1.0 - x) * (1.0 - x) * (1.0 - x) * (1.0 - x)),
        EaseType::InOutQuart => {
            if x < 0.5 {
                8.0 * x * x * x * x
            } else {
                1.0 - (-2.0 * x + 2.0).powi(4) / 2.0
            }
        }

        EaseType::InQuint => x * x * x * x * x,
        EaseType::OutQuint => 1.0 - ((1.0 - x) * (1.0 - x) * (1.0 - x) * (1.0 - x) * (1.0 - x)),
        EaseType::InOutQuint => {
            if x < 0.5 {
                16.0 * x * x * x * x * x
            } else {
                1.0 - (-2.0 * x + 2.0).powi(5) / 2.0
            }
        }

        EaseType::InExpo => {
            if x == 0.0 {
                0.0
            } else {
                2.0_f64.powf((10.0 * x) - 10.0)
            }
        }
        EaseType::OutExpo => {
            if x == 1.0 {
                1.0
            } else {
                1.0 - 2.0_f64.powf(-10.0 * x)
            }
        }
        EaseType::InOutExpo => {
            if x == 0.0 {
                0.0
            } else {
                if x == 1.0 {
                    1.0
                } else {
                    if x < 0.5 {
                        2.0_f64.powf(20.0 * x - 10.0) / 2.0
                    } else {
                        (2.0 - 2.0_f64.powf(-20.0 * x + 10.0)) / 2.0
                    }
                }
            }
        }

        EaseType::InCirc => 1.0 - (1.0 - (x * x)).sqrt(),
        EaseType::OutCirc => (1.0 - ((x - 1.0) * (x - 1.0))).sqrt(),
        EaseType::InOutCirc => {
            if x < 0.5 {
                (1.0 - (1.0 - (2.0 * x).powi(2)).sqrt()) / 2.0
            } else {
                ((1.0 - (-2.0 * x + 2.0).powi(2)).sqrt() + 1.0) / 2.0
            }
        }

        EaseType::Unknown => x, // _ => x,
    };
    (b * result) + (a * (1.0 - result))
}
