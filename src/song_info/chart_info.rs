use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct ChartInfo {
    #[serde(default)]
    pub difficulty_type: String,

    #[serde(default)]
    pub difficulty_level: f64,

    #[serde(default)]
    pub charter: String,

    #[serde(default)]
    pub filename: String,
}

impl ChartInfo {
    pub fn difficulty_level_to_string(difficulty_level: f64) -> String {
        format!(
            "{}{}",
            difficulty_level as i64,
            if difficulty_level.fract() >= 0.5 {
                "+"
            } else {
                ""
            }
        )
    }
    pub fn get_difficulty_level_string(&self) -> String {
        Self::difficulty_level_to_string(self.difficulty_level)
    }
}
