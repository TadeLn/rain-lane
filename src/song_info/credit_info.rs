use serde::{Deserialize, Serialize};

use super::link::Link;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CreditInfo {
    #[serde(default)]
    pub artist: String,

    #[serde(default)]
    pub license: String,

    #[serde(default)]
    pub license_url: String,

    #[serde(default)]
    pub links: Vec<Link>,

    #[serde(default)]
    pub artist_links: Vec<Link>,
}

impl Default for CreditInfo {
    fn default() -> Self {
        CreditInfo {
            artist: String::new(),
            license: String::new(),
            license_url: String::new(),
            links: vec![],
            artist_links: vec![],
        }
    }
}
