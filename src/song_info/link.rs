use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Link {
    #[serde(rename = "type")]
    pub kind: String,
    pub label: String,
    pub url: String,
}
