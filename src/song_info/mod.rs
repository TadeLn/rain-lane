pub mod chart_info;
pub mod credit_info;
pub mod link;

use std::{fs, path::Path};

use serde::{Deserialize, Serialize};

use crate::{chart::JsonFileError, translate};

use self::{chart_info::ChartInfo, credit_info::CreditInfo};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SongInfo {
    #[serde(default)]
    pub song_credits: CreditInfo,

    #[serde(default)]
    pub image_credits: CreditInfo,

    #[serde(default = "SongInfo::default_title")]
    pub title: String,

    #[serde(default)]
    pub duration: i64,

    #[serde(default)]
    pub source: String,

    pub preview_start: Option<i64>,

    #[serde(default = "SongInfo::default_preview_duration")]
    pub preview_duration: u64,

    #[serde(default)]
    pub charts: Vec<ChartInfo>,
}

impl SongInfo {
    pub fn default_title() -> String {
        translate!("song.title.default")
    }
    pub fn default_preview_duration() -> u64 {
        20000
    }

    pub fn from_file(filename: &Path) -> Result<SongInfo, JsonFileError> {
        let json = fs::read_to_string(filename)?;
        Ok(serde_json::from_str::<SongInfo>(&json)?)
    }

    pub fn to_file(&self, filename: &Path) -> Result<(), JsonFileError> {
        let json = serde_json::to_string_pretty::<SongInfo>(&self)?;
        fs::write(filename, json)?;
        Ok(())
    }

    pub fn new() -> Self {
        SongInfo {
            song_credits: CreditInfo::default(),
            image_credits: CreditInfo::default(),
            title: String::new(),
            duration: 0,
            source: String::new(),
            preview_start: None,
            preview_duration: 20000,
            charts: vec![],
        }
    }
}
