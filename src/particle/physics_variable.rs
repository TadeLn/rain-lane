#[derive(Debug)]
pub struct PhysicsVariable {
    pub value: f64,
    pub velocity: f64,
    pub gravity: f64,
    pub friction: f64,
}

impl PhysicsVariable {
    pub fn update(&mut self, seconds_elapsed: f64) {
        self.value += self.velocity * seconds_elapsed;
        self.velocity -= self.velocity * self.friction * seconds_elapsed;
        self.velocity += self.gravity * seconds_elapsed;
    }

    pub fn new(value: f64, velocity: f64, gravity: f64, friction: f64) -> Self {
        Self {
            value,
            velocity,
            gravity,
            friction,
        }
    }

    pub fn new_still(value: f64) -> Self {
        Self::new(value, 0.0, 0.0, 0.0)
    }
}

impl From<f64> for PhysicsVariable {
    fn from(value: f64) -> Self {
        Self::new_still(value)
    }
}
