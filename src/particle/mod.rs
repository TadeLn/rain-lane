use std::time::{SystemTime, SystemTimeError};

use sfml::graphics::{Color, Font, RenderWindow};

pub mod changing_color;
pub mod circle_particle;
pub mod particle_engine;
pub mod physics_variable;
pub mod text_particle;

pub trait Particle {
    fn update(&mut self, current_timestamp: SystemTime) -> Result<(), SystemTimeError>;
    fn draw(&mut self, render_window: &mut RenderWindow, font: &Font);
    fn update_and_draw(
        &mut self,
        render_window: &mut RenderWindow,
        font: &Font,
        current_timestamp: SystemTime,
    ) -> Result<(), SystemTimeError> {
        self.update(current_timestamp)?;
        self.draw(render_window, font);
        Ok(())
    }

    fn alive(&self) -> bool;
    fn get_x(&self) -> f64;
    fn get_y(&self) -> f64;
    fn set_colors(&mut self, colors: Vec<Color>);
    fn set_x(&mut self, x: f64);
    fn set_y(&mut self, y: f64);
}
