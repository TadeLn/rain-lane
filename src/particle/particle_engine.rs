use std::time::{SystemTime, SystemTimeError};

use sfml::graphics::{Font, RenderWindow};

use super::{circle_particle::CircleParticle, text_particle::TextParticle, Particle};

pub enum AnyParticle {
    CircleParticle(CircleParticle),
    TextParticle(TextParticle),
}

impl Particle for AnyParticle {
    fn update(&mut self, current_timestamp: SystemTime) -> Result<(), SystemTimeError> {
        match self {
            Self::CircleParticle(p) => p.update(current_timestamp),
            Self::TextParticle(p) => p.update(current_timestamp),
        }
    }
    fn draw(&mut self, render_window: &mut RenderWindow, font: &Font) {
        match self {
            Self::CircleParticle(p) => p.draw(render_window, font),
            Self::TextParticle(p) => p.draw(render_window, font),
        }
    }

    fn alive(&self) -> bool {
        match self {
            Self::CircleParticle(p) => p.alive(),
            Self::TextParticle(p) => p.alive(),
        }
    }

    fn get_x(&self) -> f64 {
        match self {
            Self::CircleParticle(p) => p.get_x(),
            Self::TextParticle(p) => p.get_x(),
        }
    }

    fn get_y(&self) -> f64 {
        match self {
            Self::CircleParticle(p) => p.get_y(),
            Self::TextParticle(p) => p.get_y(),
        }
    }

    fn set_colors(&mut self, colors: Vec<sfml::graphics::Color>) {
        match self {
            Self::CircleParticle(p) => p.set_colors(colors),
            Self::TextParticle(p) => p.set_colors(colors),
        }
    }

    fn set_x(&mut self, x: f64) {
        match self {
            Self::CircleParticle(p) => p.set_x(x),
            Self::TextParticle(p) => p.set_x(x),
        }
    }

    fn set_y(&mut self, y: f64) {
        match self {
            Self::CircleParticle(p) => p.set_y(y),
            Self::TextParticle(p) => p.set_y(y),
        }
    }
}

pub struct ParticleEngine {
    pub particles: Vec<AnyParticle>,
}

impl ParticleEngine {
    pub fn new() -> Self {
        ParticleEngine { particles: vec![] }
    }
    pub fn draw(&mut self, window: &mut RenderWindow, font: &Font) -> Result<(), SystemTimeError> {
        for particle in &mut self.particles {
            particle.update_and_draw(window, font, SystemTime::now())?;
        }
        self.particles.retain(|x| x.alive());
        Ok(())
    }
}
