use std::time::{Duration, SystemTime};

use sfml::{
    graphics::{Color, Font, RenderTarget, RenderWindow, Text, Transformable},
    system::Vector2f,
};

use crate::translate;

use super::{changing_color::ChangingColor, physics_variable::PhysicsVariable, Particle};

pub struct TextParticle {
    create_timestamp: SystemTime,
    last_update_timestamp: SystemTime,
    text: String,
    char_size: u32,
    x: PhysicsVariable,
    y: PhysicsVariable,
    rotation: PhysicsVariable,
    scale: PhysicsVariable,
    color: ChangingColor,
    outline_color: ChangingColor,
    outline_thickness: PhysicsVariable,
    duration: Duration,
}

impl TextParticle {
    pub fn new(
        current_timestamp: SystemTime,
        text: String,
        color: ChangingColor,
        outline_color: ChangingColor,
        outline_thickness: f64,
        char_size: u32,
        x: f64,
        y: f64,
        x_vel: f64,
        y_vel: f64,
        friction: f64,
        rotation_vel: f64,
        scale: f64,
        duration: Duration,
    ) -> Self {
        Self {
            create_timestamp: current_timestamp,
            last_update_timestamp: current_timestamp,
            text,
            char_size,
            x: PhysicsVariable::new(x, x_vel, 0.0, friction),
            y: PhysicsVariable::new(y, y_vel, 0.0, friction),
            rotation: PhysicsVariable {
                value: 0.0,
                velocity: rotation_vel,
                gravity: 0.0,
                friction: 10.0,
            },
            scale: scale.into(),
            color,
            outline_color,
            outline_thickness: outline_thickness.into(),
            duration,
        }
    }

    pub fn new_judgement_name_particle(
        text: &str,
        color: Color,
        x: f64,
        y: f64,
        y_vel: f64,
        scale: f64,
        timing: i64,
    ) -> Self {
        let color_transparent = {
            let mut c = color;
            c.a = 0;
            c
        };

        Self::new(
            SystemTime::now(),
            text.to_string(),
            ChangingColor::new(vec![color, color, color, color_transparent], 0.15),
            ChangingColor::new(
                vec![
                    Color::BLACK,
                    Color::BLACK,
                    Color::BLACK,
                    Color::rgba(0, 0, 0, 0),
                ],
                0.15,
            ),
            6.0,
            48,
            x,
            y,
            0.0,
            y_vel,
            20.0,
            timing as f64 * 2.0,
            scale,
            Duration::from_millis(600),
        )
    }

    pub fn new_judgement_perfect_particle(x: f64, y: f64, scale: f64, timing: i64) -> Self {
        Self::new_judgement_name_particle(
            &translate!("gameplay.judgement.perfect"),
            Color::CYAN,
            x,
            y,
            -2500.0 * scale,
            scale,
            timing,
        )
    }

    pub fn new_judgement_great_particle(x: f64, y: f64, scale: f64, timing: i64) -> Self {
        Self::new_judgement_name_particle(
            &translate!("gameplay.judgement.great"),
            Color::GREEN,
            x,
            y,
            -1500.0 * scale,
            scale,
            timing,
        )
    }

    pub fn new_judgement_bad_particle(x: f64, y: f64, scale: f64, timing: i64) -> Self {
        Self::new_judgement_name_particle(
            &translate!("gameplay.judgement.bad"),
            Color::YELLOW,
            x,
            y,
            -500.0 * scale,
            scale,
            timing,
        )
    }

    pub fn new_judgement_miss_particle(x: f64, y: f64, scale: f64, timing: i64) -> Self {
        Self::new_judgement_name_particle(
            &translate!("gameplay.judgement.miss"),
            Color::RED,
            x,
            y,
            500.0 * scale,
            scale,
            timing,
        )
    }
}

impl Particle for TextParticle {
    fn update(&mut self, current_timestamp: SystemTime) -> Result<(), std::time::SystemTimeError> {
        let seconds_elapsed = current_timestamp
            .duration_since(self.last_update_timestamp)?
            .as_secs_f64();

        let seconds_since_birth = current_timestamp
            .duration_since(self.create_timestamp)?
            .as_secs_f64();

        self.last_update_timestamp = current_timestamp;

        self.x.update(seconds_elapsed);
        self.y.update(seconds_elapsed);
        self.rotation.update(seconds_elapsed);
        self.scale.update(seconds_elapsed);
        self.color.update(seconds_since_birth);
        self.outline_color.update(seconds_since_birth);
        self.outline_thickness.update(seconds_elapsed);
        Ok(())
    }

    fn draw(&mut self, render_window: &mut RenderWindow, font: &Font) {
        // let mut text = Text::new(self.text.as_str(), &font, self.char_size);
        // text.set_position(Vector2f::new(self.x.value as f32, self.y.value as f32));
        // text.set_fill_color(self.color.current);
        let mut text = Text::new(self.text.as_str(), &font, self.char_size);
        let local_bounds_size = text.local_bounds().size();
        text.set_origin(Vector2f::new(
            local_bounds_size.x * 0.5,
            local_bounds_size.y,
        ));

        text.set_position(Vector2f::new(self.x.value as f32, self.y.value as f32));
        text.set_scale(Vector2f::new(
            self.scale.value as f32,
            self.scale.value as f32,
        ));
        text.set_rotation(self.rotation.value as f32);
        text.set_fill_color(self.color.current);
        text.set_outline_color(self.outline_color.current);
        text.set_outline_thickness(self.outline_thickness.value as f32);
        render_window.draw(&mut text);
    }

    fn alive(&self) -> bool {
        self.last_update_timestamp <= self.create_timestamp + self.duration
    }
    fn get_x(&self) -> f64 {
        self.x.value
    }
    fn get_y(&self) -> f64 {
        self.y.value
    }
    fn set_colors(&mut self, colors: Vec<Color>) {
        self.color.color_list = colors;
    }
    fn set_x(&mut self, x: f64) {
        self.x.value = x;
    }
    fn set_y(&mut self, y: f64) {
        self.y.value = y;
    }
}
