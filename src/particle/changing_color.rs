use sfml::graphics::Color;

#[derive(Debug)]
pub struct ChangingColor {
    pub current: Color,
    pub color_list: Vec<Color>,
    pub change_time: f64,
}

impl ChangingColor {
    fn interpolate_color_from_list(
        seconds_since_birth: f64,
        color_change_time: f64,
        colors: &Vec<Color>,
    ) -> Color {
        let color_timer = seconds_since_birth / color_change_time;
        if color_timer.is_infinite() {
            return if color_timer.is_sign_positive() {
                *colors.last().unwrap()
            } else {
                *colors.first().unwrap()
            };
        }
        let color_index_1 = color_timer.floor() as usize;
        let color_index_2 = color_timer.ceil() as usize;
        let color_1 = colors[color_index_1.clamp(0, colors.len() - 1)];
        let color_2 = colors[color_index_2.clamp(0, colors.len() - 1)];
        let color_2_weight = color_timer - color_index_1 as f64;
        let color_1_weight = 1.0 - color_2_weight;
        Color::rgba(
            ((color_1.r as f64 * color_1_weight) + (color_2.r as f64 * color_2_weight)) as u8,
            ((color_1.g as f64 * color_1_weight) + (color_2.g as f64 * color_2_weight)) as u8,
            ((color_1.b as f64 * color_1_weight) + (color_2.b as f64 * color_2_weight)) as u8,
            ((color_1.a as f64 * color_1_weight) + (color_2.a as f64 * color_2_weight)) as u8,
        )
    }

    pub fn new(color_list: Vec<Color>, change_time: f64) -> Self {
        Self {
            current: Color::TRANSPARENT,
            color_list,
            change_time,
        }
    }

    pub fn new_still(color: Color) -> Self {
        Self::new(vec![color], 0.0)
    }

    pub fn update(&mut self, seconds_since_birth: f64) {
        self.current = Self::interpolate_color_from_list(
            seconds_since_birth,
            self.change_time,
            &self.color_list,
        );
    }
}

impl From<Color> for ChangingColor {
    fn from(value: Color) -> Self {
        Self::new_still(value)
    }
}
