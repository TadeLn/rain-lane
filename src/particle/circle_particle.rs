use rand::{thread_rng, Rng};
use sfml::graphics::{CircleShape, Color, Font, RenderTarget, RenderWindow, Shape, Transformable};
use sfml::system::Vector2f;

use std::f64::consts::TAU;
use std::time::{Duration, SystemTime, SystemTimeError};

use crate::chart;
use crate::chart::color::Color as ChartColor;
use crate::screen::game_screen::draw::NOTE_TAP_OUTER_CIRCLE_RADIUS;

use super::changing_color::ChangingColor;
use super::physics_variable::PhysicsVariable;
use super::Particle;

#[derive(Debug)]
pub struct CircleParticle {
    create_timestamp: SystemTime,
    last_update_timestamp: SystemTime,
    circle_point_count: usize,
    x: PhysicsVariable,
    y: PhysicsVariable,
    radius: PhysicsVariable,
    thickness: PhysicsVariable,
    color: ChangingColor,
    outline_color: ChangingColor,
    duration: Duration,
}

impl CircleParticle {
    fn new(
        current_timestamp: SystemTime,
        circle_point_count: usize,
        x: f64,
        y: f64,
        x_spread: f64,
        y_spread: f64,
        ring_spread_radius: f64,
        ring_spread_thickness: f64,
        angle: f64,
        angle_spread: f64,
        speed: f64,
        speed_spread: f64,
        gravity: f64,
        friction: f64,
        size: f64,
        size_spread: f64,
        size_change_speed: f64,
        size_change_friction: f64,
        thickness: f64,
        thickness_spread: f64,
        thickness_change_speed: f64,
        thickness_change_friction: f64,
        colors: Vec<Color>,
        color_change_time: f64,
        outline_colors: Vec<Color>,
        outline_color_change_time: f64,
        duration: Duration,
    ) -> Self {
        let mut rng = thread_rng();
        let random_angle: f64 = angle + ((rng.gen::<f64>() - 0.5) * angle_spread);
        let (mut vel_x, mut vel_y) = (random_angle * TAU).sin_cos();
        let velocity = speed + ((rng.gen::<f64>() - 0.5) * speed_spread);
        vel_x *= velocity;
        vel_y *= velocity;

        let mut x = x;
        let mut y = y;
        x += (rng.gen::<f64>() - 0.5) * x_spread;
        y += (rng.gen::<f64>() - 0.5) * y_spread;
        let (ring_spread_x, ring_spread_y) = (rng.gen::<f64>() * TAU).sin_cos();
        let ring_spread_amplitude =
            ring_spread_radius + ((rng.gen::<f64>() - 0.5) * ring_spread_thickness);
        x += ring_spread_x * ring_spread_amplitude;
        y += ring_spread_y * ring_spread_amplitude;

        let r = size + ((rng.gen::<f64>() - 0.5) * size_spread);
        let t = thickness + ((rng.gen::<f64>() - 0.5) * thickness_spread);

        Self {
            create_timestamp: current_timestamp,
            last_update_timestamp: current_timestamp,
            circle_point_count,
            x: PhysicsVariable::new(x, vel_x, 0.0, friction),
            y: PhysicsVariable::new(y, vel_y, gravity, friction),
            radius: PhysicsVariable::new(r, size_change_speed, 0.0, size_change_friction),
            thickness: PhysicsVariable::new(
                t,
                thickness_change_speed,
                0.0,
                thickness_change_friction,
            ),
            color: ChangingColor::new(colors, color_change_time),
            outline_color: ChangingColor::new(outline_colors, outline_color_change_time),
            duration,
        }
    }

    pub fn new_test_particle_1(current_timestamp: SystemTime, x: f64, y: f64) -> CircleParticle {
        Self::new(
            current_timestamp,
            30,
            x,
            y,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.1,
            800.0,
            400.0,
            1500.0,
            0.0,
            15.0,
            0.0,
            -10.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            vec![
                Color::rgba(192, 255, 255, 255),
                Color::rgba(0, 255, 255, 255),
                Color::rgba(0, 255, 255, 255),
                Color::rgba(192, 0, 192, 64),
            ],
            0.25,
            vec![Color::TRANSPARENT],
            0.0,
            Duration::from_millis(5000),
        )
    }

    pub fn new_test_particle(current_timestamp: SystemTime, x: f64, y: f64) -> CircleParticle {
        Self::new(
            current_timestamp,
            30,
            x,
            y,
            10.0,
            10.0,
            0.0,
            0.0,
            0.0,
            1.0,
            400.0,
            25.0,
            0.0,
            5.0,
            15.0,
            0.0,
            -15.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            vec![
                Color::rgba(255, 128, 128, 255),
                Color::rgba(255, 255, 128, 255),
                Color::rgba(128, 255, 128, 255),
                Color::rgba(128, 255, 255, 255),
                Color::rgba(128, 128, 255, 128),
                Color::rgba(255, 128, 255, 128),
            ],
            0.15,
            vec![Color::TRANSPARENT],
            0.0,
            Duration::from_millis(5000),
        )
    }

    pub fn new_small_hit_particle(
        current_timestamp: SystemTime,
        x: f64,
        y: f64,
        scale: f64,
        color: chart::color::Color,
    ) -> CircleParticle {
        let sf_color: Color = color.into();
        let mut sf_color_transparent = sf_color.clone();
        sf_color_transparent.a = 0;
        Self::new(
            current_timestamp,
            30,
            x,
            y,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            1200.0 * scale,
            100.0 * scale,
            0.0,
            10.0,
            15.0 * scale,
            7.5 * scale,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            vec![sf_color, sf_color_transparent],
            0.5,
            vec![Color::TRANSPARENT],
            0.0,
            Duration::from_millis(500),
        )
    }

    pub fn new_ring_hit_particle(
        current_timestamp: SystemTime,
        x: f64,
        y: f64,
        scale: f64,
        color: chart::color::Color,
    ) -> CircleParticle {
        let sf_color: Color = color.into();
        let mut sf_color_transparent = sf_color.clone();
        sf_color_transparent.a = 0;
        Self::new(
            current_timestamp,
            30,
            x,
            y,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            0.0,
            0.0,
            0.0,
            NOTE_TAP_OUTER_CIRCLE_RADIUS as f64 * scale,
            0.0,
            200.0 * scale,
            5.0,
            10.0 * scale,
            0.0,
            -50.0 * scale,
            4.0,
            vec![Color::TRANSPARENT],
            0.5,
            vec![sf_color, sf_color_transparent],
            0.5,
            Duration::from_millis(500),
        )
    }

    pub fn get_colors_for_background_particle(color: ChartColor) -> Vec<Color> {
        let mut sf_color: Color = color.into();
        sf_color.a = sf_color.a / 2;
        let mut sf_color_transparent = sf_color.clone();
        sf_color_transparent.a = 0;
        vec![
            sf_color_transparent,
            sf_color,
            sf_color,
            sf_color,
            sf_color_transparent,
        ]
    }

    pub fn new_background_particle(
        current_timestamp: SystemTime,
        x: f64,
        y: f64,
        color: ChartColor,
    ) -> CircleParticle {
        let duration_ms = thread_rng().gen_range(2000..=8000);
        let size_change_speed = thread_rng().gen_range(-10.0..10.0);
        Self::new(
            current_timestamp,
            75,
            x,
            y,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            10.0,
            5.0,
            0.0,
            0.0,
            100.0,
            50.0,
            size_change_speed,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            CircleParticle::get_colors_for_background_particle(color),
            duration_ms as f64 / 1000.0 / 5.0,
            vec![Color::TRANSPARENT],
            0.0,
            Duration::from_millis(duration_ms),
        )
    }
}

impl Particle for CircleParticle {
    fn update(&mut self, current_timestamp: SystemTime) -> Result<(), SystemTimeError> {
        let seconds_elapsed = current_timestamp
            .duration_since(self.last_update_timestamp)?
            .as_secs_f64();

        let seconds_since_birth = current_timestamp
            .duration_since(self.create_timestamp)?
            .as_secs_f64();

        self.last_update_timestamp = current_timestamp;

        self.x.update(seconds_elapsed);
        self.y.update(seconds_elapsed);
        self.radius.update(seconds_elapsed);
        self.thickness.update(seconds_elapsed);
        self.color.update(seconds_since_birth);
        self.outline_color.update(seconds_since_birth);
        Ok(())
    }

    fn draw(&mut self, render_window: &mut RenderWindow, _font: &Font) {
        if self.radius.value < 0.0 {
            return;
        }
        let mut circle = CircleShape::new(self.radius.value as f32, self.circle_point_count);
        circle.set_position(Vector2f::new(
            (self.x.value - self.radius.value) as f32,
            (self.y.value - self.radius.value) as f32,
        ));
        circle.set_outline_thickness(self.thickness.value.max(0.0) as f32);
        circle.set_outline_color(self.outline_color.current);
        circle.set_fill_color(self.color.current);
        render_window.draw(&mut circle);
    }

    fn alive(&self) -> bool {
        self.last_update_timestamp <= (self.create_timestamp + self.duration)
            && self.radius.value > 0.0
    }
    fn get_x(&self) -> f64 {
        self.x.value
    }
    fn get_y(&self) -> f64 {
        self.y.value
    }
    fn set_colors(&mut self, colors: Vec<Color>) {
        self.color.color_list = colors;
    }
    fn set_x(&mut self, x: f64) {
        self.x.value = x;
    }
    fn set_y(&mut self, y: f64) {
        self.y.value = y;
    }
}
