use sfml::{
    graphics::{Color, RenderWindow},
    system::Vector2f,
};

pub mod curved_line;
pub mod straight_line;

pub trait DrawableLine {
    fn draw(
        &self,
        render_window: &mut RenderWindow,
        play_area_size: Vector2f,
        thickness: f64,
        color_start: Color,
        color_end: Color,
        debug_highlight: bool,
    ) -> ();
}
