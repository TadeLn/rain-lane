use sfml::{
    graphics::{
        CircleShape, Color, PrimitiveType, RenderStates, RenderTarget, RenderWindow, Shape,
        Transformable, Vertex,
    },
    system::Vector2f,
};

use crate::interpolate::{interpolate, EaseType};

use super::{straight_line::StraightLine, DrawableLine};

pub const CURVED_LINE_FORCE_CURVED_RENDERING: bool = false;
pub const CURVED_LINE_STEP_SIZE: f64 = 10.0;
pub const CURVED_LINE_DEBUG_DRAW_CIRCLE: bool = false;

#[derive(Debug)]
pub struct CurvedLine {
    pub straight_line: StraightLine,
    pub ease_type: EaseType,
}

impl DrawableLine for CurvedLine {
    fn draw(
        &self,
        render_window: &mut RenderWindow,
        play_area_size: Vector2f,
        thickness: f64,
        color_start: Color,
        color_end: Color,
        debug_highlight: bool,
    ) -> () {
        if !CURVED_LINE_FORCE_CURVED_RENDERING {
            if let EaseType::Linear = self.ease_type {
                return self.straight_line.draw(
                    render_window,
                    play_area_size,
                    thickness,
                    color_start,
                    color_end,
                    debug_highlight,
                );
            }
        }

        let mut vertices = vec![];

        let x1 = self.straight_line.x1;
        let x2 = self.straight_line.x2;
        let y1 = self.straight_line.y1;
        let y2 = self.straight_line.y2;
        let half_thickness = thickness * 0.5;
        let delta_y = y2 - y1;

        let mut previous_step_x = x1;
        let mut previous_step_y =
            (y1 - ((-1) as f64 * CURVED_LINE_STEP_SIZE)).clamp(y1.min(y2), y1.max(y2));
        let mut step_y = (y1 - (0 as f64 * CURVED_LINE_STEP_SIZE)).clamp(y1.min(y2), y1.max(y2));

        let steps = (delta_y.abs() / CURVED_LINE_STEP_SIZE).ceil() as usize;
        for i in 0..(steps + 1) {
            let next_step_y =
                (y1 - ((i + 1) as f64 * CURVED_LINE_STEP_SIZE)).clamp(y1.min(y2), y1.max(y2));

            if previous_step_y < 0.0 || next_step_y > play_area_size.y as f64 {
                previous_step_y = step_y;
                step_y = next_step_y;
                continue;
            }

            let t = (step_y - y1) / (y2 - y1);
            let step_x = interpolate(x1, x2, t, self.ease_type);
            let step_color = if debug_highlight {
                Color::MAGENTA
            } else {
                Color::rgba(
                    interpolate(color_start.r.into(), color_end.r.into(), t, self.ease_type) as u8,
                    interpolate(color_start.g.into(), color_end.g.into(), t, self.ease_type) as u8,
                    interpolate(color_start.b.into(), color_end.b.into(), t, self.ease_type) as u8,
                    interpolate(color_start.a.into(), color_end.a.into(), t, self.ease_type) as u8,
                )
            };

            let step_delta_x = step_x - previous_step_x;
            let step_delta_y = CURVED_LINE_STEP_SIZE;
            let step_length =
                ((step_delta_x * step_delta_x) + (step_delta_y * step_delta_y)).sqrt();
            let step_x_normalized = step_delta_x / step_length;
            let step_y_normalized = step_delta_y / step_length;
            let step_x_rotated_1 = step_y_normalized * half_thickness;
            let step_x_rotated_2 = -step_y_normalized * half_thickness;
            let step_y_rotated_1 = step_x_normalized * half_thickness;
            let step_y_rotated_2 = -step_x_normalized * half_thickness;

            if CURVED_LINE_DEBUG_DRAW_CIRCLE {
                let radius = 3.0 as f64;
                let mut circle = CircleShape::new(radius as f32, 10);
                circle.set_fill_color(step_color);
                circle.set_position(Vector2f::new(
                    (step_x + step_x_rotated_1 - radius) as f32,
                    (step_y + step_y_rotated_1 - radius) as f32,
                ));
                render_window.draw(&circle);
                circle.set_position(Vector2f::new(
                    (step_x + step_x_rotated_2 - radius) as f32,
                    (step_y + step_y_rotated_2 - radius) as f32,
                ));
                render_window.draw(&circle);
            }

            vertices.push(Vertex::new(
                Vector2f::new(
                    (step_x + step_x_rotated_1) as f32,
                    (step_y + step_y_rotated_1) as f32,
                ),
                step_color,
                Vector2f::default(),
            ));
            vertices.push(Vertex::new(
                Vector2f::new(
                    (step_x + step_x_rotated_2) as f32,
                    (step_y + step_y_rotated_2) as f32,
                ),
                step_color,
                Vector2f::default(),
            ));

            previous_step_x = step_x;
            previous_step_y = step_y;
            step_y = next_step_y;
        }

        if !CURVED_LINE_DEBUG_DRAW_CIRCLE {
            render_window.draw_primitives(
                &vertices,
                PrimitiveType::TRIANGLE_STRIP,
                &RenderStates::default(),
            );
        }
    }
}

impl CurvedLine {
    pub fn new(x1: f64, y1: f64, x2: f64, y2: f64, ease_type: EaseType) -> CurvedLine {
        CurvedLine {
            straight_line: StraightLine::new(x1, y1, x2, y2),
            ease_type,
        }
    }
}
