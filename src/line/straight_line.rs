use sfml::{
    graphics::{Color, PrimitiveType, RenderStates, RenderTarget, RenderWindow, Vertex},
    system::Vector2f,
};

use super::DrawableLine;

#[derive(Debug)]
pub struct StraightLine {
    pub x1: f64,
    pub y1: f64,
    pub x2: f64,
    pub y2: f64,
}

impl DrawableLine for StraightLine {
    fn draw(
        &self,
        render_window: &mut RenderWindow,
        _play_area_size: Vector2f,
        thickness: f64,
        color_begin: Color,
        color_end: Color,
        debug_highlight: bool,
    ) -> () {
        let x1 = self.x1;
        let x2 = self.x2;
        let y1 = self.y1;
        let y2 = self.y2;
        let half_thickness = thickness / 2.0;
        let delta_x = x2 - x1;
        let delta_y = y2 - y1;
        let length = ((delta_x * delta_x) + (delta_y * delta_y)).sqrt();
        let x_normalized = delta_x / length;
        let y_normalized = delta_y / length;
        let x_rotated_1 = y_normalized * half_thickness;
        let x_rotated_2 = -y_normalized * half_thickness;
        let y_rotated_1 = -x_normalized * half_thickness;
        let y_rotated_2 = x_normalized * half_thickness;
        let color_begin = if debug_highlight {
            Color::MAGENTA
        } else {
            color_begin
        };
        let color_end = if debug_highlight {
            Color::MAGENTA
        } else {
            color_end
        };

        let points = vec![
            Vertex::new(
                Vector2f::new((x1 + x_rotated_1) as f32, (y1 + y_rotated_1) as f32),
                color_begin,
                Vector2f::default(),
            ),
            Vertex::new(
                Vector2f::new((x1 + x_rotated_2) as f32, (y1 + y_rotated_2) as f32),
                color_begin,
                Vector2f::default(),
            ),
            Vertex::new(
                Vector2f::new((x2 + x_rotated_2) as f32, (y2 + y_rotated_2) as f32),
                color_end,
                Vector2f::default(),
            ),
            Vertex::new(
                Vector2f::new((x2 + x_rotated_1) as f32, (y2 + y_rotated_1) as f32),
                color_end,
                Vector2f::default(),
            ),
        ];
        render_window.draw_primitives(
            &points,
            PrimitiveType::TRIANGLE_FAN,
            &RenderStates::default(),
        );
    }
}
impl StraightLine {
    pub fn new(x1: f64, y1: f64, x2: f64, y2: f64) -> StraightLine {
        StraightLine { x1, y1, x2, y2 }
    }
}
