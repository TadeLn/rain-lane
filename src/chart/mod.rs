use std::{cmp::Ordering, fs, io};

use serde::{Deserialize, Serialize};

use crate::screen::game_screen::{note_locator::NoteLocator, GameScreen};

use self::{
    camera_move::CameraMove, canvas_move::CanvasMove, challenge_time::ChallengeTime,
    key_point::KeyPoint, line::Line, theme::Theme,
};
pub mod camera_move;
pub mod canvas_move;
pub mod challenge_time;
pub mod color;
pub mod color_key_point;
pub mod key_point;
pub mod line;
pub mod line_point;
pub mod note;
pub mod theme;

#[derive(Serialize, Deserialize, Debug)]
pub struct Chart {
    #[serde(rename = "fileVersion")]
    pub file_version: i64,
    #[serde(rename = "songsName")]
    pub songs_name: String,
    pub themes: [Theme; 2],
    #[serde(rename = "challengeTimes")]
    pub challenge_times: [ChallengeTime; 1],
    #[serde(rename = "bPM")]
    pub bpm: f64,
    #[serde(rename = "bpmShifts")]
    pub bpm_shifts: Vec<KeyPoint>,
    pub offset: f64,
    pub lines: Vec<Line>,
    #[serde(rename = "canvasMoves")]
    pub canvas_moves: Vec<CanvasMove>,
    #[serde(rename = "cameraMove")]
    pub camera_move: CameraMove,
}
#[derive(Debug)]
pub enum JsonFileError {
    IoError(io::Error),
    SerdeJsonError(serde_json::Error),
}

impl From<io::Error> for JsonFileError {
    fn from(value: io::Error) -> Self {
        Self::IoError(value)
    }
}

impl From<serde_json::Error> for JsonFileError {
    fn from(value: serde_json::Error) -> Self {
        Self::SerdeJsonError(value)
    }
}

impl Chart {
    pub fn from_file(filename: String) -> Result<Chart, JsonFileError> {
        let json = fs::read_to_string(filename)?;
        Ok(serde_json::from_str::<Chart>(&json)?)
    }

    pub fn to_file(&self, filename: String) -> Result<(), JsonFileError> {
        let json = serde_json::to_string_pretty::<Chart>(&self)?;
        fs::write(filename, json)?;
        Ok(())
    }

    pub fn new() -> Self {
        Chart {
            file_version: 0,
            songs_name: String::new(),
            themes: [Theme::default(), Theme::default()],
            challenge_times: [ChallengeTime::new()],
            bpm: 120.0,
            bpm_shifts: vec![],
            offset: 0.0,
            lines: vec![],
            canvas_moves: vec![],
            camera_move: CameraMove::new(),
        }
    }

    pub fn check_floor_position(
        floor_position: f64,
        time: f64,
        previous_bpm: f64,
        previous_bpm_change_ms: i64,
        previous_bpm_change_time: f64,
    ) {
        let ms = GameScreen::get_ms_from_beat(
            time,
            previous_bpm,
            previous_bpm_change_ms,
            previous_bpm_change_time,
        );
        let floor_position_ms = (floor_position * 1000.0).floor() as i64;
        if ms != floor_position_ms {
            println!(
                "WARNING: Key point floor position invalid! Calculated time is {}ms floor_position is {}ms",
                ms,
                floor_position_ms
            )
        }
    }

    pub fn verify(&self) {
        println!("Verifying chart...");
        if self.file_version != 0 {
            println!(
                "WARNING: Invalid file version (expected 0, got {})",
                self.file_version
            );
        }
        if self.bpm < 1.0 {
            if self.bpm <= 0.0 {
                println!("ERROR: BPM ({}) is less than or equal to 0", self.bpm);
            } else {
                println!("WARNING: BPM ({})  is very low", self.bpm);
            }
        }
        for (i, canvas_move) in self.canvas_moves.iter().enumerate() {
            if i != canvas_move.index {
                println!(
                    "WARNING: index of canvas_move ({}) does not match its actual index ({})",
                    canvas_move.index, i
                );
            }
        }
        // for line in &self.lines {
        //     for line_point in &line.line_points {
        //         Self::check_floor_position(
        //             line_point.floor_position,
        //             line_point.time,
        //             self.bpm,
        //             0,
        //             0.0,
        //         );
        //     }
        // }
        println!("Verifying chart done");
    }

    pub fn calculate_duration(&self) -> f64 {
        let mut duration: f64 = 0.0;
        for challenge_time in &self.challenge_times {
            duration = duration.max(challenge_time.end);
        }
        for bpm_shift in &self.bpm_shifts {
            duration = duration.max(bpm_shift.time);
        }
        for line in &self.lines {
            for color_key_point in &line.judge_ring_color {
                duration = duration.max(color_key_point.time);
            }
            for color_key_point in &line.line_color {
                duration = duration.max(color_key_point.time);
            }
            for line_point in &line.line_points {
                duration = duration.max(line_point.time);
            }
            for note in &line.notes {
                duration = duration.max(note.time + note.get_note_duration().unwrap_or(0.0));
            }
        }
        for canvas_move in &self.canvas_moves {
            for key_point in &canvas_move.speed_key_points {
                duration = duration.max(key_point.time);
            }
            for key_point in &canvas_move.x_position_key_points {
                duration = duration.max(key_point.time);
            }
        }
        for key_point in &self.camera_move.x_position_key_points {
            duration = duration.max(key_point.time);
        }
        for key_point in &self.camera_move.scale_key_points {
            duration = duration.max(key_point.time);
        }
        duration
    }

    pub fn get_ordered_notes(&self) -> Vec<(f64, NoteLocator)> {
        // TODO: Calculate this once and cache on chart load
        let mut result = Vec::new();
        for (line_id, line) in self.lines.iter().enumerate() {
            for (note_id, note) in line.notes.iter().enumerate() {
                result.push((note.time, NoteLocator::new(line_id, note_id)));
            }
        }
        result.sort_by(|a, b| {
            if a.0 < b.0 {
                Ordering::Less
            } else if a.0 > b.0 {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        });
        result
    }
}
