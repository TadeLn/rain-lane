use serde::{Deserialize, Serialize};

use crate::timed_entity::TimedEntity;

use super::color::Color;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct LinePoint {
    pub time: f64,
    #[serde(rename = "xPosition")]
    pub x_position: f64,
    pub color: Color,
    #[serde(rename = "easeType")]
    pub ease_type: i64,
    #[serde(rename = "canvasIndex")]
    pub canvas_index: i64,
    #[serde(rename = "floorPosition")]
    pub floor_position: f64,
}

impl TimedEntity for LinePoint {
    fn get_time(&self) -> f64 {
        self.time
    }
}
