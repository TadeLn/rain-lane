use serde::{Deserialize, Serialize};

use super::color::Color;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Theme {
    #[serde(rename = "colorsList")]
    pub colors_list: [Color; 3],
}

impl Theme {
    pub fn new() -> Self {
        Theme {
            colors_list: [
                Color {
                    r: 240,
                    g: 240,
                    b: 240,
                    a: 255,
                },
                Color {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 255,
                },
                Color {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 128,
                },
            ],
        }
    }
    pub fn get_background_color(&self) -> Color {
        self.colors_list[0]
    }
    pub fn get_note_color(&self) -> Color {
        self.colors_list[1]
    }
    pub fn get_particle_color(&self) -> Color {
        self.colors_list[2]
    }
}

impl Default for Theme {
    fn default() -> Self {
        Theme {
            colors_list: [
                Color {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 255,
                },
                Color {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 255,
                },
                Color {
                    r: 255,
                    g: 255,
                    b: 255,
                    a: 255,
                },
            ],
        }
    }
}
