use std::ops::{Add, Mul};

use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Serialize, Deserialize, Debug)]
pub struct Color<T = u8> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

impl Mul<f64> for Color {
    type Output = Color<f64>;
    fn mul(self, rhs: f64) -> Self::Output {
        Color {
            r: (self.r as f64 * rhs),
            g: (self.g as f64 * rhs),
            b: (self.b as f64 * rhs),
            a: (self.a as f64 * rhs),
        }
    }
}
impl Add<Color<f64>> for Color<f64> {
    type Output = Color<f64>;
    fn add(self, rhs: Color<f64>) -> Self::Output {
        Color {
            r: (self.r + rhs.r),
            g: (self.g + rhs.g),
            b: (self.b + rhs.b),
            a: (self.a + rhs.a),
        }
    }
}

impl From<Color<f64>> for Color {
    fn from(value: Color<f64>) -> Self {
        Color {
            r: value.r as u8,
            g: value.g as u8,
            b: value.b as u8,
            a: value.a as u8,
        }
    }
}

impl From<Color> for Color<f64> {
    fn from(value: Color) -> Self {
        Color {
            r: value.r as f64,
            g: value.g as f64,
            b: value.b as f64,
            a: value.a as f64,
        }
    }
}

impl From<sfml::graphics::Color> for Color {
    fn from(value: sfml::graphics::Color) -> Self {
        Color {
            r: value.r,
            g: value.g,
            b: value.b,
            a: value.a,
        }
    }
}

impl Into<sfml::graphics::Color> for Color {
    fn into(self) -> sfml::graphics::Color {
        sfml::graphics::Color::rgba(self.r, self.g, self.b, self.a)
    }
}

impl Default for Color {
    fn default() -> Self {
        Color {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
        }
    }
}
