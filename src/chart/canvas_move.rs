use serde::{Deserialize, Serialize};

use crate::{
    interpolate::{interpolate, EaseType},
    timed_entity::find_neighboring_points,
};

use super::key_point::KeyPoint;

#[derive(Serialize, Deserialize, Debug)]
pub struct CanvasMove {
    pub index: usize,
    #[serde(rename = "xPositionKeyPoints")]
    pub x_position_key_points: Vec<KeyPoint>,
    #[serde(rename = "speedKeyPoints")]
    pub speed_key_points: Vec<KeyPoint>,
}

impl CanvasMove {
    pub fn get_x_position_from_time(&self, point_time: f64) -> f64 {
        let (previous, next, t) = find_neighboring_points(&self.x_position_key_points, point_time);
        interpolate(
            previous.value,
            next.value,
            t,
            EaseType::from_id(previous.ease_type),
        )
    }
    pub fn get_speed_from_time(&self, point_time: f64) -> f64 {
        // return 1.0;

        let (previous, _, _) = find_neighboring_points(&self.speed_key_points, point_time);
        return previous.value;

        /*
        interpolate(
            previous.value,
            next.value,
            t,
            EaseType::from_id(previous.ease_type),
        )
         */
    }
}
