use serde::{Deserialize, Serialize};

use crate::{
    interpolate::{interpolate, EaseType},
    timed_entity::find_neighboring_points,
};

use super::{
    canvas_move::CanvasMove, color::Color, color_key_point::ColorKeyPoint, line_point::LinePoint,
    note::Note,
};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Line {
    #[serde(rename = "linePoints")]
    pub line_points: Vec<LinePoint>,
    pub notes: Vec<Note>,
    #[serde(rename = "judgeRingColor")]
    pub judge_ring_color: Vec<ColorKeyPoint>,
    #[serde(rename = "lineColor")]
    pub line_color: Vec<ColorKeyPoint>,
}

impl Line {
    pub fn get_x_position_from_time(
        &self,
        point_time: f64,
        current_time: f64,
        canvases: &Vec<CanvasMove>,
    ) -> f64 {
        let (previous, next, t) = find_neighboring_points(&self.line_points, point_time);
        interpolate(
            previous.x_position
                + canvases[previous.canvas_index as usize].get_x_position_from_time(current_time),
            next.x_position
                + canvases[next.canvas_index as usize].get_x_position_from_time(current_time),
            t,
            EaseType::from_id(previous.ease_type),
        )
    }

    pub fn begin(&self) -> f64 {
        if let Some(point) = self.line_points.first() {
            point.time
        } else {
            0.0
        }
    }

    pub fn end(&self) -> f64 {
        if let Some(point) = self.line_points.last() {
            point.time
        } else {
            0.0
        }
    }

    pub fn is_in_bounds(&self, point_time: f64) -> bool {
        point_time > self.begin() && point_time < self.end()
    }

    // Calculate current line color
    // Returns None if `self.line_color` is empty
    pub fn get_line_color_from_time(&self, current_time: f64) -> Option<Color> {
        if self.line_color.is_empty() {
            None
        } else {
            if !self.is_in_bounds(current_time) {
                None
            } else if current_time > self.line_color.last().unwrap().time {
                Some(self.line_color.last().unwrap().start_color)
            } else if current_time < self.line_color.first().unwrap().time {
                Some(self.line_color.first().unwrap().end_color)
            } else {
                let (previous, _, t) = find_neighboring_points(&self.line_color, current_time);

                // No need to use the `interpolate` function here, because `t` is already a linear interpolation,
                // and color key points don't have any other ease types.
                Some(((previous.end_color * t) + (previous.start_color * (1.0 - t))).into())
            }
        }
    }

    // Calculate current line color
    // Returns None if `self.judge_ring_color` is empty
    pub fn get_judge_ring_color_from_time(&self, current_time: f64) -> Option<Color> {
        if self.judge_ring_color.is_empty() {
            None
        } else {
            if !self.is_in_bounds(current_time) {
                None
            } else if current_time > self.judge_ring_color.last().unwrap().time {
                Some(self.judge_ring_color.last().unwrap().start_color)
            } else if current_time < self.judge_ring_color.first().unwrap().time {
                Some(self.judge_ring_color.first().unwrap().end_color)
            } else {
                let (previous, _, t) =
                    find_neighboring_points(&self.judge_ring_color, current_time);

                // No need to use the `interpolate` function here, because `t` is already a linear interpolation,
                // and color key points don't have any other ease types.
                Some(((previous.end_color * t) + (previous.start_color * (1.0 - t))).into())
            }
        }
    }

    pub fn get_speed_from_time(&self, point_time: f64, canvases: &Vec<CanvasMove>) -> f64 {
        // return 1.0;

        let (previous, _, _) = find_neighboring_points(&self.line_points, point_time);
        return canvases[previous.canvas_index as usize].get_speed_from_time(point_time);

        /*
        let previous_point_speed =
            canvases[previous.canvas_index as usize].get_speed_from_time(current_time);
        let next_point_speed =
            canvases[next.canvas_index as usize].get_speed_from_time(current_time);
        let previous_point_time_delta = previous.time - current_time;
        let next_point_time_delta = next.time - current_time;
        let previous_point_distance = previous_point_time_delta * previous_point_speed;
        let next_point_distance = next_point_time_delta * next_point_speed;
        let line_duration = next.time - previous.time;
        let line_length = next_point_distance - previous_point_distance;
        let line_speed = line_length / line_duration;

        // return (previous.time, line_speed);
        return line_speed;

        // interpolate(
        //     previous_point_speed,
        //     next_point_speed,
        //     t,
        //     EaseType::from_id(previous.ease_type),
        // )
         */
    }
}
