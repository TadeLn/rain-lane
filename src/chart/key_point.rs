use serde::{Deserialize, Serialize};

use crate::timed_entity::TimedEntity;

#[derive(Serialize, Deserialize, Debug)]
pub struct KeyPoint {
    pub time: f64,
    pub value: f64,
    #[serde(rename = "easeType")]
    pub ease_type: i64,
    #[serde(rename = "floorPosition")]
    pub floor_position: f64,
}

impl KeyPoint {
    pub fn new(value: f64) -> Self {
        KeyPoint {
            time: 0.0,
            value,
            ease_type: 0,
            floor_position: 0.0,
        }
    }
}

impl TimedEntity for KeyPoint {
    fn get_time(&self) -> f64 {
        self.time
    }
}
