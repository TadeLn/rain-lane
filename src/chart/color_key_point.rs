use serde::{Deserialize, Serialize};

use crate::timed_entity::TimedEntity;

use super::color::Color;

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct ColorKeyPoint {
    #[serde(rename = "startColor")]
    pub start_color: Color,
    #[serde(rename = "endColor")]
    pub end_color: Color,
    pub time: f64,
}

impl TimedEntity for ColorKeyPoint {
    fn get_time(&self) -> f64 {
        self.time
    }
}
