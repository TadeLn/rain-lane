use serde::{Deserialize, Serialize};

use super::key_point::KeyPoint;

#[derive(Serialize, Deserialize, Debug)]
pub struct CameraMove {
    #[serde(rename = "scaleKeyPoints")]
    pub scale_key_points: Vec<KeyPoint>,
    #[serde(rename = "xPositionKeyPoints")]
    pub x_position_key_points: Vec<KeyPoint>,
}

impl CameraMove {
    pub fn new() -> Self {
        CameraMove {
            scale_key_points: vec![],
            x_position_key_points: vec![],
        }
    }
}
