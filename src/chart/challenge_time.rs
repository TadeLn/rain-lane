use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ChallengeTime {
    #[serde(rename = "checkPoint")]
    pub check_point: f64,
    pub start: f64,
    pub end: f64,
    #[serde(rename = "transTime")]
    pub trans_time: f64,
}

impl ChallengeTime {
    pub fn new() -> Self {
        ChallengeTime {
            check_point: 0.0,
            start: 0.0,
            end: 0.0,
            trans_time: 0.0,
        }
    }
}
