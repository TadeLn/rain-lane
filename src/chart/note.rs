use serde::{Deserialize, Serialize};

use crate::screen::game_screen::gameplay::finger::Finger;

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum MarkerType {
    Perfect = 3,
    Great = 2,
    Bad = 1,
    None = 0,
}

#[derive(Clone, Debug)]
pub struct HitInfo {
    pub done: bool,
    pub done_end: bool,
    pub marked: MarkerType,
    pub missed: bool,
    pub chord_hits: Vec<Finger>,
}

impl Default for HitInfo {
    fn default() -> Self {
        HitInfo {
            done: false,
            done_end: false,
            marked: MarkerType::None,
            missed: false,
            chord_hits: Vec::new(),
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Note {
    #[serde(rename = "type")]
    kind: i64,
    pub time: f64,
    #[serde(rename = "floorPosition")]
    pub floor_position: f64,
    #[serde(rename = "otherInformations")]
    pub other_informations: Vec<f64>,

    #[serde(skip_deserializing, skip_serializing, default)]
    pub hit_info: HitInfo,
}

#[derive(Clone, Debug, PartialEq)]
pub enum NoteType {
    TapNote,
    DragNote,
    HoldNote,
    UnknownNote,
}

impl Note {
    pub fn get_note_type(&self) -> NoteType {
        match self.kind {
            0 => NoteType::TapNote,
            1 => NoteType::DragNote,
            2 => NoteType::HoldNote,
            _ => NoteType::UnknownNote,
        }
    }
    pub fn get_note_end_time(&self) -> Option<f64> {
        match self.get_note_type() {
            NoteType::HoldNote => Some(self.other_informations[0]),
            _ => None,
        }
    }
    pub fn get_note_duration(&self) -> Option<f64> {
        match self.get_note_type() {
            NoteType::HoldNote => Some(self.other_informations[1]),
            _ => None,
        }
    }
    pub fn get_note_end_floor_position(&self) -> Option<f64> {
        match self.get_note_type() {
            NoteType::HoldNote => Some(self.other_informations[2]),
            _ => None,
        }
    }

    pub fn hit(&mut self) {
        if self.get_note_type() == NoteType::HoldNote {
            self.hit_info.done = true;
        } else {
            self.hit_info.done = true;
            self.hit_info.done_end = true;
        }
    }

    pub fn hit_end(&mut self) {
        self.hit_info.done_end = true;
        self.hit_info.chord_hits.clear();
    }

    pub fn mark(&mut self, marker: MarkerType) {
        if self.hit_info.marked < marker {
            self.hit_info.marked = marker;
        }
    }

    pub fn miss(&mut self) {
        self.hit_info.done = true;
        self.hit_info.done_end = true;
        self.hit_info.missed = true;
    }

    pub fn miss_end(&mut self) {
        self.hit_info.done = true;
        self.hit_info.done_end = true;
        self.hit_info.missed = true;
    }

    pub fn add_chord_hit(&mut self, finger: Finger) {
        if !self.hit_info.chord_hits.contains(&finger) {
            self.hit_info.chord_hits.push(finger);
        }
    }

    pub fn remove_chord_hit(&mut self, finger: Finger) {
        self.hit_info.chord_hits.retain(|x| x != &finger);
    }
}
