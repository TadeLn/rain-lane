# Changelog

## v0.2.0

### Additions
- Importing songs from Rizilne
- Translation system
- English and Polish translations
- Loading screen messages
- CHANGELOG.md file

### Changes
- Changed project name from rain-lane to rainlane 
- Changed default filename when choosing a font

### Fixes
- "Settings loaded successfully" toast is now a green "success" toast.
- Fixed crash when changing difficulty on a song with no charts


## v0.1.0

### Additions

- Initial release
- Gameplay
- Basic, keyboard-only menus
- Song select screen
- Judgement
- Basic settings
- Fullscreen mode (F11)