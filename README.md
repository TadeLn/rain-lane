# Rainlane

*Rainlane* - an open-source *Rizline* rhythm game clone.

**Note:** This project is currently in Alpha, do not expect anything to work properly yet.

**Rainlane Discord server:** https://discord.gg/HEd4AQGFkz


# Installing

Go to the [releases tab](https://gitlab.com/rainlane/rainlane-client/-/releases) and download the newest version for your operating system. Unpack the `.zip` archive and open `rainlane.exe` on Windows, or execute the `launch-rainlane` shell script on Linux.

## Adding songs

The game comes bundled with one test chart, which is not finished yet. If you want to add more custom songs, you can do so by putting your songs into the `songs/` directory right next to the executable file. Then, you will have to select **Rescan Songs** in the Settings menu to add the songs to the game.

### Importing your songs from Rizline

If you bought any songs in the original *Rizline*, and have an Android mobile device, you can import them to *Rainlane* by following these instructions:

#### Short version
0. Have `ffmpeg`, `ffprobe`, `vgmstream-cli` and `AssetRipper` in PATH.
1. Download everything in *Rizline*.
2. Connect the mobile device to your PC.
3. Navigate to `Android/data/com.PigeonGames.Rizline`.
4. Copy the `com.PigeonGames.Rizline` directory onto your desktop.
5. Select **Import Songs** in *Rainlane*.
6. Wait.
7. It's done!


#### Detailed version
0. You have to install several external tools to extract charts from Rizline. *Rainlane* uses these executables: `ffmpeg`, `ffprobe`, `vgmstream-cli` and `AssetRipper`. All of them have to be downloaded and their location has to be in the `PATH` environment variable. This step will be simplified in the future. You can get the necessary binaries here: [FFmpeg / FFprobe](https://ffmpeg.org/download.html), [vgmstream-cli](https://vgmstream.org/), [AssetRipper](https://github.com/AssetRipper/AssetRipper/releases)
1. Open *Rizline* and make sure that every relevant file is downloaded:
    - Chart files are downloaded when playing a chart for the first time. Each difficulty is a separate chart file, so you'll need to play all of them if you want to have a complete export. For example, if you have played some songs only on IN difficulty, and never played on EZ or HD, only the IN chart will be downloaded and imported into *Rainlane*.
    - Music files are downloaded when clicking on a song circle for the first time.
    - Illustrations (512px x 512px) are all downloaded at the start, you don't have to do anything extra.
    - (Optional) Big versions of illustrations (2048px x 2048px) are downloaded when you look at them in the Collections screen after purchasing them from the in-game shop. To have a complete export, you will have to buy all of the illustrations from the shop, for 500 dots each, look at them in the Collections screen, and wait for them to load the high quality version. The big illustrations are not necessary for the charts to work, but will look better in the menus, especially with a high resolution.
2. Connect your Android device to your PC with an USB cable. Make sure that the device's files are visible on your computer.
3. Using your PC, navigate to the `Android` directory on your device. In it, there should be 4 directories: `data`, `meta`, `obb`, `obj`. Enter the `data` directory. You'll see a lot of directories for all of the applications installed. Find the one named `com.PigeonGames.Rizline`.
4. Copy the `com.PigeonGames.Rizline` directory (or its contents) onto your PC. During the import process, *Rainlane* will automatically find the right directory in the game files.
The game finds and uses the `com.PigeonGames.Rizline/files/UnityCache/Shared` directory to get everything; if you want to, you can copy over only that one directory instead of copying over the entire game.
5. Launch *Rainlane*, and select **Import Songs** from the Settings menu. A file selection window will show up. You need to select the directory with *Rizline*'s files. Press OK.
6. Now you'll have to wait for the import process to finish. The audio files take the longest to process, because each file has to be converted from an ACB to a WAV, and then to an OGG (or another format). Big illustrations also take some time, but not as much. Other files do not take very long to process.
On my setup, big illustrations take about 1 second each to extract, and the audio files take about 2-3 seconds each. In total, a full 51 song export (from Rizline v1.0.16) takes about 3 minutes to finish.
7. After everything is done, the game will initiate a song rescan, which will automatically add all *Rizline* songs to the song list. Then, you can play them like any other song. All of the songs extracted were saved to the `songs/Rizline` directory if you ever need to find them.


# Building from source

1. Install [Rust](https://rustup.rs/) (for Windows, you will also have to install the MSVC toolchain)
2. Download or clone the project
3. Install [SFML 2.6](https://www.sfml-dev.org/download/sfml/2.6.0/)
    - Unless you can install this version of SFML with your package manager, you will have to set some environment variables:
        - Set `SFML_INCLUDE_DIR` to the `include/` directory of SFML
        - Set `SFML_LIBS_DIR` to the `lib/` directory of SFML
        - For Linux, set `LD_LIBRARY_PATH` to the `lib/` directory of SFML
        - For Windows, copy & paste `.dll` flies from the `lib/` directory of SFML into the project's `run/` directory
        - For Mac, good luck
4. Open a terminal and navigate to the `run/` directory
5. Run `cargo run`
